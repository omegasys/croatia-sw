package com.omega.croatiasw.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;
import java.sql.Timestamp;
import java.time.LocalDateTime;

/**
 * Created by volcanohong on AD 2017-07-03.
 */
@Entity
@Table(name = "JOB_LOCK_STATUS", schema = "admin_all")
@JsonIgnoreProperties({ "handler","hibernateLazyInitializer" })
public class JobLockStatus implements java.io.Serializable{
    private String jobName;
    private Boolean locked;
    private Timestamp jobStartedTime;
    private Timestamp jobEndTime;
    private String jobMessage;

    @Id
    @Column(name = "JOB_NAME")
    public String getJobName() {
        return jobName;
    }

    public void setJobName(String jobName) {
        this.jobName = jobName;
    }

    @Basic
    @Column(name = "LOCKED")
    public Boolean getLocked() {
        return locked;
    }

    public void setLocked(Boolean locked) {
        this.locked = locked;
    }

    @Basic
    @Column(name = "JOB_STARTED_TIME")
    public Timestamp getJobStartedTime() {
        return jobStartedTime;
    }

    public void setJobStartedTime(Timestamp jobStartedTime) {
        this.jobStartedTime = jobStartedTime;
    }

    public void setJobStartedTimeNow() {
        this.jobStartedTime = Timestamp.valueOf(LocalDateTime.now());
    }

    @Basic
    @Column(name = "JOB_END_TIME")
    public Timestamp getJobEndTime() {
        return jobEndTime;
    }

    public void setJobEndTime(Timestamp jobEndTime) {
        this.jobEndTime = jobEndTime;
    }

    public void setJobEndTimeNow() {
        this.jobEndTime = Timestamp.valueOf(LocalDateTime.now());
    }

    @Basic
    @Column(name = "JOB_MESSAGE")
    public String getJobMessage() {
        return jobMessage;
    }

    public void setJobMessage(String jobMessage) {
        this.jobMessage = jobMessage;
    }

}
