package com.omega.croatiasw.service;


import com.omega.croatiasw.entity.JobLockStatus;
import com.omega.croatiasw.repository.JobLockStatusRepository;
import com.omega.croatiasw.util.TimestampUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;
import javax.transaction.Transactional;

/**
 * Created by volcanohong on AD 2017-07-03.
 */
@Service
@Transactional
public class JobLockService {
    private final Logger log = LoggerFactory.getLogger(getClass().getSimpleName());
    private JdbcTemplate jdbc;
    private JobLockStatusRepository jobLockStatusRepository;

    public JobLockService(JdbcTemplate jdbc, JobLockStatusRepository jobLockStatusRepository) {
        this.jdbc = jdbc;
        this.jobLockStatusRepository = jobLockStatusRepository;
    }

    private Boolean changeJobLockStatus(JobLockStatus jobLockStatus) {
        log.info("change job, {}, lock status, {}", jobLockStatus.getJobName(), jobLockStatus.getLocked());
        SqlParameterSource parameters = new MapSqlParameterSource()
                .addValue("Job_Name", jobLockStatus.getJobName())
                .addValue("IsLocked", jobLockStatus.getLocked())
                .addValue("Job_Started_Time", jobLockStatus.getJobStartedTime())
                .addValue("Job_End_Time", jobLockStatus.getJobEndTime())
                .addValue("Job_Message", jobLockStatus.getJobMessage());
        SimpleJdbcCall jdbcCall = new SimpleJdbcCall(jdbc)
                .withSchemaName("admin_all")
                .withProcedureName("usp_setJobLockStatus");
        try {
            jdbcCall.execute(parameters);
        } catch (Exception e) {
            log.debug(e.getMessage());
//            e.printStackTrace();
            return false;
        }
        return true;
    }

    //The Table Id is composted by jobName and target job execution Date
    private JobLockStatus createJobLockStatusByNameAndTime(String jobName) {
        JobLockStatus jobLockStatus = new JobLockStatus();
        jobLockStatus.setJobName(jobName);
        return jobLockStatus;
    }

    public Boolean tryLock(String jobName, String jobMessage) {
        JobLockStatus jobLockStatus = createJobLockStatusByNameAndTime(jobName);
        jobLockStatus.setLocked(true);
        jobLockStatus.setJobStartedTimeNow();
        jobLockStatus.setJobEndTime(null);
        jobLockStatus.setJobMessage(jobMessage);
        return changeJobLockStatus(jobLockStatus);
    }

    public Boolean tryUnlock(String jobName, String jobMessage) {
        JobLockStatus jobLockStatus = createJobLockStatusByNameAndTime(jobName);
        jobLockStatus.setLocked(false);
        jobLockStatus.setJobEndTimeNow();
        jobLockStatus.setJobMessage(jobMessage);
        return changeJobLockStatus(jobLockStatus);
    }

    public Boolean tryResetAfterLockedOneHour(String jobNameId, String jobMessage) {
        JobLockStatus jobLockStatus;
        try {
            jobLockStatus = jobLockStatusRepository.getOne(jobNameId);
            if (jobLockStatus != null && jobLockStatus.getLocked()
                    && jobLockStatus.getJobStartedTime().before(TimestampUtil.lastHour())) {
                jobLockStatus.setLocked(false);
                jobLockStatus.setJobStartedTime(null);
                jobLockStatus.setJobEndTime(null);
                jobLockStatus.setJobMessage(jobMessage);
                return changeJobLockStatus(jobLockStatus);
            }
        } catch (EntityNotFoundException e) {
            return false;
        }
        return false;
    }
}
