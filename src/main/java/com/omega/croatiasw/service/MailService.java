package com.omega.croatiasw.service;

import com.omega.croatiasw.vm.TransactionDetailVM;
import com.omega.croatiasw.vm.TransactionHeaderVM;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.MessageSource;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.thymeleaf.context.Context;
import org.thymeleaf.spring5.SpringTemplateEngine;

import java.nio.charset.StandardCharsets;
import javax.mail.internet.MimeMessage;
import java.util.List;
import java.util.Locale;


/**
 * @author Wenbo at March 21st
 */
@Service
public class MailService {
    private final Logger log = LoggerFactory.getLogger(getClass().getSimpleName());

    private static final String TARGET_EMAIL = "tomislav.smolcic@germaniasport.hr";
    private static final String OMEGA_EMAIL = "support@omegasys.eu";

    private static final String FROM_MAIL = "info@omegasys.eu";

    private static final String TRANSACTIONS = "transactions";

    private final JavaMailSender javaMailSender;

    private final MessageSource messageSource;

    private final SpringTemplateEngine templateEngine;

    private static Long lastFailedTransactionId = 0L;

    public MailService(JavaMailSender javaMailSender,
                       MessageSource messageSource, SpringTemplateEngine templateEngine) {

        this.javaMailSender = javaMailSender;
        this.messageSource = messageSource;
        this.templateEngine = templateEngine;
    }

    @Async
    public void sendEmailFromTemplate(List<TransactionDetailVM> details, String templateName, String titleKey) {
        Locale locale = Locale.forLanguageTag("en");
        Context context = new Context(locale);
        context.setVariable(TRANSACTIONS, details);
        String content = templateEngine.process(templateName, context);
        String subject = messageSource.getMessage(titleKey, null, locale);
        sendEmail(TARGET_EMAIL, subject, content,  true);
    }

    @Async
    public void sendEmail(String to, String subject, String content, boolean isHtml) {
        log.debug("Send email[html '{}'] to '{}' with subject '{}' and content={}",
                isHtml, to, subject, content);

        // Prepare message using a Spring helper
        MimeMessage mimeMessage = javaMailSender.createMimeMessage();

        try {
            MimeMessageHelper message = new MimeMessageHelper(mimeMessage, false, StandardCharsets.UTF_8.name());
            message.setTo(to);
            message.setFrom(FROM_MAIL);
            message.setSubject(subject);
            message.setText(content, isHtml);
            javaMailSender.send(mimeMessage);
            log.debug("Sent email to User '{}'", to);
        } catch (Exception e) {
            if (log.isDebugEnabled()) {
                log.warn("Email could not be sent to user '{}'", to, e);
            } else {
                log.warn("Email could not be sent to user '{}': {}", to, e.getMessage());
            }
        }
    }

    @Async
    public void sendJobCompleteEmail(TransactionHeaderVM transactionHeaderVM, List<TransactionDetailVM> detailvms) {
        log.debug("Sending job completed email");
        switch (transactionHeaderVM.getTransactionType()) {
            case "Product" :
                sendEmailFromTemplate(detailvms, "mail/productChangeEmail", "email.completed.title");
                break;
            case "Game" :
                sendEmailFromTemplate(detailvms, "mail/gameChangeEmail", "email.completed.title");
                break;
            case "Jackpot" :
                sendEmailFromTemplate(detailvms, "mail/jackpotChangeEmail", "email.completed.title");
                break;
            case "TableGameReport" :
                sendEmailFromTemplate(detailvms, "mail/tableGameReportEmail", "email.completed.title");
                break;
            case "SlotGameReport" :
                sendEmailFromTemplate(detailvms, "mail/slotGameReportEmail", "email.completed.title");
                break;
            default:
                break;
        }

    }

    public Boolean hasThisTransactionBeenSent(Long transactionId){
        return lastFailedTransactionId.equals(transactionId);
    }

    @Async
    public void sendJobFailEmail(TransactionHeaderVM headerVM) {
        log.debug("Sending job failed email");
        if(headerVM.getTransactionID() != null){
            lastFailedTransactionId = headerVM.getTransactionID();
        }
        Locale locale = Locale.forLanguageTag("en");
        Context context = new Context(locale);
        context.setVariable(TRANSACTIONS, headerVM);
        String content = templateEngine.process("mail/failEmail", context);
        String subject = messageSource.getMessage("email.failed.title", null, locale);
        sendEmail(TARGET_EMAIL, subject, content,  true);
        sendEmail(OMEGA_EMAIL, subject, content,  true);
    }

    @Async
    public void sendDailyJobFailEmail() {
        log.debug("Sending daily report job failed email");
        Locale locale = Locale.forLanguageTag("en");
        Context context = new Context(locale);
        String content = templateEngine.process("mail/dailyFailEmail", context);
        String subject = messageSource.getMessage("email.failed.title", null, locale);
        sendEmail(TARGET_EMAIL, subject, content,  true);
        sendEmail(OMEGA_EMAIL, subject, content,  true);
    }

    @Async
    public void sendMonthlyJobFailEmail() {
        log.debug("Sending monthly report job failed email");
        Locale locale = Locale.forLanguageTag("en");
        Context context = new Context(locale);
        String content = templateEngine.process("mail/monthlyFailEmail", context);
        String subject = messageSource.getMessage("email.failed.title", null, locale);
        sendEmail(TARGET_EMAIL, subject, content,  true);
        sendEmail(OMEGA_EMAIL, subject, content,  true);
    }

}
