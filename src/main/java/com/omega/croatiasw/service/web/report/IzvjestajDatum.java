
package com.omega.croatiasw.service.web.report;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * Podaci o izvještaju, datum za koji se šalje izvještaj i datum vrijeme kreiranja.
 * 
 * <p>Java class for IzvjestajDatum complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="IzvjestajDatum"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{http://e-porezna.porezna-uprava.hr/casina/v1-0}IzvjestajType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="DatumIzvjestaja" type="{http://e-porezna.porezna-uprava.hr/casina/v1-0}Datum"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "IzvjestajDatum", propOrder = {
    "datumIzvjestaja"
})
public class IzvjestajDatum
    extends IzvjestajType
{

    @XmlElement(name = "DatumIzvjestaja", required = true)
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar datumIzvjestaja;

    /**
     * Gets the value of the datumIzvjestaja property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getDatumIzvjestaja() {
        return datumIzvjestaja;
    }

    /**
     * Sets the value of the datumIzvjestaja property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setDatumIzvjestaja(XMLGregorianCalendar value) {
        this.datumIzvjestaja = value;
    }

}
