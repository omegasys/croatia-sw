
package com.omega.croatiasw.service.web.report;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * Podaci o jackpotu.
 * 
 * <p>Java class for JackpotOpisnikSmanjen complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="JackpotOpisnikSmanjen"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{http://e-porezna.porezna-uprava.hr/casina/v1-0}Opisnik"&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "JackpotOpisnikSmanjen")
public class JackpotOpisnikSmanjen
    extends Opisnik
{


}
