
package com.omega.croatiasw.service.web.report;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.omega.croatiasw.service.web.report package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _EchoRequest_QNAME = new QName("http://e-porezna.porezna-uprava.hr/casina/v1-0", "echoRequest");
    private final static QName _EchoResponse_QNAME = new QName("http://e-porezna.porezna-uprava.hr/casina/v1-0", "echoResponse");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.omega.croatiasw.service.web.report
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link Odgovor }
     * 
     */
    public Odgovor createOdgovor() {
        return new Odgovor();
    }

    /**
     * Create an instance of {@link StatusJackpota }
     * 
     */
    public StatusJackpota createStatusJackpota() {
        return new StatusJackpota();
    }

    /**
     * Create an instance of {@link StatusIgre }
     * 
     */
    public StatusIgre createStatusIgre() {
        return new StatusIgre();
    }

    /**
     * Create an instance of {@link MjesecniObracun }
     * 
     */
    public MjesecniObracun createMjesecniObracun() {
        return new MjesecniObracun();
    }

    /**
     * Create an instance of {@link IzvjestajStolova }
     * 
     */
    public IzvjestajStolova createIzvjestajStolova() {
        return new IzvjestajStolova();
    }

    /**
     * Create an instance of {@link IzvjestajStolova.Igre }
     * 
     */
    public IzvjestajStolova.Igre createIzvjestajStolovaIgre() {
        return new IzvjestajStolova.Igre();
    }

    /**
     * Create an instance of {@link IzvjestajIKonta }
     * 
     */
    public IzvjestajIKonta createIzvjestajIKonta() {
        return new IzvjestajIKonta();
    }

    /**
     * Create an instance of {@link IzvjestajAutomata }
     * 
     */
    public IzvjestajAutomata createIzvjestajAutomata() {
        return new IzvjestajAutomata();
    }

    /**
     * Create an instance of {@link IzvjestajAutomata.Igre }
     * 
     */
    public IzvjestajAutomata.Igre createIzvjestajAutomataIgre() {
        return new IzvjestajAutomata.Igre();
    }

    /**
     * Create an instance of {@link Odgovor.Greske }
     * 
     */
    public Odgovor.Greske createOdgovorGreske() {
        return new Odgovor.Greske();
    }

    /**
     * Create an instance of {@link TransakcijaBrisanje }
     * 
     */
    public TransakcijaBrisanje createTransakcijaBrisanje() {
        return new TransakcijaBrisanje();
    }

    /**
     * Create an instance of {@link ZaglavljeTransakcijaBrisanje }
     * 
     */
    public ZaglavljeTransakcijaBrisanje createZaglavljeTransakcijaBrisanje() {
        return new ZaglavljeTransakcijaBrisanje();
    }

    /**
     * Create an instance of {@link TransakcijaIzvjestajAutomata }
     * 
     */
    public TransakcijaIzvjestajAutomata createTransakcijaIzvjestajAutomata() {
        return new TransakcijaIzvjestajAutomata();
    }

    /**
     * Create an instance of {@link Zaglavlje }
     * 
     */
    public Zaglavlje createZaglavlje() {
        return new Zaglavlje();
    }

    /**
     * Create an instance of {@link TransakcijaIzvjestajIKontaCasina }
     * 
     */
    public TransakcijaIzvjestajIKontaCasina createTransakcijaIzvjestajIKontaCasina() {
        return new TransakcijaIzvjestajIKontaCasina();
    }

    /**
     * Create an instance of {@link TransakcijaIzvjestajStolova }
     * 
     */
    public TransakcijaIzvjestajStolova createTransakcijaIzvjestajStolova() {
        return new TransakcijaIzvjestajStolova();
    }

    /**
     * Create an instance of {@link TransakcijaIzvjestajTurnira }
     * 
     */
    public TransakcijaIzvjestajTurnira createTransakcijaIzvjestajTurnira() {
        return new TransakcijaIzvjestajTurnira();
    }

    /**
     * Create an instance of {@link IzvjestajTurnira }
     * 
     */
    public IzvjestajTurnira createIzvjestajTurnira() {
        return new IzvjestajTurnira();
    }

    /**
     * Create an instance of {@link TransakcijaMjesecniObracun }
     * 
     */
    public TransakcijaMjesecniObracun createTransakcijaMjesecniObracun() {
        return new TransakcijaMjesecniObracun();
    }

    /**
     * Create an instance of {@link TransakcijaStatusCasina }
     * 
     */
    public TransakcijaStatusCasina createTransakcijaStatusCasina() {
        return new TransakcijaStatusCasina();
    }

    /**
     * Create an instance of {@link StatusCasina }
     * 
     */
    public StatusCasina createStatusCasina() {
        return new StatusCasina();
    }

    /**
     * Create an instance of {@link TransakcijaStatusIgre }
     * 
     */
    public TransakcijaStatusIgre createTransakcijaStatusIgre() {
        return new TransakcijaStatusIgre();
    }

    /**
     * Create an instance of {@link TransakcijaStatusJackpota }
     * 
     */
    public TransakcijaStatusJackpota createTransakcijaStatusJackpota() {
        return new TransakcijaStatusJackpota();
    }

    /**
     * Create an instance of {@link CasinoOpisnik }
     * 
     */
    public CasinoOpisnik createCasinoOpisnik() {
        return new CasinoOpisnik();
    }

    /**
     * Create an instance of {@link CasinoOpisnikSmanjen }
     * 
     */
    public CasinoOpisnikSmanjen createCasinoOpisnikSmanjen() {
        return new CasinoOpisnikSmanjen();
    }

    /**
     * Create an instance of {@link IgraOpisnik }
     * 
     */
    public IgraOpisnik createIgraOpisnik() {
        return new IgraOpisnik();
    }

    /**
     * Create an instance of {@link IgraOpisnikSmanjen }
     * 
     */
    public IgraOpisnikSmanjen createIgraOpisnikSmanjen() {
        return new IgraOpisnikSmanjen();
    }

    /**
     * Create an instance of {@link IzvjestajType }
     * 
     */
    public IzvjestajType createIzvjestajType() {
        return new IzvjestajType();
    }

    /**
     * Create an instance of {@link IzvjestajDatum }
     * 
     */
    public IzvjestajDatum createIzvjestajDatum() {
        return new IzvjestajDatum();
    }

    /**
     * Create an instance of {@link IzvjestajMjesec }
     * 
     */
    public IzvjestajMjesec createIzvjestajMjesec() {
        return new IzvjestajMjesec();
    }

    /**
     * Create an instance of {@link JackpotOpisnik }
     * 
     */
    public JackpotOpisnik createJackpotOpisnik() {
        return new JackpotOpisnik();
    }

    /**
     * Create an instance of {@link JackpotOpisnikSmanjen }
     * 
     */
    public JackpotOpisnikSmanjen createJackpotOpisnikSmanjen() {
        return new JackpotOpisnikSmanjen();
    }

    /**
     * Create an instance of {@link Obracun1 }
     * 
     */
    public Obracun1 createObracun1() {
        return new Obracun1();
    }

    /**
     * Create an instance of {@link Obracun2 }
     * 
     */
    public Obracun2 createObracun2() {
        return new Obracun2();
    }

    /**
     * Create an instance of {@link Opisnik }
     * 
     */
    public Opisnik createOpisnik() {
        return new Opisnik();
    }

    /**
     * Create an instance of {@link Priredjivac }
     * 
     */
    public Priredjivac createPriredjivac() {
        return new Priredjivac();
    }

    /**
     * Create an instance of {@link Status }
     * 
     */
    public Status createStatus() {
        return new Status();
    }

    /**
     * Create an instance of {@link PovezanaTransakcija }
     * 
     */
    public PovezanaTransakcija createPovezanaTransakcija() {
        return new PovezanaTransakcija();
    }

    /**
     * Create an instance of {@link StatusJackpota.Casina }
     * 
     */
    public StatusJackpota.Casina createStatusJackpotaCasina() {
        return new StatusJackpota.Casina();
    }

    /**
     * Create an instance of {@link StatusIgre.Jackpotovi }
     * 
     */
    public StatusIgre.Jackpotovi createStatusIgreJackpotovi() {
        return new StatusIgre.Jackpotovi();
    }

    /**
     * Create an instance of {@link MjesecniObracun.IgreNaAutomatima }
     * 
     */
    public MjesecniObracun.IgreNaAutomatima createMjesecniObracunIgreNaAutomatima() {
        return new MjesecniObracun.IgreNaAutomatima();
    }

    /**
     * Create an instance of {@link MjesecniObracun.Stolovi }
     * 
     */
    public MjesecniObracun.Stolovi createMjesecniObracunStolovi() {
        return new MjesecniObracun.Stolovi();
    }

    /**
     * Create an instance of {@link MjesecniObracun.Turniri }
     * 
     */
    public MjesecniObracun.Turniri createMjesecniObracunTurniri() {
        return new MjesecniObracun.Turniri();
    }

    /**
     * Create an instance of {@link MjesecniObracun.Ukupno }
     * 
     */
    public MjesecniObracun.Ukupno createMjesecniObracunUkupno() {
        return new MjesecniObracun.Ukupno();
    }

    /**
     * Create an instance of {@link IzvjestajStolova.Igre.Igra }
     * 
     */
    public IzvjestajStolova.Igre.Igra createIzvjestajStolovaIgreIgra() {
        return new IzvjestajStolova.Igre.Igra();
    }

    /**
     * Create an instance of {@link IzvjestajIKonta.Stanje }
     * 
     */
    public IzvjestajIKonta.Stanje createIzvjestajIKontaStanje() {
        return new IzvjestajIKonta.Stanje();
    }

    /**
     * Create an instance of {@link IzvjestajAutomata.Igre.Igra }
     * 
     */
    public IzvjestajAutomata.Igre.Igra createIzvjestajAutomataIgreIgra() {
        return new IzvjestajAutomata.Igre.Igra();
    }

    /**
     * Create an instance of {@link Odgovor.Greske.Greska }
     * 
     */
    public Odgovor.Greske.Greska createOdgovorGreskeGreska() {
        return new Odgovor.Greske.Greska();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     */
    @XmlElementDecl(namespace = "http://e-porezna.porezna-uprava.hr/casina/v1-0", name = "echoRequest")
    public JAXBElement<String> createEchoRequest(String value) {
        return new JAXBElement<String>(_EchoRequest_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     */
    @XmlElementDecl(namespace = "http://e-porezna.porezna-uprava.hr/casina/v1-0", name = "echoResponse")
    public JAXBElement<String> createEchoResponse(String value) {
        return new JAXBElement<String>(_EchoResponse_QNAME, String.class, null, value);
    }

}
