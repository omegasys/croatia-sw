
package com.omega.croatiasw.service.web.report;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Podaci o jackpotu.
 * 
 * <p>Java class for JackpotOpisnik complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="JackpotOpisnik"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{http://e-porezna.porezna-uprava.hr/casina/v1-0}Opisnik"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="Naziv" type="{http://e-porezna.porezna-uprava.hr/casina/v1-0}NazivDugi"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "JackpotOpisnik", propOrder = {
    "naziv"
})
public class JackpotOpisnik
    extends Opisnik
{

    @XmlElement(name = "Naziv", required = true)
    protected String naziv;

    /**
     * Gets the value of the naziv property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNaziv() {
        return naziv;
    }

    /**
     * Sets the value of the naziv property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNaziv(String value) {
        this.naziv = value;
    }

}
