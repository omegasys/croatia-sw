
package com.omega.croatiasw.service.web.report;

import java.math.BigDecimal;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Podaci o obračunu.
 * 
 * <p>Java class for Obracun2 complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="Obracun2"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="Uplata" type="{http://e-porezna.porezna-uprava.hr/casina/v1-0}Iznos"/&gt;
 *         &lt;element name="Isplata" type="{http://e-porezna.porezna-uprava.hr/casina/v1-0}Iznos"/&gt;
 *         &lt;element name="JackpotAkumulacija" type="{http://e-porezna.porezna-uprava.hr/casina/v1-0}Iznos"/&gt;
 *         &lt;element name="JackpotIsplata" type="{http://e-porezna.porezna-uprava.hr/casina/v1-0}Iznos"/&gt;
 *         &lt;element name="Razlika" type="{http://e-porezna.porezna-uprava.hr/casina/v1-0}Iznos"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Obracun2", propOrder = {
    "uplata",
    "isplata",
    "jackpotAkumulacija",
    "jackpotIsplata",
    "razlika"
})
public class Obracun2 {

    @XmlElement(name = "Uplata", required = true)
    protected BigDecimal uplata;
    @XmlElement(name = "Isplata", required = true)
    protected BigDecimal isplata;
    @XmlElement(name = "JackpotAkumulacija", required = true)
    protected BigDecimal jackpotAkumulacija;
    @XmlElement(name = "JackpotIsplata", required = true)
    protected BigDecimal jackpotIsplata;
    @XmlElement(name = "Razlika", required = true)
    protected BigDecimal razlika;

    /**
     * Gets the value of the uplata property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getUplata() {
        return uplata;
    }

    /**
     * Sets the value of the uplata property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setUplata(BigDecimal value) {
        this.uplata = value;
    }

    /**
     * Gets the value of the isplata property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getIsplata() {
        return isplata;
    }

    /**
     * Sets the value of the isplata property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setIsplata(BigDecimal value) {
        this.isplata = value;
    }

    /**
     * Gets the value of the jackpotAkumulacija property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getJackpotAkumulacija() {
        return jackpotAkumulacija;
    }

    /**
     * Sets the value of the jackpotAkumulacija property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setJackpotAkumulacija(BigDecimal value) {
        this.jackpotAkumulacija = value;
    }

    /**
     * Gets the value of the jackpotIsplata property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getJackpotIsplata() {
        return jackpotIsplata;
    }

    /**
     * Sets the value of the jackpotIsplata property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setJackpotIsplata(BigDecimal value) {
        this.jackpotIsplata = value;
    }

    /**
     * Gets the value of the razlika property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getRazlika() {
        return razlika;
    }

    /**
     * Sets the value of the razlika property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setRazlika(BigDecimal value) {
        this.razlika = value;
    }

}
