
package com.omega.croatiasw.service.web.status;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Svaka se igra prije korištenja treba prijaviti u
 * 				Ministarstvo financija i aktivirati. Igra se prijavljuje svojom
 * 				jedinstvenom šifrom, vrstom i nazivom. U slučaju da igra ima svoj
 * 				jackpot ili sudjeluje u jackpotu zajedno s drugim igrama tada je
 * 				obavezan i dio podataka vezanih uz jackpot.
 * 
 * <p>Java class for StatusIgre complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="StatusIgre"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="Igra" type="{http://e-porezna.porezna-uprava.hr/casina/v1-0}IgraOpisnik"/&gt;
 *         &lt;element name="Casino" type="{http://e-porezna.porezna-uprava.hr/casina/v1-0}CasinoOpisnikSmanjen"/&gt;
 *         &lt;element name="Jackpotovi" minOccurs="0"&gt;
 *           &lt;complexType&gt;
 *             &lt;complexContent&gt;
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                 &lt;sequence&gt;
 *                   &lt;element name="Jackpot" type="{http://e-porezna.porezna-uprava.hr/casina/v1-0}JackpotOpisnikSmanjen" maxOccurs="unbounded"/&gt;
 *                 &lt;/sequence&gt;
 *               &lt;/restriction&gt;
 *             &lt;/complexContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *         &lt;element name="Status" type="{http://e-porezna.porezna-uprava.hr/casina/v1-0}Status"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "StatusIgre", propOrder = {
    "igra",
    "casino",
    "jackpotovi",
    "status"
})
public class StatusIgre {

    @XmlElement(name = "Igra", required = true)
    protected IgraOpisnik igra;
    @XmlElement(name = "Casino", required = true)
    protected CasinoOpisnikSmanjen casino;
    @XmlElement(name = "Jackpotovi")
    protected StatusIgre.Jackpotovi jackpotovi;
    @XmlElement(name = "Status", required = true)
    protected Status status;

    /**
     * Gets the value of the igra property.
     * 
     * @return
     *     possible object is
     *     {@link IgraOpisnik }
     *     
     */
    public IgraOpisnik getIgra() {
        return igra;
    }

    /**
     * Sets the value of the igra property.
     * 
     * @param value
     *     allowed object is
     *     {@link IgraOpisnik }
     *     
     */
    public void setIgra(IgraOpisnik value) {
        this.igra = value;
    }

    /**
     * Gets the value of the casino property.
     * 
     * @return
     *     possible object is
     *     {@link CasinoOpisnikSmanjen }
     *     
     */
    public CasinoOpisnikSmanjen getCasino() {
        return casino;
    }

    /**
     * Sets the value of the casino property.
     * 
     * @param value
     *     allowed object is
     *     {@link CasinoOpisnikSmanjen }
     *     
     */
    public void setCasino(CasinoOpisnikSmanjen value) {
        this.casino = value;
    }

    /**
     * Gets the value of the jackpotovi property.
     * 
     * @return
     *     possible object is
     *     {@link StatusIgre.Jackpotovi }
     *     
     */
    public StatusIgre.Jackpotovi getJackpotovi() {
        return jackpotovi;
    }

    /**
     * Sets the value of the jackpotovi property.
     * 
     * @param value
     *     allowed object is
     *     {@link StatusIgre.Jackpotovi }
     *     
     */
    public void setJackpotovi(StatusIgre.Jackpotovi value) {
        this.jackpotovi = value;
    }

    /**
     * Gets the value of the status property.
     * 
     * @return
     *     possible object is
     *     {@link Status }
     *     
     */
    public Status getStatus() {
        return status;
    }

    /**
     * Sets the value of the status property.
     * 
     * @param value
     *     allowed object is
     *     {@link Status }
     *     
     */
    public void setStatus(Status value) {
        this.status = value;
    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType&gt;
     *   &lt;complexContent&gt;
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *       &lt;sequence&gt;
     *         &lt;element name="Jackpot" type="{http://e-porezna.porezna-uprava.hr/casina/v1-0}JackpotOpisnikSmanjen" maxOccurs="unbounded"/&gt;
     *       &lt;/sequence&gt;
     *     &lt;/restriction&gt;
     *   &lt;/complexContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "jackpot"
    })
    public static class Jackpotovi {

        @XmlElement(name = "Jackpot", required = true)
        protected List<JackpotOpisnikSmanjen> jackpot;

        /**
         * Gets the value of the jackpot property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the jackpot property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getJackpot().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link JackpotOpisnikSmanjen }
         * 
         * 
         */
        public List<JackpotOpisnikSmanjen> getJackpot() {
            if (jackpot == null) {
                jackpot = new ArrayList<JackpotOpisnikSmanjen>();
            }
            return this.jackpot;
        }

    }

}
