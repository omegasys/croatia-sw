
package com.omega.croatiasw.service.web.status;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="Zaglavlje" type="{http://e-porezna.porezna-uprava.hr/casina/v1-0}Zaglavlje"/&gt;
 *         &lt;element name="IzvjestajStolova" type="{http://e-porezna.porezna-uprava.hr/casina/v1-0}IzvjestajStolova"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "zaglavlje",
    "izvjestajStolova"
})
@XmlRootElement(name = "TransakcijaIzvjestajStolova")
public class TransakcijaIzvjestajStolova {

    @XmlElement(name = "Zaglavlje", required = true)
    protected Zaglavlje zaglavlje;
    @XmlElement(name = "IzvjestajStolova", required = true)
    protected IzvjestajStolova izvjestajStolova;

    /**
     * Gets the value of the zaglavlje property.
     * 
     * @return
     *     possible object is
     *     {@link Zaglavlje }
     *     
     */
    public Zaglavlje getZaglavlje() {
        return zaglavlje;
    }

    /**
     * Sets the value of the zaglavlje property.
     * 
     * @param value
     *     allowed object is
     *     {@link Zaglavlje }
     *     
     */
    public void setZaglavlje(Zaglavlje value) {
        this.zaglavlje = value;
    }

    /**
     * Gets the value of the izvjestajStolova property.
     * 
     * @return
     *     possible object is
     *     {@link IzvjestajStolova }
     *     
     */
    public IzvjestajStolova getIzvjestajStolova() {
        return izvjestajStolova;
    }

    /**
     * Sets the value of the izvjestajStolova property.
     * 
     * @param value
     *     allowed object is
     *     {@link IzvjestajStolova }
     *     
     */
    public void setIzvjestajStolova(IzvjestajStolova value) {
        this.izvjestajStolova = value;
    }

}
