
package com.omega.croatiasw.service.web.status;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Izvještaj casina o igrama na automatima šalje se
 * 				jednom dnevno. Izvještaj obuhvaća sve igre koje spadaju u kategoriju
 * 				igara na automatima.
 * 
 * <p>Java class for IzvjestajAutomata complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="IzvjestajAutomata"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="Izvjestaj" type="{http://e-porezna.porezna-uprava.hr/casina/v1-0}IzvjestajDatum"/&gt;
 *         &lt;element name="Casino" type="{http://e-porezna.porezna-uprava.hr/casina/v1-0}CasinoOpisnikSmanjen"/&gt;
 *         &lt;element name="Igre" minOccurs="0"&gt;
 *           &lt;complexType&gt;
 *             &lt;complexContent&gt;
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                 &lt;sequence&gt;
 *                   &lt;element name="Igra" maxOccurs="unbounded"&gt;
 *                     &lt;complexType&gt;
 *                       &lt;complexContent&gt;
 *                         &lt;extension base="{http://e-porezna.porezna-uprava.hr/casina/v1-0}IgraOpisnikSmanjen"&gt;
 *                           &lt;sequence&gt;
 *                             &lt;element name="Obracun" type="{http://e-porezna.porezna-uprava.hr/casina/v1-0}Obracun2"/&gt;
 *                           &lt;/sequence&gt;
 *                         &lt;/extension&gt;
 *                       &lt;/complexContent&gt;
 *                     &lt;/complexType&gt;
 *                   &lt;/element&gt;
 *                 &lt;/sequence&gt;
 *               &lt;/restriction&gt;
 *             &lt;/complexContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *         &lt;element name="Obracun" type="{http://e-porezna.porezna-uprava.hr/casina/v1-0}Obracun2"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "IzvjestajAutomata", propOrder = {
    "izvjestaj",
    "casino",
    "igre",
    "obracun"
})
public class IzvjestajAutomata {

    @XmlElement(name = "Izvjestaj", required = true)
    protected IzvjestajDatum izvjestaj;
    @XmlElement(name = "Casino", required = true)
    protected CasinoOpisnikSmanjen casino;
    @XmlElement(name = "Igre")
    protected IzvjestajAutomata.Igre igre;
    @XmlElement(name = "Obracun", required = true)
    protected Obracun2 obracun;

    /**
     * Gets the value of the izvjestaj property.
     * 
     * @return
     *     possible object is
     *     {@link IzvjestajDatum }
     *     
     */
    public IzvjestajDatum getIzvjestaj() {
        return izvjestaj;
    }

    /**
     * Sets the value of the izvjestaj property.
     * 
     * @param value
     *     allowed object is
     *     {@link IzvjestajDatum }
     *     
     */
    public void setIzvjestaj(IzvjestajDatum value) {
        this.izvjestaj = value;
    }

    /**
     * Gets the value of the casino property.
     * 
     * @return
     *     possible object is
     *     {@link CasinoOpisnikSmanjen }
     *     
     */
    public CasinoOpisnikSmanjen getCasino() {
        return casino;
    }

    /**
     * Sets the value of the casino property.
     * 
     * @param value
     *     allowed object is
     *     {@link CasinoOpisnikSmanjen }
     *     
     */
    public void setCasino(CasinoOpisnikSmanjen value) {
        this.casino = value;
    }

    /**
     * Gets the value of the igre property.
     * 
     * @return
     *     possible object is
     *     {@link IzvjestajAutomata.Igre }
     *     
     */
    public IzvjestajAutomata.Igre getIgre() {
        return igre;
    }

    /**
     * Sets the value of the igre property.
     * 
     * @param value
     *     allowed object is
     *     {@link IzvjestajAutomata.Igre }
     *     
     */
    public void setIgre(IzvjestajAutomata.Igre value) {
        this.igre = value;
    }

    /**
     * Gets the value of the obracun property.
     * 
     * @return
     *     possible object is
     *     {@link Obracun2 }
     *     
     */
    public Obracun2 getObracun() {
        return obracun;
    }

    /**
     * Sets the value of the obracun property.
     * 
     * @param value
     *     allowed object is
     *     {@link Obracun2 }
     *     
     */
    public void setObracun(Obracun2 value) {
        this.obracun = value;
    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType&gt;
     *   &lt;complexContent&gt;
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *       &lt;sequence&gt;
     *         &lt;element name="Igra" maxOccurs="unbounded"&gt;
     *           &lt;complexType&gt;
     *             &lt;complexContent&gt;
     *               &lt;extension base="{http://e-porezna.porezna-uprava.hr/casina/v1-0}IgraOpisnikSmanjen"&gt;
     *                 &lt;sequence&gt;
     *                   &lt;element name="Obracun" type="{http://e-porezna.porezna-uprava.hr/casina/v1-0}Obracun2"/&gt;
     *                 &lt;/sequence&gt;
     *               &lt;/extension&gt;
     *             &lt;/complexContent&gt;
     *           &lt;/complexType&gt;
     *         &lt;/element&gt;
     *       &lt;/sequence&gt;
     *     &lt;/restriction&gt;
     *   &lt;/complexContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "igra"
    })
    public static class Igre {

        @XmlElement(name = "Igra", required = true)
        protected List<IzvjestajAutomata.Igre.Igra> igra;

        /**
         * Gets the value of the igra property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the igra property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getIgra().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link IzvjestajAutomata.Igre.Igra }
         * 
         * 
         */
        public List<IzvjestajAutomata.Igre.Igra> getIgra() {
            if (igra == null) {
                igra = new ArrayList<IzvjestajAutomata.Igre.Igra>();
            }
            return this.igra;
        }


        /**
         * <p>Java class for anonymous complex type.
         * 
         * <p>The following schema fragment specifies the expected content contained within this class.
         * 
         * <pre>
         * &lt;complexType&gt;
         *   &lt;complexContent&gt;
         *     &lt;extension base="{http://e-porezna.porezna-uprava.hr/casina/v1-0}IgraOpisnikSmanjen"&gt;
         *       &lt;sequence&gt;
         *         &lt;element name="Obracun" type="{http://e-porezna.porezna-uprava.hr/casina/v1-0}Obracun2"/&gt;
         *       &lt;/sequence&gt;
         *     &lt;/extension&gt;
         *   &lt;/complexContent&gt;
         * &lt;/complexType&gt;
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "obracun"
        })
        public static class Igra
            extends IgraOpisnikSmanjen
        {

            @XmlElement(name = "Obracun", required = true)
            protected Obracun2 obracun;

            /**
             * Gets the value of the obracun property.
             * 
             * @return
             *     possible object is
             *     {@link Obracun2 }
             *     
             */
            public Obracun2 getObracun() {
                return obracun;
            }

            /**
             * Sets the value of the obracun property.
             * 
             * @param value
             *     allowed object is
             *     {@link Obracun2 }
             *     
             */
            public void setObracun(Obracun2 value) {
                this.obracun = value;
            }

        }

    }

}
