
package com.omega.croatiasw.service.web.status;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Svaki jackpot mora imati jedinstvenu šifru pod
 * 				kojom se prijavljuje Ministarstvu financija. Prilikom aktivacije
 * 				jackpota mora se dostaviti i početni iznos koji se na jackpotu
 * 				nalazi.
 * 
 * <p>Java class for StatusJackpota complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="StatusJackpota"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="Jackpot" type="{http://e-porezna.porezna-uprava.hr/casina/v1-0}JackpotOpisnik"/&gt;
 *         &lt;element name="Casina"&gt;
 *           &lt;complexType&gt;
 *             &lt;complexContent&gt;
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                 &lt;sequence&gt;
 *                   &lt;element name="Casino" type="{http://e-porezna.porezna-uprava.hr/casina/v1-0}CasinoOpisnikSmanjen" maxOccurs="unbounded"/&gt;
 *                 &lt;/sequence&gt;
 *               &lt;/restriction&gt;
 *             &lt;/complexContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *         &lt;element name="Status" type="{http://e-porezna.porezna-uprava.hr/casina/v1-0}Status"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "StatusJackpota", propOrder = {
    "jackpot",
    "casina",
    "status"
})
public class StatusJackpota {

    @XmlElement(name = "Jackpot", required = true)
    protected JackpotOpisnik jackpot;
    @XmlElement(name = "Casina", required = true)
    protected StatusJackpota.Casina casina;
    @XmlElement(name = "Status", required = true)
    protected Status status;

    /**
     * Gets the value of the jackpot property.
     * 
     * @return
     *     possible object is
     *     {@link JackpotOpisnik }
     *     
     */
    public JackpotOpisnik getJackpot() {
        return jackpot;
    }

    /**
     * Sets the value of the jackpot property.
     * 
     * @param value
     *     allowed object is
     *     {@link JackpotOpisnik }
     *     
     */
    public void setJackpot(JackpotOpisnik value) {
        this.jackpot = value;
    }

    /**
     * Gets the value of the casina property.
     * 
     * @return
     *     possible object is
     *     {@link StatusJackpota.Casina }
     *     
     */
    public StatusJackpota.Casina getCasina() {
        return casina;
    }

    /**
     * Sets the value of the casina property.
     * 
     * @param value
     *     allowed object is
     *     {@link StatusJackpota.Casina }
     *     
     */
    public void setCasina(StatusJackpota.Casina value) {
        this.casina = value;
    }

    /**
     * Gets the value of the status property.
     * 
     * @return
     *     possible object is
     *     {@link Status }
     *     
     */
    public Status getStatus() {
        return status;
    }

    /**
     * Sets the value of the status property.
     * 
     * @param value
     *     allowed object is
     *     {@link Status }
     *     
     */
    public void setStatus(Status value) {
        this.status = value;
    }


    /**
     * Lista koja sadrži jedan ili više kasina.
     * 						
     * 
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType&gt;
     *   &lt;complexContent&gt;
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *       &lt;sequence&gt;
     *         &lt;element name="Casino" type="{http://e-porezna.porezna-uprava.hr/casina/v1-0}CasinoOpisnikSmanjen" maxOccurs="unbounded"/&gt;
     *       &lt;/sequence&gt;
     *     &lt;/restriction&gt;
     *   &lt;/complexContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "casino"
    })
    public static class Casina {

        @XmlElement(name = "Casino", required = true)
        protected List<CasinoOpisnikSmanjen> casino;

        /**
         * Gets the value of the casino property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the casino property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getCasino().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link CasinoOpisnikSmanjen }
         * 
         * 
         */
        public List<CasinoOpisnikSmanjen> getCasino() {
            if (casino == null) {
                casino = new ArrayList<CasinoOpisnikSmanjen>();
            }
            return this.casino;
        }

    }

}
