
package com.omega.croatiasw.service.web.status;

import java.math.BigDecimal;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Podaci o obračunu.
 * 
 * <p>Java class for Obracun1 complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="Obracun1"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="Uplata" type="{http://e-porezna.porezna-uprava.hr/casina/v1-0}Iznos"/&gt;
 *         &lt;element name="Isplata" type="{http://e-porezna.porezna-uprava.hr/casina/v1-0}Iznos"/&gt;
 *         &lt;element name="Provizija" type="{http://e-porezna.porezna-uprava.hr/casina/v1-0}Iznos"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Obracun1", propOrder = {
    "uplata",
    "isplata",
    "provizija"
})
public class Obracun1 {

    @XmlElement(name = "Uplata", required = true)
    protected BigDecimal uplata;
    @XmlElement(name = "Isplata", required = true)
    protected BigDecimal isplata;
    @XmlElement(name = "Provizija", required = true)
    protected BigDecimal provizija;

    /**
     * Gets the value of the uplata property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getUplata() {
        return uplata;
    }

    /**
     * Sets the value of the uplata property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setUplata(BigDecimal value) {
        this.uplata = value;
    }

    /**
     * Gets the value of the isplata property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getIsplata() {
        return isplata;
    }

    /**
     * Sets the value of the isplata property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setIsplata(BigDecimal value) {
        this.isplata = value;
    }

    /**
     * Gets the value of the provizija property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getProvizija() {
        return provizija;
    }

    /**
     * Sets the value of the provizija property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setProvizija(BigDecimal value) {
        this.provizija = value;
    }

}
