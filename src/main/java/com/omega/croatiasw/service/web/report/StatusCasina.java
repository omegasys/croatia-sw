
package com.omega.croatiasw.service.web.report;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Prije slanja bilo kakvih transakcija vezanih uz
 * 				igru ili jackpot priređivač je dužan aktivirati casino slanjem
 * 				transakcije StatusCasina u Ministarstvo financija kojom registrira
 * 				casino.
 * 
 * <p>Java class for StatusCasina complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="StatusCasina"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="Casino" type="{http://e-porezna.porezna-uprava.hr/casina/v1-0}CasinoOpisnik"/&gt;
 *         &lt;element name="Priredjivac" type="{http://e-porezna.porezna-uprava.hr/casina/v1-0}Priredjivac"/&gt;
 *         &lt;element name="Status" type="{http://e-porezna.porezna-uprava.hr/casina/v1-0}Status"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "StatusCasina", propOrder = {
    "casino",
    "priredjivac",
    "status"
})
public class StatusCasina {

    @XmlElement(name = "Casino", required = true)
    protected CasinoOpisnik casino;
    @XmlElement(name = "Priredjivac", required = true)
    protected Priredjivac priredjivac;
    @XmlElement(name = "Status", required = true)
    protected Status status;

    /**
     * Gets the value of the casino property.
     * 
     * @return
     *     possible object is
     *     {@link CasinoOpisnik }
     *     
     */
    public CasinoOpisnik getCasino() {
        return casino;
    }

    /**
     * Sets the value of the casino property.
     * 
     * @param value
     *     allowed object is
     *     {@link CasinoOpisnik }
     *     
     */
    public void setCasino(CasinoOpisnik value) {
        this.casino = value;
    }

    /**
     * Gets the value of the priredjivac property.
     * 
     * @return
     *     possible object is
     *     {@link Priredjivac }
     *     
     */
    public Priredjivac getPriredjivac() {
        return priredjivac;
    }

    /**
     * Sets the value of the priredjivac property.
     * 
     * @param value
     *     allowed object is
     *     {@link Priredjivac }
     *     
     */
    public void setPriredjivac(Priredjivac value) {
        this.priredjivac = value;
    }

    /**
     * Gets the value of the status property.
     * 
     * @return
     *     possible object is
     *     {@link Status }
     *     
     */
    public Status getStatus() {
        return status;
    }

    /**
     * Sets the value of the status property.
     * 
     * @param value
     *     allowed object is
     *     {@link Status }
     *     
     */
    public void setStatus(Status value) {
        this.status = value;
    }

}
