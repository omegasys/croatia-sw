
package com.omega.croatiasw.service.web.report;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="Zaglavlje" type="{http://e-porezna.porezna-uprava.hr/casina/v1-0}Zaglavlje"/&gt;
 *         &lt;element name="IzvjestajIKontaCasina" type="{http://e-porezna.porezna-uprava.hr/casina/v1-0}IzvjestajIKonta"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "zaglavlje",
    "izvjestajIKontaCasina"
})
@XmlRootElement(name = "TransakcijaIzvjestajIKontaCasina")
public class TransakcijaIzvjestajIKontaCasina {

    @XmlElement(name = "Zaglavlje", required = true)
    protected Zaglavlje zaglavlje;
    @XmlElement(name = "IzvjestajIKontaCasina", required = true)
    protected IzvjestajIKonta izvjestajIKontaCasina;

    /**
     * Gets the value of the zaglavlje property.
     * 
     * @return
     *     possible object is
     *     {@link Zaglavlje }
     *     
     */
    public Zaglavlje getZaglavlje() {
        return zaglavlje;
    }

    /**
     * Sets the value of the zaglavlje property.
     * 
     * @param value
     *     allowed object is
     *     {@link Zaglavlje }
     *     
     */
    public void setZaglavlje(Zaglavlje value) {
        this.zaglavlje = value;
    }

    /**
     * Gets the value of the izvjestajIKontaCasina property.
     * 
     * @return
     *     possible object is
     *     {@link IzvjestajIKonta }
     *     
     */
    public IzvjestajIKonta getIzvjestajIKontaCasina() {
        return izvjestajIKontaCasina;
    }

    /**
     * Sets the value of the izvjestajIKontaCasina property.
     * 
     * @param value
     *     allowed object is
     *     {@link IzvjestajIKonta }
     *     
     */
    public void setIzvjestajIKontaCasina(IzvjestajIKonta value) {
        this.izvjestajIKontaCasina = value;
    }

}
