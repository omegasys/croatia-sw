
package com.omega.croatiasw.service.web.status;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlType;


/**
 * Podaci o izvještaju.
 * 
 * <p>Java class for IzvjestajMjesec complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="IzvjestajMjesec"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{http://e-porezna.porezna-uprava.hr/casina/v1-0}IzvjestajType"&gt;
 *       &lt;attribute name="mjesec" use="required" type="{http://e-porezna.porezna-uprava.hr/casina/v1-0}Mjesec" /&gt;
 *       &lt;attribute name="godina" use="required" type="{http://e-porezna.porezna-uprava.hr/casina/v1-0}Godina" /&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "IzvjestajMjesec")
public class IzvjestajMjesec
    extends IzvjestajType
{

    @XmlAttribute(name = "mjesec", required = true)
    protected String mjesec;
    @XmlAttribute(name = "godina", required = true)
    protected String godina;

    /**
     * Gets the value of the mjesec property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMjesec() {
        return mjesec;
    }

    /**
     * Sets the value of the mjesec property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMjesec(String value) {
        this.mjesec = value;
    }

    /**
     * Gets the value of the godina property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getGodina() {
        return godina;
    }

    /**
     * Sets the value of the godina property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setGodina(String value) {
        this.godina = value;
    }

}
