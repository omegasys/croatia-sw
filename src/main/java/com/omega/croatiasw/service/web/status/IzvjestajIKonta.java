
package com.omega.croatiasw.service.web.status;

import java.math.BigDecimal;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Izvještaj obuhvaća sve transakcije koje su se
 * 				odvijale po i-kontu priređivača na kojem igrači imaju pohranjena
 * 				svoja sredstva za igru.
 * 
 * <p>Java class for IzvjestajIKonta complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="IzvjestajIKonta"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="Izvjestaj" type="{http://e-porezna.porezna-uprava.hr/casina/v1-0}IzvjestajDatum"/&gt;
 *         &lt;element name="Casino" type="{http://e-porezna.porezna-uprava.hr/casina/v1-0}CasinoOpisnikSmanjen"/&gt;
 *         &lt;element name="Stanje"&gt;
 *           &lt;complexType&gt;
 *             &lt;complexContent&gt;
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                 &lt;sequence&gt;
 *                   &lt;element name="PocetnoStanjeIKontaCasina" type="{http://e-porezna.porezna-uprava.hr/casina/v1-0}Iznos"/&gt;
 *                   &lt;element name="IznosUplataNaIKonto" type="{http://e-porezna.porezna-uprava.hr/casina/v1-0}Iznos"/&gt;
 *                   &lt;element name="IznosIsplataSIKonta" type="{http://e-porezna.porezna-uprava.hr/casina/v1-0}Iznos"/&gt;
 *                   &lt;element name="IznosUplataIgara" type="{http://e-porezna.porezna-uprava.hr/casina/v1-0}Iznos"/&gt;
 *                   &lt;element name="IznosIsplataIgara" type="{http://e-porezna.porezna-uprava.hr/casina/v1-0}Iznos"/&gt;
 *                   &lt;element name="IznosUplataOstalo" type="{http://e-porezna.porezna-uprava.hr/casina/v1-0}Iznos"/&gt;
 *                   &lt;element name="IznosIsplataOstalo" type="{http://e-porezna.porezna-uprava.hr/casina/v1-0}Iznos"/&gt;
 *                   &lt;element name="ZavrsnoStanjeIKontaCasina" type="{http://e-porezna.porezna-uprava.hr/casina/v1-0}Iznos"/&gt;
 *                 &lt;/sequence&gt;
 *               &lt;/restriction&gt;
 *             &lt;/complexContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "IzvjestajIKonta", propOrder = {
    "izvjestaj",
    "casino",
    "stanje"
})
public class IzvjestajIKonta {

    @XmlElement(name = "Izvjestaj", required = true)
    protected IzvjestajDatum izvjestaj;
    @XmlElement(name = "Casino", required = true)
    protected CasinoOpisnikSmanjen casino;
    @XmlElement(name = "Stanje", required = true)
    protected IzvjestajIKonta.Stanje stanje;

    /**
     * Gets the value of the izvjestaj property.
     * 
     * @return
     *     possible object is
     *     {@link IzvjestajDatum }
     *     
     */
    public IzvjestajDatum getIzvjestaj() {
        return izvjestaj;
    }

    /**
     * Sets the value of the izvjestaj property.
     * 
     * @param value
     *     allowed object is
     *     {@link IzvjestajDatum }
     *     
     */
    public void setIzvjestaj(IzvjestajDatum value) {
        this.izvjestaj = value;
    }

    /**
     * Gets the value of the casino property.
     * 
     * @return
     *     possible object is
     *     {@link CasinoOpisnikSmanjen }
     *     
     */
    public CasinoOpisnikSmanjen getCasino() {
        return casino;
    }

    /**
     * Sets the value of the casino property.
     * 
     * @param value
     *     allowed object is
     *     {@link CasinoOpisnikSmanjen }
     *     
     */
    public void setCasino(CasinoOpisnikSmanjen value) {
        this.casino = value;
    }

    /**
     * Gets the value of the stanje property.
     * 
     * @return
     *     possible object is
     *     {@link IzvjestajIKonta.Stanje }
     *     
     */
    public IzvjestajIKonta.Stanje getStanje() {
        return stanje;
    }

    /**
     * Sets the value of the stanje property.
     * 
     * @param value
     *     allowed object is
     *     {@link IzvjestajIKonta.Stanje }
     *     
     */
    public void setStanje(IzvjestajIKonta.Stanje value) {
        this.stanje = value;
    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType&gt;
     *   &lt;complexContent&gt;
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *       &lt;sequence&gt;
     *         &lt;element name="PocetnoStanjeIKontaCasina" type="{http://e-porezna.porezna-uprava.hr/casina/v1-0}Iznos"/&gt;
     *         &lt;element name="IznosUplataNaIKonto" type="{http://e-porezna.porezna-uprava.hr/casina/v1-0}Iznos"/&gt;
     *         &lt;element name="IznosIsplataSIKonta" type="{http://e-porezna.porezna-uprava.hr/casina/v1-0}Iznos"/&gt;
     *         &lt;element name="IznosUplataIgara" type="{http://e-porezna.porezna-uprava.hr/casina/v1-0}Iznos"/&gt;
     *         &lt;element name="IznosIsplataIgara" type="{http://e-porezna.porezna-uprava.hr/casina/v1-0}Iznos"/&gt;
     *         &lt;element name="IznosUplataOstalo" type="{http://e-porezna.porezna-uprava.hr/casina/v1-0}Iznos"/&gt;
     *         &lt;element name="IznosIsplataOstalo" type="{http://e-porezna.porezna-uprava.hr/casina/v1-0}Iznos"/&gt;
     *         &lt;element name="ZavrsnoStanjeIKontaCasina" type="{http://e-porezna.porezna-uprava.hr/casina/v1-0}Iznos"/&gt;
     *       &lt;/sequence&gt;
     *     &lt;/restriction&gt;
     *   &lt;/complexContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "pocetnoStanjeIKontaCasina",
        "iznosUplataNaIKonto",
        "iznosIsplataSIKonta",
        "iznosUplataIgara",
        "iznosIsplataIgara",
        "iznosUplataOstalo",
        "iznosIsplataOstalo",
        "zavrsnoStanjeIKontaCasina"
    })
    public static class Stanje {

        @XmlElement(name = "PocetnoStanjeIKontaCasina", required = true)
        protected BigDecimal pocetnoStanjeIKontaCasina;
        @XmlElement(name = "IznosUplataNaIKonto", required = true)
        protected BigDecimal iznosUplataNaIKonto;
        @XmlElement(name = "IznosIsplataSIKonta", required = true)
        protected BigDecimal iznosIsplataSIKonta;
        @XmlElement(name = "IznosUplataIgara", required = true)
        protected BigDecimal iznosUplataIgara;
        @XmlElement(name = "IznosIsplataIgara", required = true)
        protected BigDecimal iznosIsplataIgara;
        @XmlElement(name = "IznosUplataOstalo", required = true)
        protected BigDecimal iznosUplataOstalo;
        @XmlElement(name = "IznosIsplataOstalo", required = true)
        protected BigDecimal iznosIsplataOstalo;
        @XmlElement(name = "ZavrsnoStanjeIKontaCasina", required = true)
        protected BigDecimal zavrsnoStanjeIKontaCasina;

        /**
         * Gets the value of the pocetnoStanjeIKontaCasina property.
         * 
         * @return
         *     possible object is
         *     {@link BigDecimal }
         *     
         */
        public BigDecimal getPocetnoStanjeIKontaCasina() {
            return pocetnoStanjeIKontaCasina;
        }

        /**
         * Sets the value of the pocetnoStanjeIKontaCasina property.
         * 
         * @param value
         *     allowed object is
         *     {@link BigDecimal }
         *     
         */
        public void setPocetnoStanjeIKontaCasina(BigDecimal value) {
            this.pocetnoStanjeIKontaCasina = value;
        }

        /**
         * Gets the value of the iznosUplataNaIKonto property.
         * 
         * @return
         *     possible object is
         *     {@link BigDecimal }
         *     
         */
        public BigDecimal getIznosUplataNaIKonto() {
            return iznosUplataNaIKonto;
        }

        /**
         * Sets the value of the iznosUplataNaIKonto property.
         * 
         * @param value
         *     allowed object is
         *     {@link BigDecimal }
         *     
         */
        public void setIznosUplataNaIKonto(BigDecimal value) {
            this.iznosUplataNaIKonto = value;
        }

        /**
         * Gets the value of the iznosIsplataSIKonta property.
         * 
         * @return
         *     possible object is
         *     {@link BigDecimal }
         *     
         */
        public BigDecimal getIznosIsplataSIKonta() {
            return iznosIsplataSIKonta;
        }

        /**
         * Sets the value of the iznosIsplataSIKonta property.
         * 
         * @param value
         *     allowed object is
         *     {@link BigDecimal }
         *     
         */
        public void setIznosIsplataSIKonta(BigDecimal value) {
            this.iznosIsplataSIKonta = value;
        }

        /**
         * Gets the value of the iznosUplataIgara property.
         * 
         * @return
         *     possible object is
         *     {@link BigDecimal }
         *     
         */
        public BigDecimal getIznosUplataIgara() {
            return iznosUplataIgara;
        }

        /**
         * Sets the value of the iznosUplataIgara property.
         * 
         * @param value
         *     allowed object is
         *     {@link BigDecimal }
         *     
         */
        public void setIznosUplataIgara(BigDecimal value) {
            this.iznosUplataIgara = value;
        }

        /**
         * Gets the value of the iznosIsplataIgara property.
         * 
         * @return
         *     possible object is
         *     {@link BigDecimal }
         *     
         */
        public BigDecimal getIznosIsplataIgara() {
            return iznosIsplataIgara;
        }

        /**
         * Sets the value of the iznosIsplataIgara property.
         * 
         * @param value
         *     allowed object is
         *     {@link BigDecimal }
         *     
         */
        public void setIznosIsplataIgara(BigDecimal value) {
            this.iznosIsplataIgara = value;
        }

        /**
         * Gets the value of the iznosUplataOstalo property.
         * 
         * @return
         *     possible object is
         *     {@link BigDecimal }
         *     
         */
        public BigDecimal getIznosUplataOstalo() {
            return iznosUplataOstalo;
        }

        /**
         * Sets the value of the iznosUplataOstalo property.
         * 
         * @param value
         *     allowed object is
         *     {@link BigDecimal }
         *     
         */
        public void setIznosUplataOstalo(BigDecimal value) {
            this.iznosUplataOstalo = value;
        }

        /**
         * Gets the value of the iznosIsplataOstalo property.
         * 
         * @return
         *     possible object is
         *     {@link BigDecimal }
         *     
         */
        public BigDecimal getIznosIsplataOstalo() {
            return iznosIsplataOstalo;
        }

        /**
         * Sets the value of the iznosIsplataOstalo property.
         * 
         * @param value
         *     allowed object is
         *     {@link BigDecimal }
         *     
         */
        public void setIznosIsplataOstalo(BigDecimal value) {
            this.iznosIsplataOstalo = value;
        }

        /**
         * Gets the value of the zavrsnoStanjeIKontaCasina property.
         * 
         * @return
         *     possible object is
         *     {@link BigDecimal }
         *     
         */
        public BigDecimal getZavrsnoStanjeIKontaCasina() {
            return zavrsnoStanjeIKontaCasina;
        }

        /**
         * Sets the value of the zavrsnoStanjeIKontaCasina property.
         * 
         * @param value
         *     allowed object is
         *     {@link BigDecimal }
         *     
         */
        public void setZavrsnoStanjeIKontaCasina(BigDecimal value) {
            this.zavrsnoStanjeIKontaCasina = value;
        }

    }

}
