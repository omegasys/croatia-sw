
package com.omega.croatiasw.service.web.report;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * Podaci o statusu igre, casina, jackpota itd.
 * 				Koristi se prilikom poruka o aktivaciji, promjeni statusa ili nekih
 * 				drugih atributa. Moguće vrijednosti su A - aktivan, N - neaktivan
 * 				ili P -promjena ostalih atributa.
 * 
 * <p>Java class for Status complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="Status"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="DatumVrijeme" type="{http://e-porezna.porezna-uprava.hr/casina/v1-0}DatumVrijeme"/&gt;
 *       &lt;/sequence&gt;
 *       &lt;attribute name="status" use="required" type="{http://e-porezna.porezna-uprava.hr/casina/v1-0}EnumStatus" /&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Status", propOrder = {
    "datumVrijeme"
})
public class Status {

    @XmlElement(name = "DatumVrijeme", required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar datumVrijeme;
    @XmlAttribute(name = "status", required = true)
    protected EnumStatus status;

    /**
     * Gets the value of the datumVrijeme property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getDatumVrijeme() {
        return datumVrijeme;
    }

    /**
     * Sets the value of the datumVrijeme property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setDatumVrijeme(XMLGregorianCalendar value) {
        this.datumVrijeme = value;
    }

    /**
     * Gets the value of the status property.
     * 
     * @return
     *     possible object is
     *     {@link EnumStatus }
     *     
     */
    public EnumStatus getStatus() {
        return status;
    }

    /**
     * Sets the value of the status property.
     * 
     * @param value
     *     allowed object is
     *     {@link EnumStatus }
     *     
     */
    public void setStatus(EnumStatus value) {
        this.status = value;
    }

}
