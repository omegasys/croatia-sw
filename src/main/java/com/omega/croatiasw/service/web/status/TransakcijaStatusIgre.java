
package com.omega.croatiasw.service.web.status;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="Zaglavlje" type="{http://e-porezna.porezna-uprava.hr/casina/v1-0}Zaglavlje"/&gt;
 *         &lt;element name="StatusIgre" type="{http://e-porezna.porezna-uprava.hr/casina/v1-0}StatusIgre"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "zaglavlje",
    "statusIgre"
})
@XmlRootElement(name = "TransakcijaStatusIgre")
public class TransakcijaStatusIgre {

    @XmlElement(name = "Zaglavlje", required = true)
    protected Zaglavlje zaglavlje;
    @XmlElement(name = "StatusIgre", required = true)
    protected StatusIgre statusIgre;

    /**
     * Gets the value of the zaglavlje property.
     * 
     * @return
     *     possible object is
     *     {@link Zaglavlje }
     *     
     */
    public Zaglavlje getZaglavlje() {
        return zaglavlje;
    }

    /**
     * Sets the value of the zaglavlje property.
     * 
     * @param value
     *     allowed object is
     *     {@link Zaglavlje }
     *     
     */
    public void setZaglavlje(Zaglavlje value) {
        this.zaglavlje = value;
    }

    /**
     * Gets the value of the statusIgre property.
     * 
     * @return
     *     possible object is
     *     {@link StatusIgre }
     *     
     */
    public StatusIgre getStatusIgre() {
        return statusIgre;
    }

    /**
     * Sets the value of the statusIgre property.
     * 
     * @param value
     *     allowed object is
     *     {@link StatusIgre }
     *     
     */
    public void setStatusIgre(StatusIgre value) {
        this.statusIgre = value;
    }

}
