
package com.omega.croatiasw.service.web.report;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Izvještaj casina o turniru šalje se završetkom
 * 				turnira. Izvještaj sadrži podatke o datumu i vremenu izrade samog
 * 				izvještaja, casinu u kojem se turnir odigrao te podatke o ukupnim
 * 				uplatama (kotizaciji), isplatama (dobicima) i proviziji koju je
 * 				priređivač zadržao od turnira.
 * 
 * <p>Java class for IzvjestajTurnira complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="IzvjestajTurnira"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="Izvjestaj" type="{http://e-porezna.porezna-uprava.hr/casina/v1-0}IzvjestajType"/&gt;
 *         &lt;element name="Casino" type="{http://e-porezna.porezna-uprava.hr/casina/v1-0}CasinoOpisnikSmanjen"/&gt;
 *         &lt;element name="Igra" type="{http://e-porezna.porezna-uprava.hr/casina/v1-0}IgraOpisnikSmanjen"/&gt;
 *         &lt;element name="Obracun" type="{http://e-porezna.porezna-uprava.hr/casina/v1-0}Obracun1"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "IzvjestajTurnira", propOrder = {
    "izvjestaj",
    "casino",
    "igra",
    "obracun"
})
public class IzvjestajTurnira {

    @XmlElement(name = "Izvjestaj", required = true)
    protected IzvjestajType izvjestaj;
    @XmlElement(name = "Casino", required = true)
    protected CasinoOpisnikSmanjen casino;
    @XmlElement(name = "Igra", required = true)
    protected IgraOpisnikSmanjen igra;
    @XmlElement(name = "Obracun", required = true)
    protected Obracun1 obracun;

    /**
     * Gets the value of the izvjestaj property.
     * 
     * @return
     *     possible object is
     *     {@link IzvjestajType }
     *     
     */
    public IzvjestajType getIzvjestaj() {
        return izvjestaj;
    }

    /**
     * Sets the value of the izvjestaj property.
     * 
     * @param value
     *     allowed object is
     *     {@link IzvjestajType }
     *     
     */
    public void setIzvjestaj(IzvjestajType value) {
        this.izvjestaj = value;
    }

    /**
     * Gets the value of the casino property.
     * 
     * @return
     *     possible object is
     *     {@link CasinoOpisnikSmanjen }
     *     
     */
    public CasinoOpisnikSmanjen getCasino() {
        return casino;
    }

    /**
     * Sets the value of the casino property.
     * 
     * @param value
     *     allowed object is
     *     {@link CasinoOpisnikSmanjen }
     *     
     */
    public void setCasino(CasinoOpisnikSmanjen value) {
        this.casino = value;
    }

    /**
     * Gets the value of the igra property.
     * 
     * @return
     *     possible object is
     *     {@link IgraOpisnikSmanjen }
     *     
     */
    public IgraOpisnikSmanjen getIgra() {
        return igra;
    }

    /**
     * Sets the value of the igra property.
     * 
     * @param value
     *     allowed object is
     *     {@link IgraOpisnikSmanjen }
     *     
     */
    public void setIgra(IgraOpisnikSmanjen value) {
        this.igra = value;
    }

    /**
     * Gets the value of the obracun property.
     * 
     * @return
     *     possible object is
     *     {@link Obracun1 }
     *     
     */
    public Obracun1 getObracun() {
        return obracun;
    }

    /**
     * Sets the value of the obracun property.
     * 
     * @param value
     *     allowed object is
     *     {@link Obracun1 }
     *     
     */
    public void setObracun(Obracun1 value) {
        this.obracun = value;
    }

}
