
package com.omega.croatiasw.service.web.report;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="ID" type="{http://e-porezna.porezna-uprava.hr/casina/v1-0}Guid"/&gt;
 *         &lt;element name="DatumVrijeme" type="{http://e-porezna.porezna-uprava.hr/casina/v1-0}DatumVrijeme"/&gt;
 *         &lt;element name="Greske" minOccurs="0"&gt;
 *           &lt;complexType&gt;
 *             &lt;complexContent&gt;
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                 &lt;sequence&gt;
 *                   &lt;element name="Greska" maxOccurs="unbounded"&gt;
 *                     &lt;complexType&gt;
 *                       &lt;complexContent&gt;
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                           &lt;sequence&gt;
 *                             &lt;element name="Sifra" type="{http://e-porezna.porezna-uprava.hr/casina/v1-0}SifraGreske"/&gt;
 *                             &lt;element name="Opis" type="{http://e-porezna.porezna-uprava.hr/casina/v1-0}OpisGreske"/&gt;
 *                           &lt;/sequence&gt;
 *                         &lt;/restriction&gt;
 *                       &lt;/complexContent&gt;
 *                     &lt;/complexType&gt;
 *                   &lt;/element&gt;
 *                 &lt;/sequence&gt;
 *               &lt;/restriction&gt;
 *             &lt;/complexContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *       &lt;/sequence&gt;
 *       &lt;attribute name="status" use="required" type="{http://e-porezna.porezna-uprava.hr/casina/v1-0}StatusGreske" /&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "id",
    "datumVrijeme",
    "greske"
})
@XmlRootElement(name = "Odgovor")
public class Odgovor {

    @XmlElement(name = "ID", required = true)
    protected String id;
    @XmlElement(name = "DatumVrijeme", required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar datumVrijeme;
    @XmlElement(name = "Greske")
    protected Odgovor.Greske greske;
    @XmlAttribute(name = "status", required = true)
    protected StatusGreske status;

    /**
     * Gets the value of the id property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getID() {
        return id;
    }

    /**
     * Sets the value of the id property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setID(String value) {
        this.id = value;
    }

    /**
     * Gets the value of the datumVrijeme property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getDatumVrijeme() {
        return datumVrijeme;
    }

    /**
     * Sets the value of the datumVrijeme property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setDatumVrijeme(XMLGregorianCalendar value) {
        this.datumVrijeme = value;
    }

    /**
     * Gets the value of the greske property.
     * 
     * @return
     *     possible object is
     *     {@link Odgovor.Greske }
     *     
     */
    public Odgovor.Greske getGreske() {
        return greske;
    }

    /**
     * Sets the value of the greske property.
     * 
     * @param value
     *     allowed object is
     *     {@link Odgovor.Greske }
     *     
     */
    public void setGreske(Odgovor.Greske value) {
        this.greske = value;
    }

    /**
     * Gets the value of the status property.
     * 
     * @return
     *     possible object is
     *     {@link StatusGreske }
     *     
     */
    public StatusGreske getStatus() {
        return status;
    }

    /**
     * Sets the value of the status property.
     * 
     * @param value
     *     allowed object is
     *     {@link StatusGreske }
     *     
     */
    public void setStatus(StatusGreske value) {
        this.status = value;
    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType&gt;
     *   &lt;complexContent&gt;
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *       &lt;sequence&gt;
     *         &lt;element name="Greska" maxOccurs="unbounded"&gt;
     *           &lt;complexType&gt;
     *             &lt;complexContent&gt;
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *                 &lt;sequence&gt;
     *                   &lt;element name="Sifra" type="{http://e-porezna.porezna-uprava.hr/casina/v1-0}SifraGreske"/&gt;
     *                   &lt;element name="Opis" type="{http://e-porezna.porezna-uprava.hr/casina/v1-0}OpisGreske"/&gt;
     *                 &lt;/sequence&gt;
     *               &lt;/restriction&gt;
     *             &lt;/complexContent&gt;
     *           &lt;/complexType&gt;
     *         &lt;/element&gt;
     *       &lt;/sequence&gt;
     *     &lt;/restriction&gt;
     *   &lt;/complexContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "greska"
    })
    public static class Greske {

        @XmlElement(name = "Greska", required = true)
        protected List<Odgovor.Greske.Greska> greska;

        /**
         * Gets the value of the greska property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the greska property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getGreska().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link Odgovor.Greske.Greska }
         * 
         * 
         */
        public List<Odgovor.Greske.Greska> getGreska() {
            if (greska == null) {
                greska = new ArrayList<Odgovor.Greske.Greska>();
            }
            return this.greska;
        }


        /**
         * <p>Java class for anonymous complex type.
         * 
         * <p>The following schema fragment specifies the expected content contained within this class.
         * 
         * <pre>
         * &lt;complexType&gt;
         *   &lt;complexContent&gt;
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
         *       &lt;sequence&gt;
         *         &lt;element name="Sifra" type="{http://e-porezna.porezna-uprava.hr/casina/v1-0}SifraGreske"/&gt;
         *         &lt;element name="Opis" type="{http://e-porezna.porezna-uprava.hr/casina/v1-0}OpisGreske"/&gt;
         *       &lt;/sequence&gt;
         *     &lt;/restriction&gt;
         *   &lt;/complexContent&gt;
         * &lt;/complexType&gt;
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "sifra",
            "opis"
        })
        public static class Greska {

            @XmlElement(name = "Sifra", required = true)
            protected String sifra;
            @XmlElement(name = "Opis", required = true)
            protected String opis;

            /**
             * Gets the value of the sifra property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getSifra() {
                return sifra;
            }

            /**
             * Sets the value of the sifra property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setSifra(String value) {
                this.sifra = value;
            }

            /**
             * Gets the value of the opis property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getOpis() {
                return opis;
            }

            /**
             * Sets the value of the opis property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setOpis(String value) {
                this.opis = value;
            }

        }

    }

}
