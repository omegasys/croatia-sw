package com.omega.croatiasw.service.web.report;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;
import javax.xml.bind.annotation.XmlSeeAlso;

/**
 * This class was generated by Apache CXF 3.3.1
 * 2019-05-01T17:01:26.044-07:00
 * Generated source version: 3.3.1
 *
 */
@WebService(targetNamespace = "http://e-porezna.porezna-uprava.hr/casina/2015/services/iCasinaIzvjestajiService", name = "iCasinaIzvjestajiPortType")
@XmlSeeAlso({ObjectFactory.class})
@SOAPBinding(parameterStyle = SOAPBinding.ParameterStyle.BARE)
public interface ICasinaIzvjestajiPortType {

    @WebMethod(action = "http://e-porezna.porezna-uprava.hr/casina/2015/services/iCasinaPromjenaStatusaService/dnevniIzvjestajAutomata")
    @WebResult(name = "Odgovor", targetNamespace = "http://e-porezna.porezna-uprava.hr/casina/v1-0", partName = "response")
    public Odgovor dnevniIzvjestajAutomata(
        @WebParam(partName = "request", name = "TransakcijaIzvjestajAutomata", targetNamespace = "http://e-porezna.porezna-uprava.hr/casina/v1-0")
        TransakcijaIzvjestajAutomata request
    );

    @WebMethod(action = "http://e-porezna.porezna-uprava.hr/casina/2015/services/iCasinaPromjenaStatusaService/izvjestajTurnira")
    @WebResult(name = "Odgovor", targetNamespace = "http://e-porezna.porezna-uprava.hr/casina/v1-0", partName = "response")
    public Odgovor izvjestajTurnira(
        @WebParam(partName = "request", name = "TransakcijaIzvjestajTurnira", targetNamespace = "http://e-porezna.porezna-uprava.hr/casina/v1-0")
        TransakcijaIzvjestajTurnira request
    );

    @WebMethod(action = "http://e-porezna.porezna-uprava.hr/casina/2015/services/iCasinaPromjenaStatusaService/dnevniIzvjestajIKontaCasina")
    @WebResult(name = "Odgovor", targetNamespace = "http://e-porezna.porezna-uprava.hr/casina/v1-0", partName = "response")
    public Odgovor dnevniIzvjestajIKontaCasina(
        @WebParam(partName = "request", name = "TransakcijaIzvjestajIKontaCasina", targetNamespace = "http://e-porezna.porezna-uprava.hr/casina/v1-0")
        TransakcijaIzvjestajIKontaCasina request
    );

    @WebMethod(action = "http://e-porezna.porezna-uprava.hr/casina/2015/services/iCasinaPromjenaStatusaService/izvjestajBrisanje")
    @WebResult(name = "Odgovor", targetNamespace = "http://e-porezna.porezna-uprava.hr/casina/v1-0", partName = "response")
    public Odgovor izvjestajBrisanje(
        @WebParam(partName = "request", name = "TransakcijaBrisanje", targetNamespace = "http://e-porezna.porezna-uprava.hr/casina/v1-0")
        TransakcijaBrisanje request
    );

    @WebMethod(action = "http://e-porezna.porezna-uprava.hr/casina/2015/services/iCasinaPromjenaStatusaService/izvjestajMjesecniObracun")
    @WebResult(name = "Odgovor", targetNamespace = "http://e-porezna.porezna-uprava.hr/casina/v1-0", partName = "response")
    public Odgovor izvjestajMjesecniObracun(
        @WebParam(partName = "request", name = "TransakcijaMjesecniObracun", targetNamespace = "http://e-porezna.porezna-uprava.hr/casina/v1-0")
        TransakcijaMjesecniObracun request
    );

    @WebMethod(action = "http://e-porezna.porezna-uprava.hr/casina/2015/services/iCasinaPromjenaStatusaService/dnevniIzvjestajStolova")
    @WebResult(name = "Odgovor", targetNamespace = "http://e-porezna.porezna-uprava.hr/casina/v1-0", partName = "response")
    public Odgovor dnevniIzvjestajStolova(
        @WebParam(partName = "request", name = "TransakcijaIzvjestajStolova", targetNamespace = "http://e-porezna.porezna-uprava.hr/casina/v1-0")
        TransakcijaIzvjestajStolova request
    );

    @WebMethod(action = "http://e-porezna.porezna-uprava.hr/casina/2015/services/iCasinaPromjenaStatusaService/echo")
    @WebResult(name = "echoResponse", targetNamespace = "http://e-porezna.porezna-uprava.hr/casina/v1-0", partName = "response")
    public java.lang.String echo(
        @WebParam(partName = "request", name = "echoRequest", targetNamespace = "http://e-porezna.porezna-uprava.hr/casina/v1-0")
        java.lang.String request
    );
}
