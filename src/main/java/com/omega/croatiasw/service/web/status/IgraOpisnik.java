
package com.omega.croatiasw.service.web.status;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Podaci o igri (automat, turnir, stol).
 * 			
 * 
 * <p>Java class for IgraOpisnik complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="IgraOpisnik"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{http://e-porezna.porezna-uprava.hr/casina/v1-0}Opisnik"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="Naziv" type="{http://e-porezna.porezna-uprava.hr/casina/v1-0}NazivDugi"/&gt;
 *       &lt;/sequence&gt;
 *       &lt;attribute name="vrsta" use="required" type="{http://e-porezna.porezna-uprava.hr/casina/v1-0}VrstaIgre" /&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "IgraOpisnik", propOrder = {
    "naziv"
})
public class IgraOpisnik
    extends Opisnik
{

    @XmlElement(name = "Naziv", required = true)
    protected String naziv;
    @XmlAttribute(name = "vrsta", required = true)
    protected VrstaIgre vrsta;

    /**
     * Gets the value of the naziv property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNaziv() {
        return naziv;
    }

    /**
     * Sets the value of the naziv property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNaziv(String value) {
        this.naziv = value;
    }

    /**
     * Gets the value of the vrsta property.
     * 
     * @return
     *     possible object is
     *     {@link VrstaIgre }
     *     
     */
    public VrstaIgre getVrsta() {
        return vrsta;
    }

    /**
     * Sets the value of the vrsta property.
     * 
     * @param value
     *     allowed object is
     *     {@link VrstaIgre }
     *     
     */
    public void setVrsta(VrstaIgre value) {
        this.vrsta = value;
    }

}
