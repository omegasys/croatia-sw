
package com.omega.croatiasw.service.web.status;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;


/**
 * Podaci o igri (automat, turnir, stol).
 * 			
 * 
 * <p>Java class for IgraOpisnikSmanjen complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="IgraOpisnikSmanjen"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{http://e-porezna.porezna-uprava.hr/casina/v1-0}Opisnik"&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "IgraOpisnikSmanjen")
@XmlSeeAlso({
    com.omega.croatiasw.service.web.status.IzvjestajAutomata.Igre.Igra.class,
    com.omega.croatiasw.service.web.status.IzvjestajStolova.Igre.Igra.class
})
public class IgraOpisnikSmanjen
    extends Opisnik
{


}
