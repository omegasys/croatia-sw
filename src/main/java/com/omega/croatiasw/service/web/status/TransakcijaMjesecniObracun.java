
package com.omega.croatiasw.service.web.status;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="Zaglavlje" type="{http://e-porezna.porezna-uprava.hr/casina/v1-0}Zaglavlje"/&gt;
 *         &lt;element name="MjesecniObracun" type="{http://e-porezna.porezna-uprava.hr/casina/v1-0}MjesecniObracun"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "zaglavlje",
    "mjesecniObracun"
})
@XmlRootElement(name = "TransakcijaMjesecniObracun")
public class TransakcijaMjesecniObracun {

    @XmlElement(name = "Zaglavlje", required = true)
    protected Zaglavlje zaglavlje;
    @XmlElement(name = "MjesecniObracun", required = true)
    protected MjesecniObracun mjesecniObracun;

    /**
     * Gets the value of the zaglavlje property.
     * 
     * @return
     *     possible object is
     *     {@link Zaglavlje }
     *     
     */
    public Zaglavlje getZaglavlje() {
        return zaglavlje;
    }

    /**
     * Sets the value of the zaglavlje property.
     * 
     * @param value
     *     allowed object is
     *     {@link Zaglavlje }
     *     
     */
    public void setZaglavlje(Zaglavlje value) {
        this.zaglavlje = value;
    }

    /**
     * Gets the value of the mjesecniObracun property.
     * 
     * @return
     *     possible object is
     *     {@link MjesecniObracun }
     *     
     */
    public MjesecniObracun getMjesecniObracun() {
        return mjesecniObracun;
    }

    /**
     * Sets the value of the mjesecniObracun property.
     * 
     * @param value
     *     allowed object is
     *     {@link MjesecniObracun }
     *     
     */
    public void setMjesecniObracun(MjesecniObracun value) {
        this.mjesecniObracun = value;
    }

}
