
package com.omega.croatiasw.service.web.report;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Podaci o casinu.
 * 
 * <p>Java class for CasinoOpisnik complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="CasinoOpisnik"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{http://e-porezna.porezna-uprava.hr/casina/v1-0}Opisnik"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="Naziv" type="{http://e-porezna.porezna-uprava.hr/casina/v1-0}NazivDugi"/&gt;
 *       &lt;/sequence&gt;
 *       &lt;attribute name="vrsta" use="required" type="{http://e-porezna.porezna-uprava.hr/casina/v1-0}VrstaCasina" /&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CasinoOpisnik", propOrder = {
    "naziv"
})
public class CasinoOpisnik
    extends Opisnik
{

    @XmlElement(name = "Naziv", required = true)
    protected String naziv;
    @XmlAttribute(name = "vrsta", required = true)
    protected VrstaCasina vrsta;

    /**
     * Gets the value of the naziv property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNaziv() {
        return naziv;
    }

    /**
     * Sets the value of the naziv property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNaziv(String value) {
        this.naziv = value;
    }

    /**
     * Gets the value of the vrsta property.
     * 
     * @return
     *     possible object is
     *     {@link VrstaCasina }
     *     
     */
    public VrstaCasina getVrsta() {
        return vrsta;
    }

    /**
     * Sets the value of the vrsta property.
     * 
     * @param value
     *     allowed object is
     *     {@link VrstaCasina }
     *     
     */
    public void setVrsta(VrstaCasina value) {
        this.vrsta = value;
    }

}
