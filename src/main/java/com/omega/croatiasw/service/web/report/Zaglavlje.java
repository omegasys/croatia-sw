
package com.omega.croatiasw.service.web.report;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * Svaka transakcija mora sadržavati zaglavlje. 
 * 				Zaglavlje sadrži sistemske podatke o
 * 				identifikatoru transakcije, OIB-u priređivača koji šalje
 * 				transakciju, datumu i vremenu slanja transakcije te podatke o
 * 				ponovno poslanoj transakciji
 * 
 * <p>Java class for Zaglavlje complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="Zaglavlje"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="ID" type="{http://e-porezna.porezna-uprava.hr/casina/v1-0}Guid"/&gt;
 *         &lt;element name="OIB" type="{http://e-porezna.porezna-uprava.hr/casina/v1-0}Oib"/&gt;
 *         &lt;element name="DatumVrijeme" type="{http://e-porezna.porezna-uprava.hr/casina/v1-0}DatumVrijeme"/&gt;
 *         &lt;element name="PovezanaTransakcija" type="{http://e-porezna.porezna-uprava.hr/casina/v1-0}PovezanaTransakcija" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Zaglavlje", propOrder = {
    "id",
    "oib",
    "datumVrijeme",
    "povezanaTransakcija"
})
public class Zaglavlje {

    @XmlElement(name = "ID", required = true)
    protected String id;
    @XmlElement(name = "OIB", required = true)
    protected String oib;
    @XmlElement(name = "DatumVrijeme", required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar datumVrijeme;
    @XmlElement(name = "PovezanaTransakcija")
    protected PovezanaTransakcija povezanaTransakcija;

    /**
     * Gets the value of the id property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getID() {
        return id;
    }

    /**
     * Sets the value of the id property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setID(String value) {
        this.id = value;
    }

    /**
     * Gets the value of the oib property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOIB() {
        return oib;
    }

    /**
     * Sets the value of the oib property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOIB(String value) {
        this.oib = value;
    }

    /**
     * Gets the value of the datumVrijeme property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getDatumVrijeme() {
        return datumVrijeme;
    }

    /**
     * Sets the value of the datumVrijeme property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setDatumVrijeme(XMLGregorianCalendar value) {
        this.datumVrijeme = value;
    }

    /**
     * Gets the value of the povezanaTransakcija property.
     * 
     * @return
     *     possible object is
     *     {@link PovezanaTransakcija }
     *     
     */
    public PovezanaTransakcija getPovezanaTransakcija() {
        return povezanaTransakcija;
    }

    /**
     * Sets the value of the povezanaTransakcija property.
     * 
     * @param value
     *     allowed object is
     *     {@link PovezanaTransakcija }
     *     
     */
    public void setPovezanaTransakcija(PovezanaTransakcija value) {
        this.povezanaTransakcija = value;
    }

}
