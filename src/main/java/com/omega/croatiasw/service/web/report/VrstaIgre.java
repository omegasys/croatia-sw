
package com.omega.croatiasw.service.web.report;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for VrstaIgre.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="VrstaIgre"&gt;
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *     &lt;length value="1"/&gt;
 *     &lt;enumeration value="T"/&gt;
 *     &lt;enumeration value="S"/&gt;
 *     &lt;enumeration value="A"/&gt;
 *   &lt;/restriction&gt;
 * &lt;/simpleType&gt;
 * </pre>
 * 
 */
@XmlType(name = "VrstaIgre")
@XmlEnum
public enum VrstaIgre {

    T,
    S,
    A;

    public String value() {
        return name();
    }

    public static VrstaIgre fromValue(String v) {
        return valueOf(v);
    }

}
