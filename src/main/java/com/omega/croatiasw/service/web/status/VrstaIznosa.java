
package com.omega.croatiasw.service.web.status;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for VrstaIznosa.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="VrstaIznosa"&gt;
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *     &lt;length value="1"/&gt;
 *     &lt;enumeration value="B"/&gt;
 *     &lt;enumeration value="N"/&gt;
 *   &lt;/restriction&gt;
 * &lt;/simpleType&gt;
 * </pre>
 * 
 */
@XmlType(name = "VrstaIznosa")
@XmlEnum
public enum VrstaIznosa {

    B,
    N;

    public String value() {
        return name();
    }

    public static VrstaIznosa fromValue(String v) {
        return valueOf(v);
    }

}
