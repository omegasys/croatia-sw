
package com.omega.croatiasw.service.web.status;

import java.math.BigDecimal;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Konačni mjesečni obračun služi za prijavu naknade
 * 				koju priređivač treba platiti Ministarstvu financija. On obuhvaća
 * 				sumarne podatke o automatima, stolovima i turnirima kao i ukupnu
 * 				naknadu koju priređivač treba uplatiti Ministarstvu financija.
 * 			
 * 
 * <p>Java class for MjesecniObracun complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="MjesecniObracun"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="Izvjestaj" type="{http://e-porezna.porezna-uprava.hr/casina/v1-0}IzvjestajMjesec"/&gt;
 *         &lt;element name="Casino" type="{http://e-porezna.porezna-uprava.hr/casina/v1-0}CasinoOpisnikSmanjen"/&gt;
 *         &lt;element name="IgreNaAutomatima" minOccurs="0"&gt;
 *           &lt;complexType&gt;
 *             &lt;complexContent&gt;
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                 &lt;sequence&gt;
 *                   &lt;element name="Uplata" type="{http://e-porezna.porezna-uprava.hr/casina/v1-0}Iznos"/&gt;
 *                   &lt;element name="Isplata" type="{http://e-porezna.porezna-uprava.hr/casina/v1-0}Iznos"/&gt;
 *                   &lt;element name="JackpotAkumulacija" type="{http://e-porezna.porezna-uprava.hr/casina/v1-0}Iznos"/&gt;
 *                   &lt;element name="JackpotIsplata" type="{http://e-porezna.porezna-uprava.hr/casina/v1-0}Iznos"/&gt;
 *                   &lt;element name="Osnovica" type="{http://e-porezna.porezna-uprava.hr/casina/v1-0}Iznos"/&gt;
 *                   &lt;element name="Naknada" type="{http://e-porezna.porezna-uprava.hr/casina/v1-0}Iznos"/&gt;
 *                 &lt;/sequence&gt;
 *               &lt;/restriction&gt;
 *             &lt;/complexContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *         &lt;element name="Stolovi" minOccurs="0"&gt;
 *           &lt;complexType&gt;
 *             &lt;complexContent&gt;
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                 &lt;sequence&gt;
 *                   &lt;element name="Osnovica" type="{http://e-porezna.porezna-uprava.hr/casina/v1-0}Iznos"/&gt;
 *                   &lt;element name="Naknada" type="{http://e-porezna.porezna-uprava.hr/casina/v1-0}Iznos"/&gt;
 *                 &lt;/sequence&gt;
 *               &lt;/restriction&gt;
 *             &lt;/complexContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *         &lt;element name="Turniri" minOccurs="0"&gt;
 *           &lt;complexType&gt;
 *             &lt;complexContent&gt;
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                 &lt;sequence&gt;
 *                   &lt;element name="Osnovica" type="{http://e-porezna.porezna-uprava.hr/casina/v1-0}Iznos"/&gt;
 *                   &lt;element name="Naknada" type="{http://e-porezna.porezna-uprava.hr/casina/v1-0}Iznos"/&gt;
 *                 &lt;/sequence&gt;
 *               &lt;/restriction&gt;
 *             &lt;/complexContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *         &lt;element name="Ukupno"&gt;
 *           &lt;complexType&gt;
 *             &lt;complexContent&gt;
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                 &lt;all&gt;
 *                   &lt;element name="Naknada" type="{http://e-porezna.porezna-uprava.hr/casina/v1-0}Iznos"/&gt;
 *                 &lt;/all&gt;
 *               &lt;/restriction&gt;
 *             &lt;/complexContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "MjesecniObracun", propOrder = {
    "izvjestaj",
    "casino",
    "igreNaAutomatima",
    "stolovi",
    "turniri",
    "ukupno"
})
public class MjesecniObracun {

    @XmlElement(name = "Izvjestaj", required = true)
    protected IzvjestajMjesec izvjestaj;
    @XmlElement(name = "Casino", required = true)
    protected CasinoOpisnikSmanjen casino;
    @XmlElement(name = "IgreNaAutomatima")
    protected MjesecniObracun.IgreNaAutomatima igreNaAutomatima;
    @XmlElement(name = "Stolovi")
    protected MjesecniObracun.Stolovi stolovi;
    @XmlElement(name = "Turniri")
    protected MjesecniObracun.Turniri turniri;
    @XmlElement(name = "Ukupno", required = true)
    protected MjesecniObracun.Ukupno ukupno;

    /**
     * Gets the value of the izvjestaj property.
     * 
     * @return
     *     possible object is
     *     {@link IzvjestajMjesec }
     *     
     */
    public IzvjestajMjesec getIzvjestaj() {
        return izvjestaj;
    }

    /**
     * Sets the value of the izvjestaj property.
     * 
     * @param value
     *     allowed object is
     *     {@link IzvjestajMjesec }
     *     
     */
    public void setIzvjestaj(IzvjestajMjesec value) {
        this.izvjestaj = value;
    }

    /**
     * Gets the value of the casino property.
     * 
     * @return
     *     possible object is
     *     {@link CasinoOpisnikSmanjen }
     *     
     */
    public CasinoOpisnikSmanjen getCasino() {
        return casino;
    }

    /**
     * Sets the value of the casino property.
     * 
     * @param value
     *     allowed object is
     *     {@link CasinoOpisnikSmanjen }
     *     
     */
    public void setCasino(CasinoOpisnikSmanjen value) {
        this.casino = value;
    }

    /**
     * Gets the value of the igreNaAutomatima property.
     * 
     * @return
     *     possible object is
     *     {@link MjesecniObracun.IgreNaAutomatima }
     *     
     */
    public MjesecniObracun.IgreNaAutomatima getIgreNaAutomatima() {
        return igreNaAutomatima;
    }

    /**
     * Sets the value of the igreNaAutomatima property.
     * 
     * @param value
     *     allowed object is
     *     {@link MjesecniObracun.IgreNaAutomatima }
     *     
     */
    public void setIgreNaAutomatima(MjesecniObracun.IgreNaAutomatima value) {
        this.igreNaAutomatima = value;
    }

    /**
     * Gets the value of the stolovi property.
     * 
     * @return
     *     possible object is
     *     {@link MjesecniObracun.Stolovi }
     *     
     */
    public MjesecniObracun.Stolovi getStolovi() {
        return stolovi;
    }

    /**
     * Sets the value of the stolovi property.
     * 
     * @param value
     *     allowed object is
     *     {@link MjesecniObracun.Stolovi }
     *     
     */
    public void setStolovi(MjesecniObracun.Stolovi value) {
        this.stolovi = value;
    }

    /**
     * Gets the value of the turniri property.
     * 
     * @return
     *     possible object is
     *     {@link MjesecniObracun.Turniri }
     *     
     */
    public MjesecniObracun.Turniri getTurniri() {
        return turniri;
    }

    /**
     * Sets the value of the turniri property.
     * 
     * @param value
     *     allowed object is
     *     {@link MjesecniObracun.Turniri }
     *     
     */
    public void setTurniri(MjesecniObracun.Turniri value) {
        this.turniri = value;
    }

    /**
     * Gets the value of the ukupno property.
     * 
     * @return
     *     possible object is
     *     {@link MjesecniObracun.Ukupno }
     *     
     */
    public MjesecniObracun.Ukupno getUkupno() {
        return ukupno;
    }

    /**
     * Sets the value of the ukupno property.
     * 
     * @param value
     *     allowed object is
     *     {@link MjesecniObracun.Ukupno }
     *     
     */
    public void setUkupno(MjesecniObracun.Ukupno value) {
        this.ukupno = value;
    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType&gt;
     *   &lt;complexContent&gt;
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *       &lt;sequence&gt;
     *         &lt;element name="Uplata" type="{http://e-porezna.porezna-uprava.hr/casina/v1-0}Iznos"/&gt;
     *         &lt;element name="Isplata" type="{http://e-porezna.porezna-uprava.hr/casina/v1-0}Iznos"/&gt;
     *         &lt;element name="JackpotAkumulacija" type="{http://e-porezna.porezna-uprava.hr/casina/v1-0}Iznos"/&gt;
     *         &lt;element name="JackpotIsplata" type="{http://e-porezna.porezna-uprava.hr/casina/v1-0}Iznos"/&gt;
     *         &lt;element name="Osnovica" type="{http://e-porezna.porezna-uprava.hr/casina/v1-0}Iznos"/&gt;
     *         &lt;element name="Naknada" type="{http://e-porezna.porezna-uprava.hr/casina/v1-0}Iznos"/&gt;
     *       &lt;/sequence&gt;
     *     &lt;/restriction&gt;
     *   &lt;/complexContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "uplata",
        "isplata",
        "jackpotAkumulacija",
        "jackpotIsplata",
        "osnovica",
        "naknada"
    })
    public static class IgreNaAutomatima {

        @XmlElement(name = "Uplata", required = true)
        protected BigDecimal uplata;
        @XmlElement(name = "Isplata", required = true)
        protected BigDecimal isplata;
        @XmlElement(name = "JackpotAkumulacija", required = true)
        protected BigDecimal jackpotAkumulacija;
        @XmlElement(name = "JackpotIsplata", required = true)
        protected BigDecimal jackpotIsplata;
        @XmlElement(name = "Osnovica", required = true)
        protected BigDecimal osnovica;
        @XmlElement(name = "Naknada", required = true)
        protected BigDecimal naknada;

        /**
         * Gets the value of the uplata property.
         * 
         * @return
         *     possible object is
         *     {@link BigDecimal }
         *     
         */
        public BigDecimal getUplata() {
            return uplata;
        }

        /**
         * Sets the value of the uplata property.
         * 
         * @param value
         *     allowed object is
         *     {@link BigDecimal }
         *     
         */
        public void setUplata(BigDecimal value) {
            this.uplata = value;
        }

        /**
         * Gets the value of the isplata property.
         * 
         * @return
         *     possible object is
         *     {@link BigDecimal }
         *     
         */
        public BigDecimal getIsplata() {
            return isplata;
        }

        /**
         * Sets the value of the isplata property.
         * 
         * @param value
         *     allowed object is
         *     {@link BigDecimal }
         *     
         */
        public void setIsplata(BigDecimal value) {
            this.isplata = value;
        }

        /**
         * Gets the value of the jackpotAkumulacija property.
         * 
         * @return
         *     possible object is
         *     {@link BigDecimal }
         *     
         */
        public BigDecimal getJackpotAkumulacija() {
            return jackpotAkumulacija;
        }

        /**
         * Sets the value of the jackpotAkumulacija property.
         * 
         * @param value
         *     allowed object is
         *     {@link BigDecimal }
         *     
         */
        public void setJackpotAkumulacija(BigDecimal value) {
            this.jackpotAkumulacija = value;
        }

        /**
         * Gets the value of the jackpotIsplata property.
         * 
         * @return
         *     possible object is
         *     {@link BigDecimal }
         *     
         */
        public BigDecimal getJackpotIsplata() {
            return jackpotIsplata;
        }

        /**
         * Sets the value of the jackpotIsplata property.
         * 
         * @param value
         *     allowed object is
         *     {@link BigDecimal }
         *     
         */
        public void setJackpotIsplata(BigDecimal value) {
            this.jackpotIsplata = value;
        }

        /**
         * Gets the value of the osnovica property.
         * 
         * @return
         *     possible object is
         *     {@link BigDecimal }
         *     
         */
        public BigDecimal getOsnovica() {
            return osnovica;
        }

        /**
         * Sets the value of the osnovica property.
         * 
         * @param value
         *     allowed object is
         *     {@link BigDecimal }
         *     
         */
        public void setOsnovica(BigDecimal value) {
            this.osnovica = value;
        }

        /**
         * Gets the value of the naknada property.
         * 
         * @return
         *     possible object is
         *     {@link BigDecimal }
         *     
         */
        public BigDecimal getNaknada() {
            return naknada;
        }

        /**
         * Sets the value of the naknada property.
         * 
         * @param value
         *     allowed object is
         *     {@link BigDecimal }
         *     
         */
        public void setNaknada(BigDecimal value) {
            this.naknada = value;
        }

    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType&gt;
     *   &lt;complexContent&gt;
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *       &lt;sequence&gt;
     *         &lt;element name="Osnovica" type="{http://e-porezna.porezna-uprava.hr/casina/v1-0}Iznos"/&gt;
     *         &lt;element name="Naknada" type="{http://e-porezna.porezna-uprava.hr/casina/v1-0}Iznos"/&gt;
     *       &lt;/sequence&gt;
     *     &lt;/restriction&gt;
     *   &lt;/complexContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "osnovica",
        "naknada"
    })
    public static class Stolovi {

        @XmlElement(name = "Osnovica", required = true)
        protected BigDecimal osnovica;
        @XmlElement(name = "Naknada", required = true)
        protected BigDecimal naknada;

        /**
         * Gets the value of the osnovica property.
         * 
         * @return
         *     possible object is
         *     {@link BigDecimal }
         *     
         */
        public BigDecimal getOsnovica() {
            return osnovica;
        }

        /**
         * Sets the value of the osnovica property.
         * 
         * @param value
         *     allowed object is
         *     {@link BigDecimal }
         *     
         */
        public void setOsnovica(BigDecimal value) {
            this.osnovica = value;
        }

        /**
         * Gets the value of the naknada property.
         * 
         * @return
         *     possible object is
         *     {@link BigDecimal }
         *     
         */
        public BigDecimal getNaknada() {
            return naknada;
        }

        /**
         * Sets the value of the naknada property.
         * 
         * @param value
         *     allowed object is
         *     {@link BigDecimal }
         *     
         */
        public void setNaknada(BigDecimal value) {
            this.naknada = value;
        }

    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType&gt;
     *   &lt;complexContent&gt;
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *       &lt;sequence&gt;
     *         &lt;element name="Osnovica" type="{http://e-porezna.porezna-uprava.hr/casina/v1-0}Iznos"/&gt;
     *         &lt;element name="Naknada" type="{http://e-porezna.porezna-uprava.hr/casina/v1-0}Iznos"/&gt;
     *       &lt;/sequence&gt;
     *     &lt;/restriction&gt;
     *   &lt;/complexContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "osnovica",
        "naknada"
    })
    public static class Turniri {

        @XmlElement(name = "Osnovica", required = true)
        protected BigDecimal osnovica;
        @XmlElement(name = "Naknada", required = true)
        protected BigDecimal naknada;

        /**
         * Gets the value of the osnovica property.
         * 
         * @return
         *     possible object is
         *     {@link BigDecimal }
         *     
         */
        public BigDecimal getOsnovica() {
            return osnovica;
        }

        /**
         * Sets the value of the osnovica property.
         * 
         * @param value
         *     allowed object is
         *     {@link BigDecimal }
         *     
         */
        public void setOsnovica(BigDecimal value) {
            this.osnovica = value;
        }

        /**
         * Gets the value of the naknada property.
         * 
         * @return
         *     possible object is
         *     {@link BigDecimal }
         *     
         */
        public BigDecimal getNaknada() {
            return naknada;
        }

        /**
         * Sets the value of the naknada property.
         * 
         * @param value
         *     allowed object is
         *     {@link BigDecimal }
         *     
         */
        public void setNaknada(BigDecimal value) {
            this.naknada = value;
        }

    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType&gt;
     *   &lt;complexContent&gt;
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *       &lt;all&gt;
     *         &lt;element name="Naknada" type="{http://e-porezna.porezna-uprava.hr/casina/v1-0}Iznos"/&gt;
     *       &lt;/all&gt;
     *     &lt;/restriction&gt;
     *   &lt;/complexContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {

    })
    public static class Ukupno {

        @XmlElement(name = "Naknada", required = true)
        protected BigDecimal naknada;

        /**
         * Gets the value of the naknada property.
         * 
         * @return
         *     possible object is
         *     {@link BigDecimal }
         *     
         */
        public BigDecimal getNaknada() {
            return naknada;
        }

        /**
         * Sets the value of the naknada property.
         * 
         * @param value
         *     allowed object is
         *     {@link BigDecimal }
         *     
         */
        public void setNaknada(BigDecimal value) {
            this.naknada = value;
        }

    }

}
