package com.omega.croatiasw.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;

import com.omega.croatiasw.enumeration.CroatiaTransactionType;
import com.omega.croatiasw.vm.TransactionDetailVM;
import com.omega.croatiasw.vm.TransactionHeaderVM;

/**
 * @author Wenbo at Mar 18th, 2019
 */
@Service
public class CroatiaReportService {
    private final Logger log = LoggerFactory.getLogger(getClass().getSimpleName());

    private JdbcTemplate jdbc;

    private final MailService mailService;

    private final StatusChangeReportService statusChangeReportService;

    private final DailyReportService dailyReportService;

    public CroatiaReportService(JdbcTemplate jdbc, MailService mailService,
                                StatusChangeReportService statusChangeReportService,
                                DailyReportService dailyReportService) {
        this.jdbc = jdbc;
        this.mailService = mailService;
        this.statusChangeReportService = statusChangeReportService;
        this.dailyReportService = dailyReportService;
    }

    @Transactional(propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
    public Boolean sendReport() {
        List<TransactionHeaderVM> transactions = getHeader();
        if (transactions.isEmpty()) {
            log.info("Transaction header is empty, nothing to be sent");
            return false;
        } else {
            TransactionHeaderVM header = transactions.get(0);
            if(CroatiaTransactionType.Cancellation.name().equals(header.getTransactionType())){
                return send(header, null);
            } else {
                List<TransactionDetailVM> detailVMS = getDetail(header.getTransactionID(), header.getTransactionType(),
                        header.getSQLTransactionID());
                if (detailVMS.isEmpty()) {
                    log.error("No Transaction detail found for given header");
                    if (!mailService.hasThisTransactionBeenSent(header.getTransactionID())) {
                        mailService.sendJobFailEmail(header);
                    }
                    return false;
                } else {
                    return send(header, detailVMS);
                }
            }
        }
    }

    /**
     * function to generate daily reports
     *
     * @return
     */
    public void generateDailyReports() {
        try {
            generateDailySlotGameReport();
            generateDailyTableGameReport();
            generateDailyAccountReport();
        } catch (Exception e) {
            log.error("Exception:: ", e);
            mailService.sendDailyJobFailEmail();
        }
    }

    /**
     * function to generate monthly reports
     *
     * @return
     */
    public void generateMonthlyReports() {
        try {
            generateMonthlyStatementReports();
        } catch (Exception e) {
            log.error("Exception:: ", e);
            mailService.sendMonthlyJobFailEmail();
        }
    }

    private Boolean send(TransactionHeaderVM headerVM, List<TransactionDetailVM> details) {
        switch (CroatiaTransactionType.valueOf(headerVM.getTransactionType())) {
            case Casino:
                return statusChangeReportService.casinoStatusChangeReport(headerVM, details);
//            case "Product":
//                return statusChangeReportService.productStatusChangeReport(headerVM, details);
            case Game:
                return statusChangeReportService.gameStatusChangeReport(headerVM, details);
            case Jackpot:
                return statusChangeReportService.jackpotStatusChangeReport(headerVM, details);
            case SlotGameReport:
                return dailyReportService.dailySlotGameReport(headerVM, details);
            case TableGameReport:
                return dailyReportService.dailyTableGameReport(headerVM, details);
            case IContoReport:
                return dailyReportService.dailyAccountReport(headerVM, details);
            case MonthlyReport:
                return dailyReportService.monthlyStatementReport(headerVM, details);
            case Cancellation:
                return dailyReportService.cancelDailyReport(headerVM);
            default:
                return false;
        }
    }

    @SuppressWarnings("unchecked")
    private List<TransactionHeaderVM> getHeader() {
        log.info("get transaction header");
        SqlParameterSource parameters = new MapSqlParameterSource();
        SimpleJdbcCall jdbcCall = new SimpleJdbcCall(jdbc).withSchemaName("croatia")
                .withProcedureName("usp_GetTransactionHeader")
                .returningResultSet("data", new BeanPropertyRowMapper(TransactionHeaderVM.class));
        Map m = jdbcCall.execute(parameters);
        return (List) m.get("data");
    }

    @SuppressWarnings("unchecked")
    private List<TransactionDetailVM> getDetail(Long tranId, String tranType, Long sqlTranId) {
        log.info("get transaction detail");
        SqlParameterSource parameters = new MapSqlParameterSource().addValue("TransactionID", tranId)
                .addValue("TransactionType", tranType)
                .addValue("SQLTransactionID", sqlTranId);
        SimpleJdbcCall jdbcCall = new SimpleJdbcCall(jdbc).withSchemaName("croatia")
                .withProcedureName("usp_GetTransactionDetail")
                .returningResultSet("data", new BeanPropertyRowMapper(TransactionDetailVM.class));
        Map m = jdbcCall.execute(parameters);
        return (List) m.get("data");
    }

    private void generateDailySlotGameReport() {
        log.info("generating daily slot game report");
        SqlParameterSource parameters = new MapSqlParameterSource()
                .addValue("RelatedTransactionID", null)
                .addValue("FromDate", null)
                .addValue("ToDate", null);
        SimpleJdbcCall jdbcCall = new SimpleJdbcCall(jdbc).withSchemaName("croatia")
                .withProcedureName("usp_GenerateDailySlotGameReportTransaction");
        jdbcCall.execute(parameters);
    }

    private void generateDailyTableGameReport() {
        log.info("generating daily table game report");
        SqlParameterSource parameters = new MapSqlParameterSource()
                .addValue("RelatedTransactionID", null)
                .addValue("FromDate", null)
                .addValue("ToDate", null);
        SimpleJdbcCall jdbcCall = new SimpleJdbcCall(jdbc).withSchemaName("croatia")
                .withProcedureName("usp_GenerateDailyTableGameReportTransaction");
        jdbcCall.execute(parameters);
    }

    private void generateDailyAccountReport() {
        log.info("generating daily casino account report");
        SqlParameterSource parameters = new MapSqlParameterSource()
                .addValue("RelatedTransactionID", null)
                .addValue("FromDate", null)
                .addValue("ToDate", null);
        SimpleJdbcCall jdbcCall = new SimpleJdbcCall(jdbc).withSchemaName("croatia")
                .withProcedureName("usp_GenerateDailyIContoReportTransaction");
        jdbcCall.execute(parameters);
    }

    private void generateMonthlyStatementReports() {
        log.info("generating monthly statement report");
        SqlParameterSource parameters = new MapSqlParameterSource()
                .addValue("RelatedTransactionID", null)
                .addValue("FromDate", null)
                .addValue("ToDate", null);
        SimpleJdbcCall jdbcCall = new SimpleJdbcCall(jdbc).withSchemaName("croatia")
                .withProcedureName("usp_GenerateMonthlyReportTransaction");
        jdbcCall.execute(parameters);
    }
}
