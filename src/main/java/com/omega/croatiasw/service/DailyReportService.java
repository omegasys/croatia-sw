package com.omega.croatiasw.service;

import static java.util.Objects.isNull;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.stereotype.Service;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;
import javax.xml.namespace.QName;
import javax.xml.ws.BindingProvider;
import java.io.ByteArrayOutputStream;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.net.URL;
import java.sql.Timestamp;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Map;

import com.omega.croatiasw.enumeration.CroatiaLogStatus;
import com.omega.croatiasw.service.web.report.CasinoOpisnikSmanjen;
import com.omega.croatiasw.service.web.report.ICasinaIzvjestajiPortType;
import com.omega.croatiasw.service.web.report.ICasinaIzvjestajiService;
import com.omega.croatiasw.service.web.report.IzvjestajAutomata;
import com.omega.croatiasw.service.web.report.IzvjestajDatum;
import com.omega.croatiasw.service.web.report.IzvjestajIKonta;
import com.omega.croatiasw.service.web.report.IzvjestajMjesec;
import com.omega.croatiasw.service.web.report.IzvjestajStolova;
import com.omega.croatiasw.service.web.report.MjesecniObracun;
import com.omega.croatiasw.service.web.report.Obracun2;
import com.omega.croatiasw.service.web.report.Odgovor;
import com.omega.croatiasw.service.web.report.PovezanaTransakcija;
import com.omega.croatiasw.service.web.report.StatusGreske;
import com.omega.croatiasw.service.web.report.TransakcijaBrisanje;
import com.omega.croatiasw.service.web.report.TransakcijaIzvjestajAutomata;
import com.omega.croatiasw.service.web.report.TransakcijaIzvjestajIKontaCasina;
import com.omega.croatiasw.service.web.report.TransakcijaIzvjestajStolova;
import com.omega.croatiasw.service.web.report.TransakcijaMjesecniObracun;
import com.omega.croatiasw.service.web.report.Zaglavlje;
import com.omega.croatiasw.service.web.report.ZaglavljeTransakcijaBrisanje;
import com.omega.croatiasw.vm.TransactionDetailVM;
import com.omega.croatiasw.vm.TransactionHeaderVM;

/**
 * @author Wenbo
 */
@Service
public class DailyReportService {
    private final Logger log = LoggerFactory.getLogger(getClass().getSimpleName());

    private JdbcTemplate jdbc;

    private final MailService mailService;

    private ICasinaIzvjestajiPortType servicePort;

    private static final QName SERVICE_NAME = new QName(
            "http://e-porezna.porezna-uprava.hr/casina/2015/services/iCasinaIzvjestajiService",
            "iCasinaIzvjestajiService");


    public DailyReportService(JdbcTemplate jdbc, MailService mailService) {
        this.jdbc = jdbc;
        this.mailService = mailService;
        //port for daily reports
        URL url = getClass().getResource("/wsdl/iCasinaIzvjestajiService.wsdl");
        ICasinaIzvjestajiService iis = new ICasinaIzvjestajiService(url, SERVICE_NAME);
        this.servicePort = iis.getICasinaIzvjestajiServicePort();
        BindingProvider bp = (BindingProvider) this.servicePort;
        Map requestContext = bp.getRequestContext();
        requestContext.put(BindingProvider.ENDPOINT_ADDRESS_PROPERTY,
                "https://cis.porezna-uprava.hr:8443/iCasinaIzvjestajiService");
    }

    public Boolean cancelDailyReport(TransactionHeaderVM headerVM) {
        TransakcijaBrisanje deletionRequest = createDeletionPayload(headerVM);
        String requestStr = "";
        try {
            JAXBContext ctx = JAXBContext.newInstance(TransakcijaBrisanje.class);
            requestStr = toXmlString(ctx, deletionRequest);
            log.info("cancel request payload is:: {}", requestStr);
        } catch (JAXBException e) {
            log.error(e.toString());
        }
        Odgovor response = servicePort.izvjestajBrisanje(deletionRequest);
        return insertTransactionLog(headerVM, response, requestStr);
    }

    public Boolean dailySlotGameReport(TransactionHeaderVM headerVM, List<TransactionDetailVM> detailVMs) {
        TransakcijaIzvjestajAutomata dailySlotGameReportRequest = createDailySlotGamePayload(headerVM, detailVMs);
        String requestStr = "";
        try {
            JAXBContext ctx = JAXBContext.newInstance(TransakcijaIzvjestajAutomata.class);
            requestStr = toXmlString(ctx, dailySlotGameReportRequest);
            log.info("daily slot request payload is:: {}", requestStr);
        } catch (JAXBException e) {
            log.error(e.toString());
        }
        Odgovor response = servicePort.dnevniIzvjestajAutomata(dailySlotGameReportRequest);
        return insertTransactionLog(headerVM, response, requestStr);
    }

    public Boolean dailyTableGameReport(TransactionHeaderVM headerVM, List<TransactionDetailVM> detailVMs) {
        TransakcijaIzvjestajStolova dailyTableGameReportRequest = createDailyTableGamePayload(headerVM, detailVMs);
        String requestStr = "";
        try {
            JAXBContext ctx = JAXBContext.newInstance(TransakcijaIzvjestajStolova.class);
            requestStr = toXmlString(ctx, dailyTableGameReportRequest);
            log.info("daily table request payload is:: {}", requestStr);
        } catch (JAXBException e) {
            log.error(e.toString());
        }
        Odgovor response = servicePort.dnevniIzvjestajStolova(dailyTableGameReportRequest);
        return insertTransactionLog(headerVM, response, requestStr);
    }

    public Boolean dailyAccountReport(TransactionHeaderVM headerVM, List<TransactionDetailVM> detailVMs) {
        TransakcijaIzvjestajIKontaCasina dailyAccountReportRequest = createDailyAccountPayload(headerVM,
                detailVMs.get(0));
        String requestStr = "";
        try {
            JAXBContext ctx = JAXBContext.newInstance(TransakcijaIzvjestajIKontaCasina.class);
            requestStr = toXmlString(ctx, dailyAccountReportRequest);
            log.info("daily account request payload is:: {}", requestStr);
        } catch (JAXBException e) {
            log.error(e.toString());
        }
        Odgovor response = servicePort.dnevniIzvjestajIKontaCasina(dailyAccountReportRequest);
        return insertTransactionLog(headerVM, response, requestStr);
    }

    public Boolean monthlyStatementReport(TransactionHeaderVM headerVM, List<TransactionDetailVM> detailVMs)  {
        TransakcijaMjesecniObracun monthlyStatementRequest = createMonthlyStatementPayload(headerVM, detailVMs.get(0));
        String requestStr = "";
        try {
            JAXBContext ctx = JAXBContext.newInstance(TransakcijaMjesecniObracun.class);
            requestStr = toXmlString(ctx, monthlyStatementRequest);
            log.info("monthly account request payload is:: {}", requestStr);
        } catch (JAXBException e) {
            log.error(e.toString());
        }
        // for testing purpose
//        writeLog(headerVM.getTransactionID(), CroatiaLogStatus.SENT, null, null);
//        return true;
        Odgovor response = servicePort.izvjestajMjesecniObracun(monthlyStatementRequest);
        return insertTransactionLog(headerVM, response, requestStr);
    }

    private Obracun2 calculateGameRevenue(TransactionDetailVM detailVM) {
        BigDecimal gamePayIn = detailVM.getPayInAmountReal()
                .add(detailVM.getPayInAmountPlayableBonus())
                .add(detailVM.getPayInAmountReleasedBonus())
                .negate()
                .setScale(2, RoundingMode.HALF_UP);

        BigDecimal gamePayOut = detailVM.getPayOutAmountReal()
                .add(detailVM.getPayOutAmountPlayableBonus())
                .add(detailVM.getPayOutAmountReleasedBonus())
                .setScale(2, RoundingMode.HALF_UP);

        BigDecimal jackpotPayIn = detailVM.getJackpotAccumulatedAmountReal()
                .add(detailVM.getJackpotAccumulatedAmountPlayableBonus())
                .add(detailVM.getJackpotAccumulatedAmountReleasedBonus())
                .negate()
                .setScale(2, RoundingMode.HALF_UP);

        BigDecimal jackpotPayOut = detailVM.getJackpotPayoutAccumulatedAmountReal()
                .add(detailVM.getJackpotPayoutAccumulatedAmountPlayableBonus())
                .add(detailVM.getJackpotPayoutAccumulatedAmountReleasedBonus())
                .setScale(2, RoundingMode.HALF_UP);

        //game account
        Obracun2 gameAccount = new Obracun2();
        gameAccount.setUplata(gamePayIn);
        gameAccount.setIsplata(gamePayOut);
        gameAccount.setJackpotAkumulacija(jackpotPayIn);
        gameAccount.setJackpotIsplata(jackpotPayOut);

        // difference = payIn - payOut - jackpotAccumulated
        gameAccount.setRazlika(gameAccount.getUplata()
                .subtract(gameAccount.getIsplata())
                .subtract(gameAccount.getJackpotAkumulacija()));
        return gameAccount;
    }

    /**
     * Method to create payloads for daily slot game endpoint
     *
     * @param headerVM  transaction header
     * @param detailVMs transaction details, may return multiple if multiple games are involved
     * @return
     */
    private TransakcijaIzvjestajAutomata createDailySlotGamePayload(TransactionHeaderVM headerVM,
                                                                    List<TransactionDetailVM> detailVMs) {
        Zaglavlje header = createPayloadHeader(headerVM);

        //slot report
        IzvjestajAutomata slotReport = new IzvjestajAutomata();

        //Used index 0 because all rows are about same day but difference games
        //report
        IzvjestajDatum report = new IzvjestajDatum();
        if (detailVMs.get(0).getDateOfReport() != null) {
            try {
                XMLGregorianCalendar calendar = getXMLGregorianCalendar(detailVMs.get(0).getDateOfReport());
                report.setDatumIzvjestaja(calendar);
            } catch (Exception e) {
                log.error("javax.xml.datatype.DatatypeFactory misconfiguration");
            }
        }
        if (detailVMs.get(0).getDateGenerated() != null) {
            try {
                XMLGregorianCalendar calendar = getXMLGregorianCalendar(detailVMs.get(0).getDateGenerated());
                report.setDatumVrijeme(calendar);
            } catch (Exception e) {
                log.error("javax.xml.datatype.DatatypeFactory misconfiguration");
            }
        }
        slotReport.setIzvjestaj(report);

        //casino
        CasinoOpisnikSmanjen casino = new CasinoOpisnikSmanjen();
        casino.setSifra(detailVMs.get(0).getCasino());

        slotReport.setCasino(casino);

        //casino account initialize
        Obracun2 casinoAccount = new Obracun2();
        casinoAccount.setUplata(BigDecimal.ZERO);
        casinoAccount.setIsplata(BigDecimal.ZERO);
        casinoAccount.setJackpotIsplata(BigDecimal.ZERO);
        casinoAccount.setJackpotAkumulacija(BigDecimal.ZERO);
        casinoAccount.setRazlika(BigDecimal.ZERO);

        //games  0..1
        if (detailVMs.size() > 1 || !detailVMs.get(0).getGameCode().isEmpty()) {
            //game 1..n
            IzvjestajAutomata.Igre games = new IzvjestajAutomata.Igre();
            detailVMs.forEach(detailVM -> {
                if (detailVM.getGameCode() != null) {
                    Obracun2 gameAccount = calculateGameRevenue(detailVM);

                    IzvjestajAutomata.Igre.Igra game = new IzvjestajAutomata.Igre.Igra();
                    game.setSifra(detailVM.getGameCode());
                    game.setObracun(gameAccount);
                    games.getIgra().add(game);

                    //update casinoAccount
                    casinoAccount.setUplata(casinoAccount.getUplata()
                            .add(game.getObracun().getUplata())
                            .setScale(2, RoundingMode.HALF_UP));
                    casinoAccount.setIsplata(casinoAccount.getIsplata()
                            .add(game.getObracun().getIsplata())
                            .setScale(2, RoundingMode.HALF_UP));
                    casinoAccount.setJackpotAkumulacija(casinoAccount.getJackpotAkumulacija()
                            .add(game.getObracun().getJackpotAkumulacija())
                            .setScale(2, RoundingMode.HALF_UP));
                    casinoAccount.setJackpotIsplata(casinoAccount.getJackpotIsplata()
                            .add(game.getObracun().getJackpotIsplata())
                            .setScale(2, RoundingMode.HALF_UP));
                }
            });
            slotReport.setIgre(games);
        }

        // difference = payIn - payOut - jackpotAccumulated
        casinoAccount.setRazlika(casinoAccount.getUplata()
                .subtract(casinoAccount.getIsplata())
                .subtract(casinoAccount.getJackpotAkumulacija()));
        slotReport.setObracun(casinoAccount);

        //request
        TransakcijaIzvjestajAutomata dailySlotGameRequest = new TransakcijaIzvjestajAutomata();
        dailySlotGameRequest.setZaglavlje(header);
        dailySlotGameRequest.setIzvjestajAutomata(slotReport);

        return dailySlotGameRequest;
    }

    /**
     * Method to create payloads for daily table game endpoint
     *
     * @param headerVM  transaction header
     * @param detailVMs transaction details, may return multiple if multiple games are involved
     * @return
     */
    private TransakcijaIzvjestajStolova createDailyTableGamePayload(TransactionHeaderVM headerVM,
                                                                    List<TransactionDetailVM> detailVMs) {
        Zaglavlje header = createPayloadHeader(headerVM);

        //table report
        IzvjestajStolova tableReport = new IzvjestajStolova();

        //Used index 0 because all rows are about same day but difference games
        //report
        IzvjestajDatum report = new IzvjestajDatum();
        if (detailVMs.get(0).getDateOfReport() != null) {
            try {
                XMLGregorianCalendar calendar = getXMLGregorianCalendar(detailVMs.get(0).getDateOfReport());
                report.setDatumIzvjestaja(calendar);
            } catch (Exception e) {
                log.error("javax.xml.datatype.DatatypeFactory misconfiguration");
            }
        }
        if (detailVMs.get(0).getDateGenerated() != null) {
            try {
                XMLGregorianCalendar calendar = getXMLGregorianCalendar(detailVMs.get(0).getDateGenerated());
                report.setDatumVrijeme(calendar);
            } catch (Exception e) {
                log.error("javax.xml.datatype.DatatypeFactory misconfiguration");
            }
        }

        tableReport.setIzvjestaj(report);

        //casino
        CasinoOpisnikSmanjen casino = new CasinoOpisnikSmanjen();
        casino.setSifra(detailVMs.get(0).getCasino());

        tableReport.setCasino(casino);

        //casino account initialize
        Obracun2 casinoAccount = new Obracun2();
        casinoAccount.setUplata(BigDecimal.ZERO);
        casinoAccount.setIsplata(BigDecimal.ZERO);
        casinoAccount.setJackpotIsplata(BigDecimal.ZERO);
        casinoAccount.setJackpotAkumulacija(BigDecimal.ZERO);
        casinoAccount.setRazlika(BigDecimal.ZERO);

        //games  0..1
        if (detailVMs.size() > 1 || !detailVMs.get(0).getGameCode().isEmpty()) {
            //game 1..n
            IzvjestajStolova.Igre games = new IzvjestajStolova.Igre();
            detailVMs.forEach(detailVM -> {
                //game account
                Obracun2 gameAccount = calculateGameRevenue(detailVM);

                IzvjestajStolova.Igre.Igra game = new IzvjestajStolova.Igre.Igra();
                game.setSifra(detailVM.getGameCode());
                game.setObracun(gameAccount);
                games.getIgra().add(game);

                //update casinoAccount
                casinoAccount.setUplata(casinoAccount.getUplata()
                        .add(game.getObracun().getUplata())
                        .setScale(2, RoundingMode.HALF_UP));
                casinoAccount.setIsplata(casinoAccount.getIsplata()
                        .add(game.getObracun().getIsplata())
                        .setScale(2, RoundingMode.HALF_UP));
                casinoAccount.setJackpotAkumulacija(casinoAccount.getJackpotAkumulacija()
                        .add(game.getObracun().getJackpotAkumulacija())
                        .setScale(2, RoundingMode.HALF_UP));
                casinoAccount.setJackpotIsplata(casinoAccount.getJackpotIsplata()
                        .add(game.getObracun().getJackpotIsplata())
                        .setScale(2, RoundingMode.HALF_UP));

            });
            tableReport.setIgre(games);
        }

        // difference = payIn - payOut - jackpotAccumulated
        casinoAccount.setRazlika(casinoAccount.getUplata()
                .subtract(casinoAccount.getIsplata())
                .subtract(casinoAccount.getJackpotAkumulacija()));
        tableReport.setObracun(casinoAccount);

        //request
        TransakcijaIzvjestajStolova dailyTableGameRequest = new TransakcijaIzvjestajStolova();
        dailyTableGameRequest.setZaglavlje(header);
        dailyTableGameRequest.setIzvjestajStolova(tableReport);

        return dailyTableGameRequest;
    }

    /**
     * Method to create payloads for daily account endpoint
     *
     * @param headerVM transaction header
     * @param detailVM transaction detail, it will only contain one row of data, as only HRK is supported
     * @return
     */
    private TransakcijaIzvjestajIKontaCasina createDailyAccountPayload(TransactionHeaderVM headerVM,
                                                                       TransactionDetailVM detailVM) {
        Zaglavlje header = createPayloadHeader(headerVM);

        //Used index 0 because all rows are about same day but difference games
        //report
        IzvjestajDatum report = new IzvjestajDatum();
        if (detailVM.getDateOfReport() != null) {
            try {
                XMLGregorianCalendar calendar = getXMLGregorianCalendar(detailVM.getDateOfReport());
                report.setDatumIzvjestaja(calendar);
            } catch (Exception e) {
                log.error("javax.xml.datatype.DatatypeFactory misconfiguration");
            }
        }
        if (detailVM.getDateGenerated() != null) {
            try {
                XMLGregorianCalendar calendar = getXMLGregorianCalendar(detailVM.getDateGenerated());
                report.setDatumVrijeme(calendar);
            } catch (Exception e) {
                log.error("javax.xml.datatype.DatatypeFactory misconfiguration");
            }
        }

        //casino
        CasinoOpisnikSmanjen casino = new CasinoOpisnikSmanjen();
        casino.setSifra(detailVM.getCasino());

        //balance, used index 0 because for now only one currency is needed
        //when multiple currencies are involved, a loop is needed here
        IzvjestajIKonta.Stanje balance = new IzvjestajIKonta.Stanje();
        balance.setPocetnoStanjeIKontaCasina(detailVM.getOpenBalanceReal()
                .add(detailVM.getOpenBalancePlayableBonus())
                .add(detailVM.getOpenBalanceReleasedBonus())
                .setScale(2, RoundingMode.HALF_UP));
        //        balance.setPocetnoStanjeIKontaCasina(BigDecimal.ZERO);
        balance.setIznosUplataNaIKonto(detailVM.getPayInAmountReal()
                .add(detailVM.getPayInAmountPlayableBonus())
                .add(detailVM.getPayInAmountReleasedBonus())
                .negate()
                .setScale(2, RoundingMode.HALF_UP));
        balance.setIznosIsplataSIKonta(detailVM.getPayOutAmountReal()
                .add(detailVM.getPayOutAmountPlayableBonus())
                .add(detailVM.getPayOutAmountReleasedBonus())
                .negate()
                .setScale(2, RoundingMode.HALF_UP));
        balance.setIznosUplataIgara(detailVM.getGamePayInAmountReal()
                .add(detailVM.getGamePayInAmountPlayableBonus())
                .add(detailVM.getGamePayInAmountReleasedBonus())
                .negate()
                .setScale(2, RoundingMode.HALF_UP));
        balance.setIznosIsplataIgara(detailVM.getGamePayOutAmountReal()
                .add(detailVM.getGamePayOutAmountPlayableBonus())
                .add(detailVM.getGamePayOutAmountReleasedBonus())
                .setScale(2, RoundingMode.HALF_UP));
        balance.setIznosUplataOstalo(BigDecimal.ZERO);
        balance.setIznosIsplataOstalo(BigDecimal.ZERO);
        //closing balance
        balance.setZavrsnoStanjeIKontaCasina(balance.getPocetnoStanjeIKontaCasina()
                .add(balance.getIznosUplataNaIKonto())
                .add(balance.getIznosUplataIgara())
                .subtract(balance.getIznosIsplataSIKonta())
                .subtract(balance.getIznosIsplataIgara())
                .setScale(2, RoundingMode.HALF_UP));
        //        balance.setZavrsnoStanjeIKontaCasina(BigDecimal.ZERO);

        //account report
        IzvjestajIKonta accountReport = new IzvjestajIKonta();
        accountReport.setIzvjestaj(report);
        accountReport.setCasino(casino);
        accountReport.setStanje(balance);

        //request
        TransakcijaIzvjestajIKontaCasina dailyAccountReportRequest = new TransakcijaIzvjestajIKontaCasina();
        dailyAccountReportRequest.setZaglavlje(header);
        dailyAccountReportRequest.setIzvjestajIKontaCasina(accountReport);

        return dailyAccountReportRequest;
    }

    /**
     * Method to create payloads for monthly statement endpoint
     *
     * @param headerVM transaction header
     * @param detailVM transaction detail, it will only contain one row of data, as only HRK is supported
     * @return
     */
    private TransakcijaMjesecniObracun createMonthlyStatementPayload(TransactionHeaderVM headerVM,
                                                                     TransactionDetailVM detailVM) {
        Zaglavlje header = createPayloadHeader(headerVM);
        Double feeRate = 0.15;

        //report
        IzvjestajMjesec report = new IzvjestajMjesec();
        if (detailVM.getDateOfReport() != null) {
            try {
                XMLGregorianCalendar calendar = getXMLGregorianCalendar(detailVM.getDateOfReport());
                report.setMjesec(String.format("%02d", calendar.getMonth()));
                report.setGodina(String.valueOf(calendar.getYear()));
            } catch (Exception e) {
                log.error("javax.xml.datatype.DatatypeFactory misconfiguration");
            }
        }
        if (detailVM.getDateGenerated() != null) {
            try {
                XMLGregorianCalendar calendar = getXMLGregorianCalendar(detailVM.getDateGenerated());
                report.setDatumVrijeme(calendar);
            } catch (Exception e) {
                log.error("javax.xml.datatype.DatatypeFactory misconfiguration");
            }
        }

        //casino
        CasinoOpisnikSmanjen casino = new CasinoOpisnikSmanjen();
        casino.setSifra(detailVM.getCasino());

        //slot games 0..1
        MjesecniObracun.IgreNaAutomatima slotGames = new MjesecniObracun.IgreNaAutomatima();
        slotGames.setUplata(detailVM.getSlotPayInAmountReal()
                .add(detailVM.getSlotPayInAmountReleasedBonus())
                .add(detailVM.getSlotPayInAmountPlayableBonus())
                .negate()
                .setScale(2, RoundingMode.HALF_UP));
        slotGames.setIsplata(detailVM.getSlotPayOutAmountReal()
                .add(detailVM.getSlotPayOutAmountReleasedBonus())
                .add(detailVM.getSlotPayOutAmountPlayableBonus())
                .setScale(2, RoundingMode.HALF_UP));
        slotGames.setJackpotAkumulacija(detailVM.getSlotJackpotAccumulatedAmountReal()
                .add(detailVM.getSlotJackpotAccumulatedPayInAmountReleasedBonus())
                .add(detailVM.getSlotJackpotAccumulatedPayInAmountPlayableBonus())
                .negate()
                .setScale(2, RoundingMode.HALF_UP));
        slotGames.setJackpotIsplata(detailVM.getSlotJackpotPayOutAmountReal()
                .add(detailVM.getSlotJackpotPayOutAmountReleasedBonus())
                .add(detailVM.getSlotJackpotPayOutAmountPlayableBonus())
                .setScale(2, RoundingMode.HALF_UP));
        //difference = payin - payout - jackpot accumulated payin
        slotGames.setOsnovica(
                slotGames.getUplata().subtract(slotGames.getIsplata()).subtract(slotGames.getJackpotAkumulacija()));
        slotGames.setNaknada(
                slotGames.getOsnovica().multiply(BigDecimal.valueOf(feeRate)).setScale(2, RoundingMode.HALF_UP));

        //table games 0..1
        MjesecniObracun.Stolovi tableGames = new MjesecniObracun.Stolovi();
        BigDecimal payIn = detailVM.getTablePayInAmountReal()
                .add(detailVM.getTablePayInAmountReleasedBonus())
                .add(detailVM.getTablePayInAmountPlayableBonus())
                .negate()
                .setScale(2, RoundingMode.HALF_UP);
        BigDecimal payOut = detailVM.getTablePayOutAmountReal()
                .add(detailVM.getTablePayOutAmountReleasedBonus())
                .add(detailVM.getTablePayOutAmountPlayableBonus())
                .setScale(2, RoundingMode.HALF_UP);
        BigDecimal jackpotAccumulated = detailVM.getTableJackpotAccumulatedAmountReal()
                .add(detailVM.getTableJackpotAccumulatedPayInAmountReleasedBonus())
                .add(detailVM.getTableJackpotAccumulatedPayInAmountPlayableBonus())
                .negate()
                .setScale(2, RoundingMode.HALF_UP);

        tableGames.setOsnovica(payIn.subtract(payOut).subtract(jackpotAccumulated));
        tableGames.setNaknada(
                tableGames.getOsnovica().multiply(BigDecimal.valueOf(feeRate)).setScale(2, RoundingMode.HALF_UP));

        //Total
        MjesecniObracun.Ukupno total = new MjesecniObracun.Ukupno();
        total.setNaknada(slotGames.getNaknada().add(tableGames.getNaknada()));

        //table report
        MjesecniObracun montlyStatement = new MjesecniObracun();
        montlyStatement.setIzvjestaj(report);
        montlyStatement.setCasino(casino);
        montlyStatement.setIgreNaAutomatima(slotGames);
        montlyStatement.setStolovi(tableGames);
        montlyStatement.setTurniri(null);
        montlyStatement.setUkupno(total);

        //request
        TransakcijaMjesecniObracun montlyStatementRequest = new TransakcijaMjesecniObracun();
        montlyStatementRequest.setZaglavlje(header);
        montlyStatementRequest.setMjesecniObracun(montlyStatement);

        return montlyStatementRequest;
    }

    private Zaglavlje createPayloadHeader(TransactionHeaderVM headerVM) {
        //header
        Zaglavlje header = new Zaglavlje();
        header.setID(headerVM.getGUID());
        header.setOIB(headerVM.getPersonalID());
        if (headerVM.getDateTime() != null) {
            try {
                XMLGregorianCalendar calendar = getXMLGregorianCalendar(headerVM.getDateTime());
                header.setDatumVrijeme(calendar);
            } catch (Exception e) {
                log.error("javax.xml.datatype.DatatypeFactory misconfiguration");
            }
        }

        if (headerVM.getRelatedTransactionID() != null) {
            //related transaction
            PovezanaTransakcija relatedTransaction = new PovezanaTransakcija();
            relatedTransaction.setID(headerVM.getRelatedTransactionGUID());
            if (headerVM.getRelatedTransactionDateTime() != null) {
                try {
                    XMLGregorianCalendar calendar = getXMLGregorianCalendar(headerVM.getRelatedTransactionDateTime());
                    relatedTransaction.setDatumVrijeme(calendar);
                } catch (Exception e) {
                    log.error("javax.xml.datatype.DatatypeFactory misconfiguration");
                }
            }
            header.setPovezanaTransakcija(relatedTransaction);
        }

        return header;
    }

    private TransakcijaBrisanje createDeletionPayload(TransactionHeaderVM headerVM) {
        TransakcijaBrisanje deletionRequest = new TransakcijaBrisanje();

        //header
        ZaglavljeTransakcijaBrisanje header = new ZaglavljeTransakcijaBrisanje();
        header.setID(headerVM.getGUID());
        header.setOIB(headerVM.getPersonalID());
        if (headerVM.getDateTime() != null) {
            try {
                XMLGregorianCalendar calendar = getXMLGregorianCalendar(headerVM.getDateTime());
                header.setDatumVrijeme(calendar);
            } catch (Exception e) {
                log.error("javax.xml.datatype.DatatypeFactory misconfiguration");
            }
        }

        //related transaction
        PovezanaTransakcija relatedTransaction = new PovezanaTransakcija();
        relatedTransaction.setID(headerVM.getRelatedTransactionGUID());
        if (headerVM.getRelatedTransactionDateTime() != null) {
            try {
                XMLGregorianCalendar calendar = getXMLGregorianCalendar(headerVM.getRelatedTransactionDateTime());
                relatedTransaction.setDatumVrijeme(calendar);
            } catch (Exception e) {
                log.error("javax.xml.datatype.DatatypeFactory misconfiguration");
            }
        }
        header.setPovezanaTransakcija(relatedTransaction);

        deletionRequest.setZaglavlje(header);

        return deletionRequest;
    }

    /**
     * status report logs
     *
     * @param headerVM transaction header
     * @param response response defined under status report wsdl
     */
    private Boolean insertTransactionLog(TransactionHeaderVM headerVM, Odgovor response, String requestStr) {
        Timestamp timestamp = new Timestamp(response.getDatumVrijeme().toGregorianCalendar().getTimeInMillis());

        if (StatusGreske.U.equals(response.getStatus())) {
            writeLog(headerVM.getTransactionID(), CroatiaLogStatus.SENT, timestamp, requestStr);
            return true;
        } else {
            List<Odgovor.Greske.Greska> errors = response.getGreske().getGreska();
            String errorStr = "";
            for (int i = 0; i < errors.size(); i++) {
                errorStr = errorStr.concat(
                        "Code: " + errors.get(i).getSifra() + ", Description: " + errors.get(i).getOpis() + ";");
            }
            writeLog(headerVM.getTransactionID(), CroatiaLogStatus.ERROR, timestamp,
                    (isNull(requestStr) ? "" : requestStr+",") + errorStr);
            if (!mailService.hasThisTransactionBeenSent(headerVM.getTransactionID())) {
                mailService.sendJobFailEmail(headerVM);
            }
            //return false here because we want to job to stop right here
            return false;
        }
    }

    private void writeLog(Long tranId, CroatiaLogStatus status, Timestamp date, String message) {
        log.info("write transaction log");
        message = getTruncatedMessage(message);
        SqlParameterSource parameters = new MapSqlParameterSource().addValue("TransactionID", tranId)
                .addValue("Status", status.getCode())
                .addValue("Date", date)
                .addValue("Message", message);
        SimpleJdbcCall jdbcCall = new SimpleJdbcCall(jdbc).withSchemaName("croatia").withProcedureName("usp_WriteLog");
        jdbcCall.execute(parameters);
    }

    /**
     * make sure the message is not longer than 2000.
     * @param message
     * @return
     */
    private String getTruncatedMessage(String message) {
        if (message.length() > 2000) {
            int length = message.length();
            log.info("original size {} ", length);
            String head = message.substring(0, 1000);
            String tail = message.substring(length - 1000);
            message = head + tail;
        }
        return message;
    }

    private XMLGregorianCalendar getXMLGregorianCalendar(Timestamp timestamp) throws DatatypeConfigurationException {
        Date date = new Date(timestamp.getTime());
        GregorianCalendar gregorianCalendar = new GregorianCalendar();
        gregorianCalendar.setTime(date);
        DatatypeFactory datatypeFactory = DatatypeFactory.newInstance();
        return datatypeFactory.newXMLGregorianCalendar(gregorianCalendar);
    }

    public String toXmlString(JAXBContext context, Object element) {
        try {
            Marshaller marshaller = context.createMarshaller();
            marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);

            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            marshaller.marshal(element, baos);
            return baos.toString();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "";
    }
}
