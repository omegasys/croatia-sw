package com.omega.croatiasw.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.stereotype.Service;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;
import javax.xml.namespace.QName;
import javax.xml.ws.BindingProvider;
import java.io.ByteArrayOutputStream;
import java.net.URL;
import java.sql.Timestamp;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Map;

import com.omega.croatiasw.enumeration.CroatiaLogStatus;
import com.omega.croatiasw.service.web.status.CasinoOpisnik;
import com.omega.croatiasw.service.web.status.CasinoOpisnikSmanjen;
import com.omega.croatiasw.service.web.status.EnumStatus;
import com.omega.croatiasw.service.web.status.ICasinaPromjenaStatusaPortType;
import com.omega.croatiasw.service.web.status.ICasinaPromjenaStatusaService;
import com.omega.croatiasw.service.web.status.IgraOpisnik;
import com.omega.croatiasw.service.web.status.JackpotOpisnik;
import com.omega.croatiasw.service.web.status.JackpotOpisnikSmanjen;
import com.omega.croatiasw.service.web.status.Odgovor;
import com.omega.croatiasw.service.web.status.PovezanaTransakcija;
import com.omega.croatiasw.service.web.status.Priredjivac;
import com.omega.croatiasw.service.web.status.Status;
import com.omega.croatiasw.service.web.status.StatusCasina;
import com.omega.croatiasw.service.web.status.StatusGreske;
import com.omega.croatiasw.service.web.status.StatusIgre;
import com.omega.croatiasw.service.web.status.StatusJackpota;
import com.omega.croatiasw.service.web.status.TransakcijaStatusCasina;
import com.omega.croatiasw.service.web.status.TransakcijaStatusIgre;
import com.omega.croatiasw.service.web.status.TransakcijaStatusJackpota;
import com.omega.croatiasw.service.web.status.VrstaCasina;
import com.omega.croatiasw.service.web.status.VrstaIgre;
import com.omega.croatiasw.service.web.status.Zaglavlje;
import com.omega.croatiasw.vm.TransactionDetailVM;
import com.omega.croatiasw.vm.TransactionHeaderVM;

/**
 * @author Wenbo
 */
@Service
public class StatusChangeReportService {
    private final Logger log = LoggerFactory.getLogger(getClass().getSimpleName());

    private JdbcTemplate jdbc;

    private final MailService mailService;

    private ICasinaPromjenaStatusaPortType servicePort;

    private static final QName SERVICE_NAME = new QName(
            "http://e-porezna.porezna-uprava.hr/casina/2015/services/iCasinaPromjenaStatusaService",
            "iCasinaPromjenaStatusaService");

    public StatusChangeReportService(JdbcTemplate jdbc, MailService mailService) {
        this.jdbc = jdbc;
        this.mailService = mailService;
        //port for status change reports
        URL url = getClass().getResource("/wsdl/iCasinaPromjenaStatusaService.wsdl");
        ICasinaPromjenaStatusaService ss = new ICasinaPromjenaStatusaService(url, SERVICE_NAME);
        this.servicePort = ss.getICasinaPromjenaStatusaServicePort();
        BindingProvider bp = (BindingProvider) this.servicePort;
        Map requestContext = bp.getRequestContext();
        requestContext.put(BindingProvider.ENDPOINT_ADDRESS_PROPERTY,
                "https://cis.porezna-uprava.hr:8443/iCasinaPromjenaStatusaService");
    }

    public Boolean casinoStatusChangeReport(TransactionHeaderVM headerVM, List<TransactionDetailVM> detailVMs) {
        TransakcijaStatusCasina casinoStatusChangeRequest = createCasinoStatusChangePayload(headerVM, detailVMs.get(0));
        try {
            JAXBContext ctx = JAXBContext.newInstance(TransakcijaStatusCasina.class);
            String requestStr = toXmlString(ctx, casinoStatusChangeRequest);
            log.info("casino status request payload is:: {}", requestStr);
        } catch (JAXBException e) {
            log.error(e.toString());
        }
        Odgovor response = servicePort.promjenaStatusCasina(casinoStatusChangeRequest);
        return insertTransactionLog(headerVM, response);
    }

    public Boolean gameStatusChangeReport(TransactionHeaderVM headerVM, List<TransactionDetailVM> detailVMs) {
        TransakcijaStatusIgre gameStatusChangeRequest = createGameStatusChangePayload(headerVM, detailVMs);
        try {
            JAXBContext ctx = JAXBContext.newInstance(TransakcijaStatusIgre.class);
            String requestStr = toXmlString(ctx, gameStatusChangeRequest);
            log.info("game status request payload is:: {}", requestStr);
        } catch (JAXBException e) {
            log.error(e.toString());
        }
        Odgovor response = servicePort.promjenaStatusIgre(gameStatusChangeRequest);
        return insertTransactionLog(headerVM, response);
    }

    public Boolean jackpotStatusChangeReport(TransactionHeaderVM headerVM, List<TransactionDetailVM> detailVMs) {
        TransakcijaStatusJackpota jackpotStatusChangeRequest = createJackpotStatusChangePayload(headerVM, detailVMs);
        try {
            JAXBContext ctx = JAXBContext.newInstance(TransakcijaStatusJackpota.class);
            String requestStr = toXmlString(ctx, jackpotStatusChangeRequest);
            log.info("game status request payload is:: {}", requestStr);
        } catch (JAXBException e) {
            log.error(e.toString());
        }
        Odgovor response = servicePort.promjenaStatusJackpota(jackpotStatusChangeRequest);
        return insertTransactionLog(headerVM, response);
    }

    /**
     * Method to create payloads for casino status change endpoint
     *
     * @param headerVM transaction header
     * @param detailVM transaction detail
     * @return
     */
    private TransakcijaStatusCasina createCasinoStatusChangePayload(TransactionHeaderVM headerVM,
                                                                    TransactionDetailVM detailVM) {
        TransakcijaStatusCasina casinoStatusChangeRequest = new TransakcijaStatusCasina();
        //casino
        CasinoOpisnik casino = new CasinoOpisnik();
        casino.setSifra(detailVM.getCode());
        casino.setVrsta(VrstaCasina.valueOf(detailVM.getType()));
        casino.setNaziv(detailVM.getName());

        //operator
        Priredjivac operator = new Priredjivac();
        operator.setOIB(detailVM.getPersonalID());
        operator.setNaziv(detailVM.getPersonalName());

        //status
        Status status = new Status();
        status.setStatus(EnumStatus.valueOf(detailVM.getStatus()));
        if (detailVM.getDate() != null) {
            try {
                XMLGregorianCalendar calendar = getXMLGregorianCalendar(detailVM.getDate());
                status.setDatumVrijeme(calendar);
            } catch (Exception e) {
                log.error("javax.xml.datatype.DatatypeFactory misconfiguration");
            }
        }

        //casino status
        StatusCasina casinoStatus = new StatusCasina();
        casinoStatus.setCasino(casino);
        casinoStatus.setPriredjivac(operator);
        casinoStatus.setStatus(status);

        //request
        Zaglavlje header = createPayloadHeader(headerVM);
        casinoStatusChangeRequest.setZaglavlje(header);
        casinoStatusChangeRequest.setStatusCasina(casinoStatus);

        return casinoStatusChangeRequest;
    }

    /**
     * Method to create payloads for game status change endpoint
     *
     * @param headerVM  transaction header
     * @param detailVMs transaction details, may return multiple if it has multiple jackpots
     * @return
     */
    private TransakcijaStatusIgre createGameStatusChangePayload(TransactionHeaderVM headerVM,
                                                                List<TransactionDetailVM> detailVMs) {
        TransakcijaStatusIgre gameStatusChangeRequest = new TransakcijaStatusIgre();

        //game status
        StatusIgre gameStatus = new StatusIgre();

        //Used index 0 because all rows are about same game but difference jackpots
        //game
        IgraOpisnik game = new IgraOpisnik();
        game.setSifra(detailVMs.get(0).getCode());
        game.setVrsta(VrstaIgre.valueOf(detailVMs.get(0).getType()));
        game.setNaziv(detailVMs.get(0).getName());
        gameStatus.setIgra(game);

        //casino descriptor
        CasinoOpisnikSmanjen casino = new CasinoOpisnikSmanjen();
        casino.setSifra(detailVMs.get(0).getCasino());
        gameStatus.setCasino(casino);

        //jackpots 0..1
        if (detailVMs.size() > 1 || !detailVMs.get(0).getJackpot().isEmpty()) {
            //game jackpot 1..n
            StatusIgre.Jackpotovi jackpots = new StatusIgre.Jackpotovi();
            detailVMs.forEach(detailVM -> {
                if (detailVM.getJackpot() != null) {
                    JackpotOpisnikSmanjen jackpot = new JackpotOpisnikSmanjen();
                    jackpot.setSifra(detailVM.getJackpot());
                    jackpots.getJackpot().add(jackpot);
                }
            });
            gameStatus.setJackpotovi(jackpots);
        }


        //status
        Status status = new Status();
        status.setStatus(EnumStatus.valueOf(detailVMs.get(0).getStatus()));
        if (detailVMs.get(0).getDate() != null) {
            try {
                XMLGregorianCalendar calendar = getXMLGregorianCalendar(detailVMs.get(0).getDate());
                status.setDatumVrijeme(calendar);
            } catch (Exception e) {
                log.error("javax.xml.datatype.DatatypeFactory misconfiguration");
            }
        }
        gameStatus.setStatus(status);

        //request
        Zaglavlje header = createPayloadHeader(headerVM);
        gameStatusChangeRequest.setZaglavlje(header);
        gameStatusChangeRequest.setStatusIgre(gameStatus);

        return gameStatusChangeRequest;
    }

    /**
     * Method to create payloads for jackpot status change endpoint
     *
     * @param headerVM  transaction header
     * @param detailVMs transaction details, may return multiple if it has multiple jackpots
     * @return
     */
    private TransakcijaStatusJackpota createJackpotStatusChangePayload(TransactionHeaderVM headerVM,
                                                                       List<TransactionDetailVM> detailVMs) {

        TransakcijaStatusJackpota jackpotStatusChangeRequest = new TransakcijaStatusJackpota();

        //Used index 0 because all rows are about same jackpot but difference casino
        //jackpot
        JackpotOpisnik jackpot = new JackpotOpisnik();
        jackpot.setSifra(detailVMs.get(0).getCode());
        jackpot.setNaziv(detailVMs.get(0).getName());

        //casino 1..n
        StatusJackpota.Casina casinos = new StatusJackpota.Casina();
        detailVMs.forEach(detailVM -> {
            if (detailVM.getCasino() != null) {
                CasinoOpisnikSmanjen casino = new CasinoOpisnikSmanjen();
                casino.setSifra(detailVM.getCasino());
                casinos.getCasino().add(casino);
            }
        });

        //status
        Status status = new Status();
        status.setStatus(EnumStatus.valueOf(detailVMs.get(0).getStatus()));
        if (detailVMs.get(0).getDate() != null) {
            try {
                XMLGregorianCalendar calendar = getXMLGregorianCalendar(detailVMs.get(0).getDate());
                status.setDatumVrijeme(calendar);
            } catch (Exception e) {
                log.error("javax.xml.datatype.DatatypeFactory misconfiguration");
            }
        }

        //game status
        StatusJackpota jackpotStatus = new StatusJackpota();
        jackpotStatus.setJackpot(jackpot);
        jackpotStatus.setCasina(casinos);
        jackpotStatus.setStatus(status);

        //request
        Zaglavlje header = createPayloadHeader(headerVM);
        jackpotStatusChangeRequest.setZaglavlje(header);
        jackpotStatusChangeRequest.setStatusJackpota(jackpotStatus);

        return jackpotStatusChangeRequest;
    }

    private Zaglavlje createPayloadHeader(TransactionHeaderVM headerVM) {
        //header
        Zaglavlje header = new Zaglavlje();
        header.setID(headerVM.getGUID());
        header.setOIB(headerVM.getPersonalID());
        if (headerVM.getDateTime() != null) {
            try {
                XMLGregorianCalendar calendar = getXMLGregorianCalendar(headerVM.getDateTime());
                header.setDatumVrijeme(calendar);
            } catch (Exception e) {
                log.error("javax.xml.datatype.DatatypeFactory misconfiguration");
            }
        }
        if (headerVM.getRelatedTransactionID() != null) {
            //related transaction
            PovezanaTransakcija relatedTransaction = new PovezanaTransakcija();
            relatedTransaction.setID(headerVM.getRelatedTransactionGUID());
            if (headerVM.getRelatedTransactionDateTime() != null) {
                try {
                    XMLGregorianCalendar calendar = getXMLGregorianCalendar(headerVM.getRelatedTransactionDateTime());
                    relatedTransaction.setDatumVrijeme(calendar);
                } catch (Exception e) {
                    log.error("javax.xml.datatype.DatatypeFactory misconfiguration");
                }
            }
            header.setPovezanaTransakcija(relatedTransaction);
        }
        return header;
    }

    /**
     * status report logs
     *
     * @param headerVM transaction header
     * @param response response defined under status report wsdl
     */
    private Boolean insertTransactionLog(TransactionHeaderVM headerVM, Odgovor response) {
        Timestamp timestamp = new Timestamp(response.getDatumVrijeme().toGregorianCalendar().getTimeInMillis());

        if (StatusGreske.U.equals(response.getStatus())) {
            writeLog(headerVM.getTransactionID(), CroatiaLogStatus.SENT, timestamp, null);
            return true;
        } else {
            List<Odgovor.Greske.Greska> errors = response.getGreske().getGreska();
            String errorStr = "";
            for (int i = 0; i < errors.size(); i++) {
                errorStr = errorStr.concat(
                        "Code: " + errors.get(i).getSifra() + ", Description: " + errors.get(i).getOpis() + ";");
            }
            writeLog(headerVM.getTransactionID(), CroatiaLogStatus.ERROR, timestamp, errorStr);
            if (!mailService.hasThisTransactionBeenSent(headerVM.getTransactionID())) {
                mailService.sendJobFailEmail(headerVM);
            }
            //return false here because we want to job to stop right here
            return false;
        }
    }

    private void writeLog(Long tranId, CroatiaLogStatus status, Timestamp date, String message) {
        log.info("write transaction log");
        SqlParameterSource parameters = new MapSqlParameterSource().addValue("TransactionID", tranId)
                .addValue("Status", status.getCode())
                .addValue("Date", date)
                .addValue("Message", message);
        SimpleJdbcCall jdbcCall = new SimpleJdbcCall(jdbc).withSchemaName("croatia").withProcedureName("usp_WriteLog");
        jdbcCall.execute(parameters);
    }

    private XMLGregorianCalendar getXMLGregorianCalendar(Timestamp timestamp) throws DatatypeConfigurationException {
        Date date = new Date(timestamp.getTime());
        GregorianCalendar gregorianCalendar = new GregorianCalendar();
        gregorianCalendar.setTime(date);
        DatatypeFactory datatypeFactory = DatatypeFactory.newInstance();
        return datatypeFactory.newXMLGregorianCalendar(gregorianCalendar);
    }

    public String toXmlString(JAXBContext context, Object element) {
        try {
            Marshaller marshaller = context.createMarshaller();
            marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);

            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            marshaller.marshal(element, baos);
            return baos.toString();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "";
    }
}
