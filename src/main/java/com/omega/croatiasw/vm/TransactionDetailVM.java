package com.omega.croatiasw.vm;

import java.math.BigDecimal;
import java.sql.Timestamp;

/**
 * @author Wenbo at Mar 18th, 2019
 */
public class TransactionDetailVM {
    private Long transactionID;
    private String code;
    private String name;
    private String type;
    private Timestamp date;
    private String status;
    private String casino;
    private String jackpot;
    private String product;
    private String personalID;
    private String personalName;

    //daily game report fields
    private Timestamp dateOfReport;
    private Timestamp dateGenerated;
    private String gameCode;
    private BigDecimal payInAmountReal;
    private BigDecimal payInAmountReleasedBonus;
    private BigDecimal payInAmountPlayableBonus;
    private BigDecimal payOutAmountReal;
    private BigDecimal payOutAmountReleasedBonus;
    private BigDecimal payOutAmountPlayableBonus;
    private BigDecimal jackpotAccumulatedAmountReal;
    private BigDecimal jackpotAccumulatedAmountReleasedBonus;
    private BigDecimal jackpotAccumulatedAmountPlayableBonus;
    private BigDecimal jackpotPayoutAccumulatedAmountReal;
    private BigDecimal jackpotPayoutAccumulatedAmountReleasedBonus;
    private BigDecimal jackpotPayoutAccumulatedAmountPlayableBonus;

    //daily account report fields
    private BigDecimal openBalanceReal;
    private BigDecimal openBalanceReleasedBonus;
    private BigDecimal openBalancePlayableBonus;
    private BigDecimal gamePayInAmountReal;
    private BigDecimal gamePayInAmountReleasedBonus;
    private BigDecimal gamePayInAmountPlayableBonus;
    private BigDecimal gamePayOutAmountReal;
    private BigDecimal gamePayOutAmountReleasedBonus;
    private BigDecimal gamePayOutAmountPlayableBonus;

    //monthly statement fields
    private BigDecimal slotPayInAmountReal;
    private BigDecimal slotPayInAmountReleasedBonus;
    private BigDecimal slotPayInAmountPlayableBonus;
    private BigDecimal slotPayOutAmountReal;
    private BigDecimal slotPayOutAmountReleasedBonus;
    private BigDecimal slotPayOutAmountPlayableBonus;
    private BigDecimal slotJackpotAccumulatedAmountReal;
    private BigDecimal slotJackpotAccumulatedPayInAmountReleasedBonus;
    private BigDecimal slotJackpotAccumulatedPayInAmountPlayableBonus;
    private BigDecimal slotJackpotPayOutAmountReal;
    private BigDecimal slotJackpotPayOutAmountReleasedBonus;
    private BigDecimal slotJackpotPayOutAmountPlayableBonus;
    private BigDecimal tablePayInAmountReal;
    private BigDecimal tablePayInAmountReleasedBonus;
    private BigDecimal tablePayInAmountPlayableBonus;
    private BigDecimal tablePayOutAmountReal;
    private BigDecimal tablePayOutAmountReleasedBonus;
    private BigDecimal tablePayOutAmountPlayableBonus;
    private BigDecimal tableJackpotAccumulatedAmountReal;
    private BigDecimal tableJackpotAccumulatedPayInAmountReleasedBonus;
    private BigDecimal tableJackpotAccumulatedPayInAmountPlayableBonus;
    private BigDecimal tableJackpotPayOutAmountReal;
    private BigDecimal tableJackpotPayOutAmountReleasedBonus;
    private BigDecimal tableJackpotPayOutAmountPlayableBonus;

    private String currency;

    public Long getTransactionID() {
        return transactionID;
    }

    public void setTransactionID(Long transactionID) {
        this.transactionID = transactionID;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Timestamp getDate() {
        return date;
    }

    public void setDate(Timestamp date) {
        this.date = date;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getCasino() {
        return casino;
    }

    public void setCasino(String casino) {
        this.casino = casino;
    }

    public String getJackpot() {
        return jackpot;
    }

    public void setJackpot(String jackpot) {
        this.jackpot = jackpot;
    }

    public String getProduct() {
        return product;
    }

    public void setProduct(String product) {
        this.product = product;
    }

    public String getPersonalID() {
        return personalID;
    }

    public void setPersonalID(String personalID) {
        this.personalID = personalID;
    }

    public String getPersonalName() {
        return personalName;
    }

    public void setPersonalName(String personalName) {
        this.personalName = personalName;
    }

    public Timestamp getDateOfReport() {
        return dateOfReport;
    }

    public void setDateOfReport(Timestamp dateOfReport) {
        this.dateOfReport = dateOfReport;
    }

    public Timestamp getDateGenerated() {
        return dateGenerated;
    }

    public void setDateGenerated(Timestamp dateGenerated) {
        this.dateGenerated = dateGenerated;
    }

    public String getGameCode() {
        return gameCode;
    }

    public void setGameCode(String gameCode) {
        this.gameCode = gameCode;
    }

    public BigDecimal getPayInAmountReal() {
        return payInAmountReal;
    }

    public void setPayInAmountReal(BigDecimal payInAmountReal) {
        this.payInAmountReal = payInAmountReal;
    }

    public BigDecimal getPayInAmountReleasedBonus() {
        return payInAmountReleasedBonus;
    }

    public void setPayInAmountReleasedBonus(BigDecimal payInAmountReleasedBonus) {
        this.payInAmountReleasedBonus = payInAmountReleasedBonus;
    }

    public BigDecimal getPayInAmountPlayableBonus() {
        return payInAmountPlayableBonus;
    }

    public void setPayInAmountPlayableBonus(BigDecimal payInAmountPlayableBonus) {
        this.payInAmountPlayableBonus = payInAmountPlayableBonus;
    }

    public BigDecimal getPayOutAmountReal() {
        return payOutAmountReal;
    }

    public void setPayOutAmountReal(BigDecimal payOutAmountReal) {
        this.payOutAmountReal = payOutAmountReal;
    }

    public BigDecimal getPayOutAmountReleasedBonus() {
        return payOutAmountReleasedBonus;
    }

    public void setPayOutAmountReleasedBonus(BigDecimal payOutAmountReleasedBonus) {
        this.payOutAmountReleasedBonus = payOutAmountReleasedBonus;
    }

    public BigDecimal getPayOutAmountPlayableBonus() {
        return payOutAmountPlayableBonus;
    }

    public void setPayOutAmountPlayableBonus(BigDecimal payOutAmountPlayableBonus) {
        this.payOutAmountPlayableBonus = payOutAmountPlayableBonus;
    }

    public BigDecimal getJackpotAccumulatedAmountReal() {
        return jackpotAccumulatedAmountReal;
    }

    public void setJackpotAccumulatedAmountReal(BigDecimal jackpotAccumulatedAmountReal) {
        this.jackpotAccumulatedAmountReal = jackpotAccumulatedAmountReal;
    }

    public BigDecimal getJackpotAccumulatedAmountReleasedBonus() {
        return jackpotAccumulatedAmountReleasedBonus;
    }

    public void setJackpotAccumulatedAmountReleasedBonus(BigDecimal jackpotAccumulatedAmountReleasedBonus) {
        this.jackpotAccumulatedAmountReleasedBonus = jackpotAccumulatedAmountReleasedBonus;
    }

    public BigDecimal getJackpotAccumulatedAmountPlayableBonus() {
        return jackpotAccumulatedAmountPlayableBonus;
    }

    public void setJackpotAccumulatedAmountPlayableBonus(BigDecimal jackpotAccumulatedAmountPlayableBonus) {
        this.jackpotAccumulatedAmountPlayableBonus = jackpotAccumulatedAmountPlayableBonus;
    }

    public BigDecimal getJackpotPayoutAccumulatedAmountReal() {
        return jackpotPayoutAccumulatedAmountReal;
    }

    public void setJackpotPayoutAccumulatedAmountReal(BigDecimal jackpotPayoutAccumulatedAmountReal) {
        this.jackpotPayoutAccumulatedAmountReal = jackpotPayoutAccumulatedAmountReal;
    }

    public BigDecimal getJackpotPayoutAccumulatedAmountReleasedBonus() {
        return jackpotPayoutAccumulatedAmountReleasedBonus;
    }

    public void setJackpotPayoutAccumulatedAmountReleasedBonus(BigDecimal jackpotPayoutAccumulatedAmountReleasedBonus) {
        this.jackpotPayoutAccumulatedAmountReleasedBonus = jackpotPayoutAccumulatedAmountReleasedBonus;
    }

    public BigDecimal getJackpotPayoutAccumulatedAmountPlayableBonus() {
        return jackpotPayoutAccumulatedAmountPlayableBonus;
    }

    public void setJackpotPayoutAccumulatedAmountPlayableBonus(BigDecimal jackpotPayoutAccumulatedAmountPlayableBonus) {
        this.jackpotPayoutAccumulatedAmountPlayableBonus = jackpotPayoutAccumulatedAmountPlayableBonus;
    }

    public BigDecimal getOpenBalanceReal() {
        return openBalanceReal;
    }

    public void setOpenBalanceReal(BigDecimal openBalanceReal) {
        this.openBalanceReal = openBalanceReal;
    }

    public BigDecimal getOpenBalanceReleasedBonus() {
        return openBalanceReleasedBonus;
    }

    public void setOpenBalanceReleasedBonus(BigDecimal openBalanceReleasedBonus) {
        this.openBalanceReleasedBonus = openBalanceReleasedBonus;
    }

    public BigDecimal getOpenBalancePlayableBonus() {
        return openBalancePlayableBonus;
    }

    public void setOpenBalancePlayableBonus(BigDecimal openBalancePlayableBonus) {
        this.openBalancePlayableBonus = openBalancePlayableBonus;
    }

    public BigDecimal getGamePayInAmountReal() {
        return gamePayInAmountReal;
    }

    public void setGamePayInAmountReal(BigDecimal gamePayInAmountReal) {
        this.gamePayInAmountReal = gamePayInAmountReal;
    }

    public BigDecimal getGamePayInAmountReleasedBonus() {
        return gamePayInAmountReleasedBonus;
    }

    public void setGamePayInAmountReleasedBonus(BigDecimal gamePayInAmountReleasedBonus) {
        this.gamePayInAmountReleasedBonus = gamePayInAmountReleasedBonus;
    }

    public BigDecimal getGamePayInAmountPlayableBonus() {
        return gamePayInAmountPlayableBonus;
    }

    public void setGamePayInAmountPlayableBonus(BigDecimal gamePayInAmountPlayableBonus) {
        this.gamePayInAmountPlayableBonus = gamePayInAmountPlayableBonus;
    }

    public BigDecimal getGamePayOutAmountReal() {
        return gamePayOutAmountReal;
    }

    public void setGamePayOutAmountReal(BigDecimal gamePayOutAmountReal) {
        this.gamePayOutAmountReal = gamePayOutAmountReal;
    }

    public BigDecimal getGamePayOutAmountReleasedBonus() {
        return gamePayOutAmountReleasedBonus;
    }

    public void setGamePayOutAmountReleasedBonus(BigDecimal gamePayOutAmountReleasedBonus) {
        this.gamePayOutAmountReleasedBonus = gamePayOutAmountReleasedBonus;
    }

    public BigDecimal getGamePayOutAmountPlayableBonus() {
        return gamePayOutAmountPlayableBonus;
    }

    public void setGamePayOutAmountPlayableBonus(BigDecimal gamePayOutAmountPlayableBonus) {
        this.gamePayOutAmountPlayableBonus = gamePayOutAmountPlayableBonus;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public BigDecimal getSlotPayInAmountReal() {
        return slotPayInAmountReal;
    }

    public void setSlotPayInAmountReal(BigDecimal slotPayInAmountReal) {
        this.slotPayInAmountReal = slotPayInAmountReal;
    }

    public BigDecimal getSlotPayInAmountReleasedBonus() {
        return slotPayInAmountReleasedBonus;
    }

    public void setSlotPayInAmountReleasedBonus(BigDecimal slotPayInAmountReleasedBonus) {
        this.slotPayInAmountReleasedBonus = slotPayInAmountReleasedBonus;
    }

    public BigDecimal getSlotPayInAmountPlayableBonus() {
        return slotPayInAmountPlayableBonus;
    }

    public void setSlotPayInAmountPlayableBonus(BigDecimal slotPayInAmountPlayableBonus) {
        this.slotPayInAmountPlayableBonus = slotPayInAmountPlayableBonus;
    }

    public BigDecimal getSlotPayOutAmountReal() {
        return slotPayOutAmountReal;
    }

    public void setSlotPayOutAmountReal(BigDecimal slotPayOutAmountReal) {
        this.slotPayOutAmountReal = slotPayOutAmountReal;
    }

    public BigDecimal getSlotPayOutAmountReleasedBonus() {
        return slotPayOutAmountReleasedBonus;
    }

    public void setSlotPayOutAmountReleasedBonus(BigDecimal slotPayOutAmountReleasedBonus) {
        this.slotPayOutAmountReleasedBonus = slotPayOutAmountReleasedBonus;
    }

    public BigDecimal getSlotPayOutAmountPlayableBonus() {
        return slotPayOutAmountPlayableBonus;
    }

    public void setSlotPayOutAmountPlayableBonus(BigDecimal slotPayOutAmountPlayableBonus) {
        this.slotPayOutAmountPlayableBonus = slotPayOutAmountPlayableBonus;
    }

    public BigDecimal getSlotJackpotAccumulatedAmountReal() {
        return slotJackpotAccumulatedAmountReal;
    }

    public void setSlotJackpotAccumulatedAmountReal(BigDecimal slotJackpotAccumulatedAmountReal) {
        this.slotJackpotAccumulatedAmountReal = slotJackpotAccumulatedAmountReal;
    }

    public BigDecimal getSlotJackpotAccumulatedPayInAmountReleasedBonus() {
        return slotJackpotAccumulatedPayInAmountReleasedBonus;
    }

    public void setSlotJackpotAccumulatedPayInAmountReleasedBonus(BigDecimal slotJackpotAccumulatedPayInAmountReleasedBonus) {
        this.slotJackpotAccumulatedPayInAmountReleasedBonus = slotJackpotAccumulatedPayInAmountReleasedBonus;
    }

    public BigDecimal getSlotJackpotAccumulatedPayInAmountPlayableBonus() {
        return slotJackpotAccumulatedPayInAmountPlayableBonus;
    }

    public void setSlotJackpotAccumulatedPayInAmountPlayableBonus(BigDecimal slotJackpotAccumulatedPayInAmountPlayableBonus) {
        this.slotJackpotAccumulatedPayInAmountPlayableBonus = slotJackpotAccumulatedPayInAmountPlayableBonus;
    }

    public BigDecimal getSlotJackpotPayOutAmountReal() {
        return slotJackpotPayOutAmountReal;
    }

    public void setSlotJackpotPayOutAmountReal(BigDecimal slotJackpotPayOutAmountReal) {
        this.slotJackpotPayOutAmountReal = slotJackpotPayOutAmountReal;
    }

    public BigDecimal getSlotJackpotPayOutAmountReleasedBonus() {
        return slotJackpotPayOutAmountReleasedBonus;
    }

    public void setSlotJackpotPayOutAmountReleasedBonus(BigDecimal slotJackpotPayOutAmountReleasedBonus) {
        this.slotJackpotPayOutAmountReleasedBonus = slotJackpotPayOutAmountReleasedBonus;
    }

    public BigDecimal getSlotJackpotPayOutAmountPlayableBonus() {
        return slotJackpotPayOutAmountPlayableBonus;
    }

    public void setSlotJackpotPayOutAmountPlayableBonus(BigDecimal slotJackpotPayOutAmountPlayableBonus) {
        this.slotJackpotPayOutAmountPlayableBonus = slotJackpotPayOutAmountPlayableBonus;
    }

    public BigDecimal getTablePayInAmountReal() {
        return tablePayInAmountReal;
    }

    public void setTablePayInAmountReal(BigDecimal tablePayInAmountReal) {
        this.tablePayInAmountReal = tablePayInAmountReal;
    }

    public BigDecimal getTablePayInAmountReleasedBonus() {
        return tablePayInAmountReleasedBonus;
    }

    public void setTablePayInAmountReleasedBonus(BigDecimal tablePayInAmountReleasedBonus) {
        this.tablePayInAmountReleasedBonus = tablePayInAmountReleasedBonus;
    }

    public BigDecimal getTablePayInAmountPlayableBonus() {
        return tablePayInAmountPlayableBonus;
    }

    public void setTablePayInAmountPlayableBonus(BigDecimal tablePayInAmountPlayableBonus) {
        this.tablePayInAmountPlayableBonus = tablePayInAmountPlayableBonus;
    }

    public BigDecimal getTablePayOutAmountReal() {
        return tablePayOutAmountReal;
    }

    public void setTablePayOutAmountReal(BigDecimal tablePayOutAmountReal) {
        this.tablePayOutAmountReal = tablePayOutAmountReal;
    }

    public BigDecimal getTablePayOutAmountReleasedBonus() {
        return tablePayOutAmountReleasedBonus;
    }

    public void setTablePayOutAmountReleasedBonus(BigDecimal tablePayOutAmountReleasedBonus) {
        this.tablePayOutAmountReleasedBonus = tablePayOutAmountReleasedBonus;
    }

    public BigDecimal getTablePayOutAmountPlayableBonus() {
        return tablePayOutAmountPlayableBonus;
    }

    public void setTablePayOutAmountPlayableBonus(BigDecimal tablePayOutAmountPlayableBonus) {
        this.tablePayOutAmountPlayableBonus = tablePayOutAmountPlayableBonus;
    }

    public BigDecimal getTableJackpotAccumulatedAmountReal() {
        return tableJackpotAccumulatedAmountReal;
    }

    public void setTableJackpotAccumulatedAmountReal(BigDecimal tableJackpotAccumulatedAmountReal) {
        this.tableJackpotAccumulatedAmountReal = tableJackpotAccumulatedAmountReal;
    }

    public BigDecimal getTableJackpotAccumulatedPayInAmountReleasedBonus() {
        return tableJackpotAccumulatedPayInAmountReleasedBonus;
    }

    public void setTableJackpotAccumulatedPayInAmountReleasedBonus(BigDecimal tableJackpotAccumulatedPayInAmountReleasedBonus) {
        this.tableJackpotAccumulatedPayInAmountReleasedBonus = tableJackpotAccumulatedPayInAmountReleasedBonus;
    }

    public BigDecimal getTableJackpotAccumulatedPayInAmountPlayableBonus() {
        return tableJackpotAccumulatedPayInAmountPlayableBonus;
    }

    public void setTableJackpotAccumulatedPayInAmountPlayableBonus(BigDecimal tableJackpotAccumulatedPayInAmountPlayableBonus) {
        this.tableJackpotAccumulatedPayInAmountPlayableBonus = tableJackpotAccumulatedPayInAmountPlayableBonus;
    }

    public BigDecimal getTableJackpotPayOutAmountReal() {
        return tableJackpotPayOutAmountReal;
    }

    public void setTableJackpotPayOutAmountReal(BigDecimal tableJackpotPayOutAmountReal) {
        this.tableJackpotPayOutAmountReal = tableJackpotPayOutAmountReal;
    }

    public BigDecimal getTableJackpotPayOutAmountReleasedBonus() {
        return tableJackpotPayOutAmountReleasedBonus;
    }

    public void setTableJackpotPayOutAmountReleasedBonus(BigDecimal tableJackpotPayOutAmountReleasedBonus) {
        this.tableJackpotPayOutAmountReleasedBonus = tableJackpotPayOutAmountReleasedBonus;
    }

    public BigDecimal getTableJackpotPayOutAmountPlayableBonus() {
        return tableJackpotPayOutAmountPlayableBonus;
    }

    public void setTableJackpotPayOutAmountPlayableBonus(BigDecimal tableJackpotPayOutAmountPlayableBonus) {
        this.tableJackpotPayOutAmountPlayableBonus = tableJackpotPayOutAmountPlayableBonus;
    }
}
