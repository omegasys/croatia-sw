package com.omega.croatiasw.vm;

import java.sql.Timestamp;

/**
 * @author Wenbo at Mar 15th, 2019
 */
public class TransactionHeaderVM {
    private Long transactionID;
    private String transactionType;
    private String GUID;
    private String personalID;
    private Timestamp dateTime;
    private Long relatedTransactionID;
    private String relatedTransactionGUID;
    private Timestamp relatedTransactionDateTime;
    private Long SQLTransactionID;

    public Long getTransactionID() {
        return transactionID;
    }

    public void setTransactionID(Long transactionID) {
        this.transactionID = transactionID;
    }

    public String getTransactionType() {
        return transactionType;
    }

    public void setTransactionType(String transactionType) {
        this.transactionType = transactionType;
    }

    public String getGUID() {
        return GUID;
    }

    public void setGUID(String GUID) {
        this.GUID = GUID;
    }

    public String getPersonalID() {
        return personalID;
    }

    public void setPersonalID(String personalID) {
        this.personalID = personalID;
    }

    public Timestamp getDateTime() {
        return dateTime;
    }

    public void setDateTime(Timestamp dateTime) {
        this.dateTime = dateTime;
    }

    public Long getRelatedTransactionID() {
        return relatedTransactionID;
    }

    public void setRelatedTransactionID(Long relatedTransactionID) {
        this.relatedTransactionID = relatedTransactionID;
    }

    public String getRelatedTransactionGUID() {
        return relatedTransactionGUID;
    }

    public void setRelatedTransactionGUID(String relatedTransactionGUID) {
        this.relatedTransactionGUID = relatedTransactionGUID;
    }

    public Timestamp getRelatedTransactionDateTime() {
        return relatedTransactionDateTime;
    }

    public void setRelatedTransactionDateTime(Timestamp relatedTransactionDateTime) {
        this.relatedTransactionDateTime = relatedTransactionDateTime;
    }

    public Long getSQLTransactionID() {
        return SQLTransactionID;
    }

    public void setSQLTransactionID(Long SQLTransactionID) {
        this.SQLTransactionID = SQLTransactionID;
    }
}
