package com.omega.croatiasw.repository;

import com.omega.croatiasw.entity.JobLockStatus;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by volcanohong on AD 2017-03-15.
 */
@Repository
public interface JobLockStatusRepository extends JpaRepository<JobLockStatus, String> {
}

