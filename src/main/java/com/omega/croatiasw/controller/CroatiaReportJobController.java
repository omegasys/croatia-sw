package com.omega.croatiasw.controller;

import com.omega.croatiasw.job.CroatiaReportJob;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author Wenbo at Mar 15th, 2019
 */
@RestController
public class CroatiaReportJobController {
    private final Logger log = LoggerFactory.getLogger(getClass().getSimpleName());
    private CroatiaReportJob croatiaReportJob;

    public CroatiaReportJobController(CroatiaReportJob croatiaReportJob) {
        this.croatiaReportJob = croatiaReportJob;
    }

    @RequestMapping(value = "/send-report-job",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> runSendReportJob() {
        log.info("REST API :: run sendReportJob");
        croatiaReportJob.sendReports();
        return ResponseEntity.ok().body("{\"croatiaSentReport\":\"Done\"}");
    }

    @RequestMapping(value = "/daily-report-job",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> runGenerateDailyReportJob() {
        log.info("REST API :: run generateDailyReportJob");
        croatiaReportJob.generateDailyReports();
        return ResponseEntity.ok().body("{\"croatiaDailyReport\":\"Done\"}");
    }

    @RequestMapping(value = "/monthly-report-job",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> runGenerateMonthlyReportJob() {
        log.info("REST API :: run generateMonthlyReportJob");
        croatiaReportJob.generateMonthlyReports();
        return ResponseEntity.ok().body("{\"croatiaMonthlyReport\":\"Done\"}");
    }
}
