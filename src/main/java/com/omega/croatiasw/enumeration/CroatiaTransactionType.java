package com.omega.croatiasw.enumeration;

/**
 * @author Wenbo at Nov 25th, 2020
 */
public enum CroatiaTransactionType {
    Casino,
    Game,
    Jackpot,
    SlotGameReport,
    TableGameReport,
    IContoReport,
    MonthlyReport,
    Cancellation
}
