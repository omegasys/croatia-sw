package com.omega.croatiasw.enumeration;

/**
 * @author Wenbo at Mar 18th, 2019
 */
public enum CroatiaLogStatus {
    /**
     * status of report sent to regulator
     */
    PENDING("Pending", "Pending"),
    SENDING("Sending", "Sending"),
    SENT("Sent", "Sent"),
    ERROR("Error", "Error");

    private final String code;
    private final String description;

    CroatiaLogStatus(String code, String description) {
        this.code = code;
        this.description = description;
    }

    public String getCode() {
        return code;
    }

    public String getDescription() {
        return description;
    }

    public static CroatiaLogStatus fromCode(String code) {
        for (CroatiaLogStatus element : CroatiaLogStatus.values()) {
            if (element.getCode().equals(code)) {
                return element;
            }
        }
        return null;
    }

    @Override
    public String toString() {
        return description;
    }
}
