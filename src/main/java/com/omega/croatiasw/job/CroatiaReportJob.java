package com.omega.croatiasw.job;

import com.omega.croatiasw.service.CroatiaReportService;
import com.omega.croatiasw.service.JobLockService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

/**
 * @author Wenbo Xue, Mar 15th 2019
 */
@Component("croatiaReportJob")
public class CroatiaReportJob {

    private final Logger log = LoggerFactory.getLogger(getClass().getSimpleName());
    private final String jobName = "Croatia report job";
    private CroatiaReportService croatiaReportService;
    private JobLockService jobLockService;

    public CroatiaReportJob(CroatiaReportService croatiaReportService, JobLockService jobLockService) {
        this.croatiaReportService = croatiaReportService;
        this.jobLockService = jobLockService;
    }

    /**
     * job to send out reports from report queue, runs every 5 minutes
     */
    @Scheduled(cron = "0 */5 * * * *")
    public void sendReports() {

        jobLockService.tryResetAfterLockedOneHour(jobName, "System Reset");
        //lock job
        if (!jobLockService.tryLock(jobName, "Job Begin Run")) {
            log.info("Job Locked: {} is running ...", jobName);
            return;
        }
        start();
    }

    /**
     * job to generate daily reports, runs at 00:01am everyday to generate previous day's report
     */
    @Scheduled(cron = "0 30 0 * * *")
    public void generateDailyReports() {
        jobLockService.tryResetAfterLockedOneHour(jobName, "System Reset");
        //lock job
        if (!jobLockService.tryLock(jobName, "Job Begin Run")) {
            log.info("Job Locked: {} is running ...", jobName);
            return;
        }
        generateDaily();
    }

    /**
     * job to generate monthly reports, runs at 00:30am every 1st day of month to generate previous month's report
     */
    @Scheduled(cron = "0 30 0 1 * ?")
    public void generateMonthlyReports() {
        jobLockService.tryResetAfterLockedOneHour(jobName, "System Reset");
        //lock job
        if (!jobLockService.tryLock(jobName, "Job Begin Run")) {
            log.info("Job Locked: {} is running ...", jobName);
            return;
        }
        generateMonthly();
    }

    private void start() {
        log.info("{} is running ...", jobName);
        final long startTime = System.currentTimeMillis();
        Boolean queueIsNoEmpty;
        do {
            queueIsNoEmpty = croatiaReportService.sendReport();
        } while (queueIsNoEmpty);

        final long endTime = System.currentTimeMillis();
        log.info("Total execution time: " + (endTime - startTime));
        //unlock job
        jobLockService.tryUnlock(jobName, "Job Run Success");

    }

    private void generateDaily() {
        log.info("{} is running ...", jobName);
        final long startTime = System.currentTimeMillis();

        croatiaReportService.generateDailyReports();

        final long endTime = System.currentTimeMillis();
        log.info("Total execution time: " + (endTime - startTime));
        //unlock job
        jobLockService.tryUnlock(jobName, "Job Run Success");
    }

    private void generateMonthly() {
        log.info("{} is running ...", jobName);
        final long startTime = System.currentTimeMillis();

        croatiaReportService.generateMonthlyReports();

        final long endTime = System.currentTimeMillis();
        log.info("Total execution time: " + (endTime - startTime));
        //unlock job
        jobLockService.tryUnlock(jobName, "Job Run Success");
    }

}
