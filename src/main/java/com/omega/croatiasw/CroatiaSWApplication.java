package com.omega.croatiasw;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@EnableScheduling
public class CroatiaSWApplication {

	public static void main(String[] args) {
		SpringApplication.run(CroatiaSWApplication.class, args);
	}

}
