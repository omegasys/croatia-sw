package com.omega.croatiasw.util;

import java.sql.Timestamp;
import java.time.LocalDate;
import java.util.Calendar;

import static java.lang.Math.abs;

public class TimestampUtil {

    public static Timestamp yymmdd(int year, int month, int day) {
        final Calendar cal = Calendar.getInstance();
        cal.set(year, month - 1, day);
        cal.set(Calendar.HOUR_OF_DAY, 0);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.SECOND, 0);
        cal.set(Calendar.MILLISECOND, 0);

        return new Timestamp(cal.getTimeInMillis());
    }

    public static Timestamp nextWeek() {
        final Calendar cal = Calendar.getInstance();
        cal.add(Calendar.DATE, +7);
        cal.set(Calendar.HOUR_OF_DAY, 0);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.SECOND, 0);
        cal.set(Calendar.MILLISECOND, 0);

        return new Timestamp(cal.getTimeInMillis());
    }

    public static Timestamp yesterday() {
        final Calendar cal = Calendar.getInstance();
        cal.add(Calendar.DATE, -1);
        cal.set(Calendar.HOUR_OF_DAY, 0);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.SECOND, 0);
        cal.set(Calendar.MILLISECOND, 0);

        return new Timestamp(cal.getTimeInMillis());
    }

    public static Timestamp lastMonth() {
        final Calendar cal = Calendar.getInstance();
        cal.add(Calendar.MONTH, -1);
        cal.set(Calendar.HOUR_OF_DAY, 0);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.SECOND, 0);
        cal.set(Calendar.MILLISECOND, 0);

        return new Timestamp(cal.getTimeInMillis());
    }

    public static Timestamp tomorrow() {
        final Calendar cal = Calendar.getInstance();
        cal.add(Calendar.DATE, +1);
        cal.set(Calendar.HOUR_OF_DAY, 0);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.SECOND, 0);
        cal.set(Calendar.MILLISECOND, 0);

        return new Timestamp(cal.getTimeInMillis());
    }

    public static Timestamp today() {
        return today(0);
    }

    public static Timestamp today(int offset) {
        final Calendar cal = Calendar.getInstance();
        cal.add(Calendar.DATE, offset);
        cal.set(Calendar.HOUR_OF_DAY, 0);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.SECOND, 0);
        cal.set(Calendar.MILLISECOND, 0);

        return new Timestamp(cal.getTimeInMillis());
    }

    public static int daysBetweenTimestamps(Timestamp startTime, Timestamp endTime) {
        int betweenDay = (int)(endTime.getTime() - startTime.getTime()) / (24 * 60 * 60 * 1000);
        return betweenDay + 1;
    }

    public static Timestamp now() {
        final Calendar cal = Calendar.getInstance();

        return new Timestamp(cal.getTimeInMillis());
    }

    public static Timestamp getBeginOfTodayInTimestamp() {
        return Timestamp.valueOf(LocalDate.now().atStartOfDay());
    }

    public static Timestamp getBeginOfTodayInTimestamp(LocalDate localDate) {
        return Timestamp.valueOf(localDate.atStartOfDay());
    }

    public static Timestamp lastHour() {
        final Calendar cal = Calendar.getInstance();
        cal.add(Calendar.HOUR, -1);
        return new Timestamp(cal.getTimeInMillis());
    }

    public static Timestamp nextYear(int noOfYearFromNow) {
        final Calendar cal = Calendar.getInstance();
        cal.add(Calendar.YEAR, noOfYearFromNow);
        return new Timestamp(cal.getTimeInMillis());
    }

    public static boolean sameDay(Timestamp startTime, Timestamp endTime) {
        int betweenDay = (int)(abs(endTime.getTime() - startTime.getTime())) / (24 * 60 * 60 * 1000);
        return betweenDay == 0;
    }

}
