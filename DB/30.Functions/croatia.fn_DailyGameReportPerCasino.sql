set quoted_identifier, ansi_nulls on
if object_id('croatia.fn_DailyGameReportPerCasino') is null
	exec('create function croatia.fn_DailyGameReportPerCasino() returns table as return(select 1 as one)')
go
alter function croatia.fn_DailyGameReportPerCasino
(
	@Date date,
	@ProductID int,
	@IsSlotGame bit
)
returns table
as
return(
	select  
			p.CODE as Casino,
			gi.ID as GameCode,
			isnull(sum(case when at.TranType = 'GAME_BET' then at.AmountReal else 0 end), 0) PayInAmountReal,
			isnull(sum(case when at.TranType = 'GAME_BET' then at.AmountReleasedBonus else 0 end),0) PayInAmountReleasedBonus,
			isnull(sum(case when at.TranType = 'GAME_BET' then at.AmountPlayableBonus else 0 end), 0) PayInAmountPlayableBonus,
			isnull(sum(case when at.TranType = 'GAME_WIN' /*and j.ACCOUNT_TRAN_ID is null*/ then at.AmountReal else 0 end), 0) PayOutAmountReal,
			isnull(sum(case when at.TranType = 'GAME_WIN' /*and j.ACCOUNT_TRAN_ID is null*/ then at.AmountReleasedBonus else 0 end), 0) PayOutAmountReleasedBonus,
			isnull(sum(case when at.TranType = 'GAME_WIN' /*and j.ACCOUNT_TRAN_ID is null*/ then at.AmountPlayableBonus else 0 end), 0) PayOutAmountPlayableBonus,
			0 JackpotAccumulatedAmountReal,
			0 JackpotAccumulatedAmountReleasedBonus,
			0 JackpotAccumulatedAmountPlayableBonus,
			0 JackpotPayoutAccumulatedAmountReal,
			0 JackpotPayoutAccumulatedAmountReleasedBonus,
			0 JackpotPayoutAccumulatedAmountPlayableBonus,
			isnull(at.Currency, '') Currency
			--sum(case when at.TranType = 'GAME_WIN' and j.ACCOUNT_TRAN_ID is not null then at.AMOUNT_REAL else 0 end) JackpotPayoutAccumulatedAmountReal,
			--sum(case when at.TranType = 'GAME_WIN' and j.ACCOUNT_TRAN_ID is not null then at.AMOUNT_RELEASED_BONUS else 0 end) JackpotPayoutAccumulatedAmountReleasedBonus,
			--sum(case when at.TranType = 'GAME_WIN' and j.ACCOUNT_TRAN_ID is not null then at.AMOUNT_PLAYABLE_BONUS else 0 end) JackpotPayoutAccumulatedAmountPlayableBonus
	from admin_all.PLATFORM p
		inner join admin_all.GAME_INFO gi on p.ID = gi.PLATFORM_ID
		inner join admin_all.GAME_CATEGORY gc on gc.id = gi.GAME_CATEGORY_ID
		left outer join admin_all.AccountTranHourlyAggregate at on gi.PLATFORM_ID = at.ProductID 
															and gi.GAME_ID = at.GameID 
															and at.Datetime >= cast(@Date as datetime)
															and at.Datetime < cast(dateadd(day, 1,  @Date) as datetime)
															and at.TranType in ('GAME_WIN', 'GAME_BET')
															and at.AggregateType = 0
	                                                        and at.Currency = 'HRK'
	where gc.IS_SLOTS = case when @IsSlotGame = 1  then 1 else gc.IS_SLOTS end
		and gc.IS_TABLEGAME = case when @IsSlotGame = 0 then 1 else gc.IS_TABLEGAME end
		and p.id = @ProductID
	group by p.CODE, gi.ID, at.Currency
	)

