set quoted_identifier, ansi_nulls on
if object_id('croatia.fn_GetUnprocessedDate') is null
	exec('create function croatia.fn_GetUnprocessedDate()  returns int as begin return 1 end')
go
if object_id('croatia.fn_GetUnprocessedDate') is not null
	drop function croatia.fn_GetUnprocessedDate
--go
--alter fuNction croatia.fn_GetUnprocessedDate
--(
--	@RelatedTransactionID bigint = null,
--	@IsDay bit
--)
--returns date
--as
--begin
--	declare @Date as date
--	if @RelatedTransactionID is not null
--	begin
--		select top 1 @Date = DateOfReport 
--		from croatia.TransactionDailyIContoHeader 
--		where TransactionID = @RelatedTransactionID 
--		order by DateOfReport desc
--	end
--	else
--	begin
--		if @IsDay = 1
--			select top 1 @Date = dateadd(day, 1, DateOfReport) from croatia.TransactionDailyIContoHeader order by DateOfReport desc
--		else
--			select top 1 @Date = dateadd(month, 1, DateOfReport) from croatia.TransactionDailyIContoHeader order by DateOfReport desc
--		if @Date is null
--		begin
--			select @Date = min(datetime)
--			from admin_all.ACCOUNT_TRAN_ALL
--			if @Date is null
--			begin
--				select @Date = min(datetime)
--				from admin_all.ACCOUNT_TRAN_REALTIME
--			end
--		end
--	end
--	if @IsDay =0
--		select @Date = admin_all.fn_StartOfMonth(@Date)
--	return @Date
--end