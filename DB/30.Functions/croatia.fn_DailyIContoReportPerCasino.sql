set quoted_identifier, ansi_nulls on
if object_id('croatia.fn_DailyIContoReportPerCasino') is null
	exec('create function croatia.fn_DailyIContoReportPerCasino() returns table as return(select 1 as one)')
go
alter function croatia.fn_DailyIContoReportPerCasino
(
	@Date date,
	@ProductID int
)
returns table
as
return (
	
	with x0 as
	(
		select Sum(BALANCE_REAL) BALANCE_REAL, sum(RELEASED_BONUS) RELEASED_BONUS, sum(PLAYABLE_BONUS) PLAYABLE_BONUS 
		from admin_all.DAILY_BALANCE_HISTORY
		where DATETIME = cast(@Date as datetime)
	)
	select  
			Casino = p.CODE,
			OpenBalanceReal = isnull(x0.BALANCE_REAL, 0), 
			OpenBalanceReleasedBonus = isnull(x0.RELEASED_BONUS,0), 
			OpenBalancePlayableBonus = isnull(x0.PLAYABLE_BONUS,0), 
			PayInAmountReal = isnull(sum(case when at.TranType in('GAME_BET', 'TRANSF_IN') then at.AmountReal else 0 end), 0), 
			PayInAmountReleasedBonus = isnull(sum(case when at.TranType in('GAME_BET', 'TRANSF_IN') then at.AmountReleasedBonus else 0 end), 0), 
			PayInAmountPlayableBonus = isnull(sum(case when at.TranType in('GAME_BET', 'TRANSF_IN') then at.AmountReleasedBonus else 0 end), 0), 
			PayOutAmountReal = isnull(sum(case when at.TranType in('GAME_BET', 'TRANSF_OUT') then at.AmountReal else 0 end), 0), 
			PayOutAmountReleasedBonus = isnull(sum(case when at.TranType in('GAME_BET', 'TRANSF_OUT') then at.AmountReleasedBonus else 0 end), 0), 
			PayOutAmountPlayableBonus = isnull(sum(case when at.TranType in('GAME_BET', 'TRANSF_OUT') then at.AmountPlayableBonus else 0 end), 0), 
			GamePayInAmountReal = isnull(sum(case when at.TranType = 'GAME_BET' then at.AmountReal else 0 end), 0), 
			GamePayInAmountReleasedBonus = isnull(sum(case when at.TranType = 'GAME_BET' then at.AmountReleasedBonus else 0 end), 0), 
			GamePayInAmountPlayableBonus = isnull(sum(case when at.TranType = 'GAME_BET' then at.AmountPlayableBonus else 0 end), 0), 
			GamePayOutAmountReal = isnull(sum(case when at.TranType = 'GAME_WIN' then at.AmountReal else 0 end), 0), 
			GamePayOutAmountReleasedBonus = isnull(sum(case when at.TranType = 'GAME_WIN' then at.AmountReleasedBonus else 0 end), 0), 
			GamePayOutAmountPlayableBonus = isnull(sum(case when at.TranType = 'GAME_WIN' then at.AmountPlayableBonus else 0 end), 0),
			isnull(at.Currency, '') Currency
	from admin_all.PLATFORM p 
		left outer join admin_all.AccountTranHourlyAggregate at on p.ID = at.ProductID 
																and at.Datetime >= cast(@Date as datetime)
																and at.Datetime < cast(dateadd(day, 1,  @Date) as datetime)
																and at.TranType in ('GAME_WIN', 'GAME_BET', 'TRANSF_IN', 'TRANSF_OUT')
																and at.AggregateType = 0 and at.Currency = 'HRK'
		cross apply x0
	where p.id = @ProductID
	group by p.CODE, x0.BALANCE_REAL, x0.PLAYABLE_BONUS, x0.RELEASED_BONUS, at.Currency
	)

