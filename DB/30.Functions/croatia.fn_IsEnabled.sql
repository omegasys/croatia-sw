set ansi_nulls , quoted_identifier on
go
if object_id('croatia.fn_IsEnabled') is null
	exec('create function croatia.fn_IsEnabled() returns bit as begin return 1 end')

go
alter function croatia.fn_IsEnabled()
returns bit
as
begin
	return isnull(cast(admin_all.fn_GetRegistry('Croatia.Regulation.Enabled') as int), 0) 
end

