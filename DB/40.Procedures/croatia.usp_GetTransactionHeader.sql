set ansi_nulls , quoted_identifier on
go
if object_id('croatia.usp_GetTransactionHeader') is null
	exec('create procedure croatia.usp_GetTransactionHeader as --')
	go
alter procedure croatia.usp_GetTransactionHeader
as
begin
	set xact_abort, nocount on
	if @@trancount = 0
	begin
		raiserror('croatia.usp_GetTransactionHeader and croatia.usp_GetTransactionDetails must be running within the same transaction', 16, 1)
		return
	end
	exec sp_getapplock 'croatia.usp_GetTransactionHeader','Exclusive', 'Transaction', -1
	declare @TransactionID bigint = null, @TransactionType varchar(30),@GUID uniqueidentifier,  @PersonalID varchar(11), @DateTime datetime,
			@RelatedTransactionID bigint, @RelatedTransactionGUID uniqueidentifier, @RelatedTransactionDateTime datetime
	select top 1 @TransactionID = TransactionID
	from croatia.TransactionHeader
	where Status not in ('Sent')
	order by TransactionID asc
	if @@rowcount = 0
		goto ___Exit___
	exec croatia.usp_WriteLog @TransactionID, 'Sending', @DateTime output, null
	select @GUID = GUID, @PersonalID = admin_all.fn_GetRegistry('Croatia.Regulation.PersonalID'), @RelatedTransactionID = RelatedTransactionID, @TransactionType = TransactionType
	from croatia.TransactionHeader with(updlock)
	where TransactionID = @TransactionID
	if @RelatedTransactionID is not null
	begin
		select @RelatedTransactionGUID = GUID, @RelatedTransactionDateTime = SendingDate
		from croatia.TransactionHeader
		where TransactionID = @RelatedTransactionID
	end
	
___Exit___:
	if @TransactionID is not null
	begin
		select	@TransactionID TransactionID, @TransactionType TransactionType, @GUID GUID,  @PersonalID PersonalID, @DateTime DateTime,
				@RelatedTransactionID RelatedTransactionID, @RelatedTransactionGUID RelatedTransactionGUID, @RelatedTransactionDateTime RelatedTransactionDateTime,
				current_transaction_id() SQLTransactionID
		return
	end
	select top 0	@TransactionID TransactionID, @TransactionType TransactionType, @GUID GUID,  @PersonalID PersonalID, @DateTime DateTime,
					@RelatedTransactionID RelatedTransactionID, @RelatedTransactionGUID RelatedTransactionGUID, @RelatedTransactionDateTime RelatedTransactionDateTime,
					current_transaction_id() SQLTransactionID

end