set quoted_identifier, ansi_nulls on
if object_id('croatia.usp_GetTransactionSummaryByGameCategory') is null
	exec('create procedure croatia.usp_GetTransactionSummaryByGameCategory as ')
go
alter procedure croatia.usp_GetTransactionSummaryByGameCategory
(
	@DateFrom date,
	@DateTo date,
	@ProductID varchar(8000)
)
as
begin
	set nocount on
	select  
			gc.IS_SLOTS,
			gc.IS_TABLEGAME,
			sum(case when at.Tran_Type = 'GAME_BET' then at.AMOUNT_REAL else 0 end) PayInAmountReal,
			sum(case when at.Tran_Type = 'GAME_BET' then at.AMOUNT_RELEASED_BONUS else 0 end) PayInAmountReleasedBonus,
			sum(case when at.Tran_Type = 'GAME_BET' then at.AMOUNT_PLAYABLE_BONUS else 0 end) PayInAmountPlayableBonus,
			sum(case when at.Tran_Type = 'GAME_WIN' /*and j.ACCOUNT_TRAN_ID is null*/ then at.AMOUNT_REAL else 0 end) PayOutAmountReal,
			sum(case when at.Tran_Type = 'GAME_WIN' /*and j.ACCOUNT_TRAN_ID is null*/ then at.AMOUNT_RELEASED_BONUS else 0 end) PayOutAmountReleasedBonus,
			sum(case when at.Tran_Type = 'GAME_WIN' /*and j.ACCOUNT_TRAN_ID is null*/ then at.AMOUNT_PLAYABLE_BONUS else 0 end) PayOutAmountPlayableBonus,
			sum(case when at.Tran_Type = 'GAME_BET' then at.AMOUNT_RAW_LOYALTY else 0 end) PayInLoyaltyAmount,
			sum(case when at.Tran_Type = 'GAME_WIN' then at.AMOUNT_RAW_LOYALTY else 0 end) PayoutLoyaltyAmount,
			count(*) Count,
			u.Currency
			--sum(case when at.Tran_Type = 'GAME_WIN' and j.ACCOUNT_TRAN_ID is not null then at.AMOUNT_REAL else 0 end) JackpotPayoutAccumulatedAmountReal,
			--sum(case when at.Tran_Type = 'GAME_WIN' and j.ACCOUNT_TRAN_ID is not null then at.AMOUNT_RELEASED_BONUS else 0 end) JackpotPayoutAccumulatedAmountReleasedBonus,
			--sum(case when at.Tran_Type = 'GAME_WIN' and j.ACCOUNT_TRAN_ID is not null then at.AMOUNT_PLAYABLE_BONUS else 0 end) JackpotPayoutAccumulatedAmountPlayableBonus
	from admin_all.ACCOUNT_TRAN at
		inner join admin_all.GAME_INFO gi on gi.PLATFORM_ID = at.PLATFORM_ID and gi.GAME_ID = at.GAME_ID
		inner join admin_all.GAME_CATEGORY gc on gc.id = gi.GAME_CATEGORY_ID
		inner join admin_all.PLATFORM p on p.ID = gi.PLATFORM_ID
		inner join admin_all.ACCOUNT a on a.id = at.ACCOUNT_ID
		inner join external_mpt.USER_CONF u on u.PARTYID = a.PARTYID
		--outer apply(
		--				select top 1 ta.ACCOUNT_TRAN_ID
		--				from admin_all.TRANSACTION_TAG_ACCOUNT_TRAN ta
		--					inner join admin_all.TRANSACTION_TAG tt on tt.ID = ta.TRANSACTION_TAG_ID
		--				where tt.CODE = 'JACKPOT'
		--					and ta.ACCOUNT_TRAN_ID = at.ID
		--			) J
	where at.Datetime >= cast(@DateFrom as datetime)
		and at.Datetime < cast(@DateTo as datetime)
		and at.Tran_Type in ('GAME_WIN', 'GAME_BET')
		and (gc.IS_SLOTS = 1 or  gc.IS_TABLEGAME = 1)
		and gi.PLATFORM_ID in (select cast(Item as int) from admin_all.fc_splitDelimiterString(@ProductID, ','))
	group by u.Currency, gc.IS_SLOTS, gc.IS_TABLEGAME
end

go 
--exec croatia.usp_GetTransactionSummaryByGameCategory '2018-01-01', '2018-01-01', 1