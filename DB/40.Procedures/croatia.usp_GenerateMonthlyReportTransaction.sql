set quoted_identifier, ansi_nulls on
If object_id('croatia.usp_GenerateMonthlyReportTransaction') is null
	exec('create procedure croatia.usp_GenerateMonthlyReportTransaction as ')
go
alter procedure croatia.usp_GenerateMonthlyReportTransaction
(
	@RelatedTransactionID bigint = null
)
as
begin
	set nocount on
	set xact_abort on
	declare @TransactionID bigint, @Date date, @ProductID int
	if @RelatedTransactionID is not null
	begin
		select @Date = DateOfReport, @ProductID = ProductID from croatia.TransactionMonthlyHeader where TransactionID = @RelatedTransactionID
	end
	else
	begin
		select top 1 @Date = dateadd(month, 1, DateOfReport) from croatia.TransactionMonthlyHeader order by DateOfReport desc
		if @Date is null
		begin
			select @Date = min(datetime)
			from admin_all.ACCOUNT_TRAN_ALL
			if @Date is null
			begin
				select @Date = min(datetime)
				from admin_all.ACCOUNT_TRAN_REALTIME
			end
		end
	end
	if @Date is null
		return;
	select @Date = admin_all.fn_StartOfMonth(@Date)
	while @Date < cast(getdate() as date)
	begin
		begin transaction
		declare c cursor local static for
			select ID 
			from admin_all.PLATFORM p 
			where @RelatedTransactionID  is null 
				or		@RelatedTransactionID  is not null 
					and ID = isnull(@ProductID, ID) 
			order by 1
		open c
		fetch next from c into @ProductID
		while @@fetch_status = 0
		begin
			save transaction a 
			exec croatia.usp_CreateTransactionHeader @TransactionID output, 'MonthlyReport', @RelatedTransactionID
			insert into croatia.TransactionMonthlyHeader(TransactionID, DateOfReport,ProductID)
				values(@TransactionID, @Date, @ProductID)
			insert into croatia.TransactionMonthlyDetail(TransactionID, Casino, SlotPayInAmountReal, SlotPayInAmountReleasedBonus, SlotPayInAmountPlayableBonus, SlotPayOutAmountReal, SlotPayOutAmountReleasedBonus, SlotPayOutAmountPlayableBonus, SlotJackpotAccumulatedAmountReal, SlotJackpotAccumulatedPayInAmountReleasedBonus, SlotJackpotAccumulatedPayInAmountPlayableBonus, SlotJackpotPayOutAmountReal, SlotJackpotPayOutAmountReleasedBonus, SlotJackpotPayOutAmountPlayableBonus, TablePayInAmountReal, TablePayInAmountReleasedBonus, TablePayInAmountPlayableBonus, TablePayOutAmountReal, TablePayOutAmountReleasedBonus, TablePayOutAmountPlayableBonus, TableJackpotAccumulatedAmountReal, TableJackpotAccumulatedPayInAmountReleasedBonus, TableJackpotAccumulatedPayInAmountPlayableBonus, TableJackpotPayOutAmountReal, TableJackpotPayOutAmountReleasedBonus, TableJackpotPayOutAmountPlayableBonus, Currency)
				exec croatia.usp_GenerateMonthlyReport @Date, @ProductID, @TransactionID
			if @@rowcount = 0
				rollback transaction a;
			if @RelatedTransactionID is not null
				break;
			fetch next from c into @ProductID
		end
		close c
		deallocate c
		commit
		if @RelatedTransactionID is not null
			break;
		select @Date = dateadd(month, 1, @Date)
	end
end

go
--exec croatia.usp_GenerateMonthlyReportTransaction