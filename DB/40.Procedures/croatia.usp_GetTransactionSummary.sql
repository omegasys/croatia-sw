set quoted_identifier, ansi_nulls on
if object_id('croatia.usp_GetTransactionSummary') is null
	exec('create procedure croatia.usp_GetTransactionSummary as ')
go
alter procedure croatia.usp_GetTransactionSummary
(
	@DateFrom date,
	@DateTo date
)
as
begin
	set nocount on
	select  
			
			sum(case when at.TranType = 'GAME_BET' then at.AmountReal else 0 end) PayInAmountReal,
			sum(case when at.TranType = 'GAME_BET' then at.AmountReleasedBonus else 0 end) PayInAmountReleasedBonus,
			sum(case when at.TranType = 'GAME_BET' then at.AmountPlayableBonus else 0 end) PayInAmountPlayableBonus,
			sum(case when at.TranType = 'GAME_WIN' /*and j.ACCOUNT_TRAN_ID is null*/ then at.AmountReal else 0 end) PayOutAmountReal,
			sum(case when at.TranType = 'GAME_WIN' /*and j.ACCOUNT_TRAN_ID is null*/ then at.AmountReleasedBonus else 0 end) PayOutAmountReleasedBonus,
			sum(case when at.TranType = 'GAME_WIN' /*and j.ACCOUNT_TRAN_ID is null*/ then at.AmountPlayableBonus else 0 end) PayOutAmountPlayableBonus,
			count(*) Count,
			at.Currency
			--sum(case when at.TranType = 'GAME_WIN' and j.ACCOUNT_TRAN_ID is not null then at.AMOUNT_REAL else 0 end) JackpotPayoutAccumulatedAmountReal,
			--sum(case when at.TranType = 'GAME_WIN' and j.ACCOUNT_TRAN_ID is not null then at.AMOUNT_RELEASED_BONUS else 0 end) JackpotPayoutAccumulatedAmountReleasedBonus,
			--sum(case when at.TranType = 'GAME_WIN' and j.ACCOUNT_TRAN_ID is not null then at.AMOUNT_PLAYABLE_BONUS else 0 end) JackpotPayoutAccumulatedAmountPlayableBonus
	from admin_all.AccountTranHourlyAggregate at
		inner join admin_all.GAME_INFO gi on gi.PLATFORM_ID = at.ProductID and gi.GAME_ID = at.GameID
		inner join admin_all.GAME_CATEGORY gc on gc.id = gi.GAME_CATEGORY_ID
		inner join admin_all.PLATFORM p on p.ID = gi.PLATFORM_ID
		--outer apply(
		--				select top 1 ta.ACCOUNT_TRAN_ID
		--				from admin_all.TRANSACTION_TAG_ACCOUNT_TRAN ta
		--					inner join admin_all.TRANSACTION_TAG tt on tt.ID = ta.TRANSACTION_TAG_ID
		--				where tt.CODE = 'JACKPOT'
		--					and ta.ACCOUNT_TRAN_ID = at.ID
		--			) J
	where at.Datetime >= cast(@DateFrom as datetime)
		and at.Datetime < cast(@DateTo as datetime)
		and at.TranType in ('GAME_WIN', 'GAME_BET')
		and (gc.IS_SLOTS = 1 or  gc.IS_TABLEGAME = 1)
	group by at.Currency
	order by at.Currency
end
go
--exec croatia.usp_GetTransactionSummary '2018-01-01', '2018-01-01'