set quoted_identifier, ansi_nulls on
if object_id('croatia.usp_GenerateDailyIContoReport') is null
	exec('create procedure croatia.usp_GenerateDailyIContoReport as ')
go
alter procedure croatia.usp_GenerateDailyIContoReport
(
	@Date date
)
as
begin
	set nocount on
	
	select  
			r.*
	from admin_all.PLATFORM p 
		cross apply croatia.fn_DailyIContoReportPerCasino(@Date, p.ID) r
end

