set quoted_identifier, ansi_nulls on
if object_id('croatia.usp_GenerateDailyGameReport') is null
	exec('create procedure croatia.usp_GenerateDailyGameReport as ')
go
alter procedure croatia.usp_GenerateDailyGameReport
(
	@Date date,
	@IsSlotGame bit
)
as
begin
	set nocount on
	select  
			r.*
	from admin_all.PLATFORM p 
		cross apply croatia.fn_DailyGameReportPerCasino(@Date, p.id, @IsSlotGame) r
end

