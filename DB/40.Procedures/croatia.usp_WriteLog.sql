set ansi_nulls , quoted_identifier on
go
if object_id('croatia.usp_WriteLog') is null
	exec('create procedure croatia.usp_WriteLog as --')
	go
alter procedure croatia.usp_WriteLog
(
	@TransactionID bigint,
	@Status varchar(30), -- 'Pending, Sent, Error'
	@Date datetime output,
	@Message nvarchar(max) = null
)
as
begin
	set nocount, xact_abort on
	declare @TransactionLogID bigint
	select @Date = getdate()
	begin transaction
	insert into croatia.TransactionLog(TransactionID, Status, Date, Message)
		values(@TransactionID, @Status, @Date, @Message)
	select @TransactionLogID =  scope_identity()
	update croatia.TransactionHeader
		set SendingDate = case when @Status = 'Sending' then @Date else SendingDate end, 
			Status = @Status, 
			StatusDate = @Date, 
			TransactionLogID = @TransactionID, 
			Retries = case when @Status = 'Sending' then Retries + 1 else Retries end
	where TransactionID = @TransactionID
		
	commit
end
go