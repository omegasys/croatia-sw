set quoted_identifier, ansi_nulls on
if object_id('croatia.usp_GenerateMonthlyReport') is null
	exec('create procedure croatia.usp_GenerateMonthlyReport as ')
go
alter procedure croatia.usp_GenerateMonthlyReport
(
	@Date date,
	@ProductID int,
	@TransactionID bigint = null
)
as
begin
	set nocount on
	select @Date = admin_all.fn_StartOfMonth(@Date)
	select  
			TransactionID = @TransactionID,
			Casino = p.CODE,
			SlotPayInAmountReal = sum(case when at.TranType = 'GAME_BET' and gc.IS_SLOTS = 1 then at.AmountReal else 0 end), 
			SlotPayInAmountReleasedBonus = sum(case when at.TranType = 'GAME_BET' and gc.IS_SLOTS = 1 then at.AmountReleasedBonus else 0 end), 
			SlotPayInAmountPlayableBonus = sum(case when at.TranType = 'GAME_BET' and gc.IS_SLOTS = 1 then at.AmountPlayableBonus else 0 end), 
			SlotPayOutAmountReal = sum(case when at.TranType = 'GAME_WIN' and gc.IS_SLOTS = 1 then at.AmountReal else 0 end), 
			SlotPayOutAmountReleasedBonus = sum(case when at.TranType = 'GAME_WIN' and gc.IS_SLOTS = 1 then at.AmountReleasedBonus else 0 end), 
			SlotPayOutAmountPlayableBonus = sum(case when at.TranType = 'GAME_WIN' and gc.IS_SLOTS = 1 then at.AmountPlayableBonus else 0 end), 
			SlotJackpotAccumulatedAmountReal = 0, 
			SlotJackpotAccumulatedPayInAmountReleasedBonus = 0, 
			SlotJackpotAccumulatedPayInAmountPlayableBonus = 0, 
			SlotJackpotPayOutAmountReal = 0, 
			SlotJackpotPayOutAmountReleasedBonus = 0, 
			SlotJackpotPayOutAmountPlayableBonus = 0, 
			TablePayInAmountReal = sum(case when at.TranType = 'GAME_BET' and gc.IS_TABLEGAME = 1 then at.AmountReal else 0 end), 
			TablePayInAmountReleasedBonus = sum(case when at.TranType = 'GAME_BET' and gc.IS_TABLEGAME = 1 then at.AmountReleasedBonus else 0 end), 
			TablePayInAmountPlayableBonus = sum(case when at.TranType = 'GAME_BET' and gc.IS_TABLEGAME = 1 then at.AmountPlayableBonus else 0 end), 
			TablePayOutAmountReal = sum(case when at.TranType = 'GAME_WIN' and gc.IS_TABLEGAME = 1 then at.AmountReal else 0 end),  
			TablePayOutAmountReleasedBonus = sum(case when at.TranType = 'GAME_WIN' and gc.IS_TABLEGAME = 1 then at.AmountReleasedBonus else 0 end),  
			TablePayOutAmountPlayableBonus = sum(case when at.TranType = 'GAME_WIN' and gc.IS_TABLEGAME = 1 then at.AmountPlayableBonus else 0 end),  
			TableJackpotAccumulatedAmountReal = 0, 
			TableJackpotAccumulatedPayInAmountReleasedBonus = 0, 
			TableJackpotAccumulatedPayInAmountPlayableBonus = 0, 
			TableJackpotPayOutAmountReal = 0, 
			TableJackpotPayOutAmountReleasedBonus = 0, 
			TableJackpotPayOutAmountPlayableBonus = 0, 		
			isnull(at.Currency, '') Currency
	from admin_all.PLATFORM p 
		left join  admin_all.AccountTranHourlyAggregate at on p.ID = at.ProductID and at.Datetime >= cast(@Date as datetime) and at.Datetime < cast(dateadd(month, 1,  @Date) as datetime) and at.TranType in ('GAME_WIN', 'GAME_BET') and at.AggregateType = 0 and at.Currency = 'HRK'
		left join admin_all.GAME_INFO gi on gi.PLATFORM_ID = at.ProductID and gi.GAME_ID = at.GameID
		left join admin_all.GAME_CATEGORY gc on gc.ID = gi.GAME_CATEGORY_ID
	where p.ID = @ProductID
	group by p.CODE, at.Currency
end

