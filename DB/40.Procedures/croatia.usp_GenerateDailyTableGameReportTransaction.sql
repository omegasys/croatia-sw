set quoted_identifier, ansi_nulls on
if object_id('croatia.usp_GenerateDailyTableGameReportTransaction') is null
	exec('create procedure croatia.usp_GenerateDailyTableGameReportTransaction as ')
go
alter procedure croatia.usp_GenerateDailyTableGameReportTransaction
(
	@RelatedTransactionID bigint = null--TableGameReport
)
as
begin
	set nocount on
	set xact_abort on
	declare @TransactionID bigint, @Date date, @ProductID int
	if @RelatedTransactionID is not null
	begin
		select top 1 @Date = DateOfReport, @ProductID = ProductID
		from croatia.TransactionDailyTableGameHeader
		where TransactionID = @RelatedTransactionID 
		order by DateOfReport desc
	end
	else
	begin
		select top 1 @Date = dateadd(day, 1, DateOfReport) from croatia.TransactionDailyTableGameHeader order by DateOfReport desc
		if @Date is null
		begin
			select @Date = min(datetime)
			from admin_all.ACCOUNT_TRAN_ALL
			if @Date is null
			begin
				select @Date = min(datetime)
				from admin_all.ACCOUNT_TRAN_REALTIME
			end
		end
	end

	if @Date is null
		return;
	while @Date < cast(getdate() as date)
	begin
        print 'Calculating table transactions of date: ' + cast(@Date as nvarchar(20))
		begin transaction
		declare c cursor local static for 
			select distinct p.ID
			from admin_all.PLATFORM p
			where (
							@RelatedTransactionID  is null 
						or	
							@RelatedTransactionID  is not null 
						and p.ID = isnull(@ProductID, ID) 
				)
				and exists(
							select * 
							from admin_all.GAME_INFO gi 
								inner join admin_all.GAME_CATEGORY gc on gc.id = gi.GAME_CATEGORY_ID
							where p.ID = gi.PLATFORM_ID
								and gc.IS_TABLEGAME = 1
						)
		open c
		fetch next from c into @ProductID
		while @@fetch_status = 0
		begin
			save transaction a 
			exec croatia.usp_CreateTransactionHeader @TransactionID output, 'TableGameReport', @RelatedTransactionID
			insert into croatia.TransactionDailyTableGameHeader(TransactionID, DateOfReport, ProductID)
				values(@TransactionID, @Date, @ProductID)
			insert into croatia.TransactionDailyTableGameDetail(TransactionID, Casino, GameCode, PayInAmountReal, PayInAmountReleasedBonus, PayInAmountPlayableBonus, PayOutAmountReal, PayOutAmountReleasedBonus, PayOutAmountPlayableBonus, JackpotAccumulatedAmountReal, JackpotAccumulatedAmountReleasedBonus, JackpotAccumulatedAmountPlayableBonus, JackpotPayoutAccumulatedAmountReal, JackpotPayoutAccumulatedAmountReleasedBonus, JackpotPayoutAccumulatedAmountPlayableBonus, Currency)
				select @TransactionID, Casino, GameCode, PayInAmountReal, PayInAmountReleasedBonus, PayInAmountPlayableBonus, PayOutAmountReal, PayOutAmountReleasedBonus, PayOutAmountPlayableBonus, JackpotAccumulatedAmountReal, JackpotAccumulatedAmountReleasedBonus, JackpotAccumulatedAmountPlayableBonus, JackpotPayoutAccumulatedAmountReal, JackpotPayoutAccumulatedAmountReleasedBonus, JackpotPayoutAccumulatedAmountPlayableBonus, Currency
				from croatia.fn_DailyGameReportPerCasino(@Date, @ProductID, 0)
			if @@rowcount = 0
				rollback transaction a;
			if @RelatedTransactionID is not null
				break;
			fetch next from c into @ProductID
		end
		close c
		deallocate c
		commit
		if @RelatedTransactionID is not null
			break;
		select @Date = dateadd(day, 1, @Date)
	end
end

