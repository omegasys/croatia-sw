set quoted_identifier, ansi_nulls on
if object_id('croatia.usp_GenerateDailyIContoReportTransaction') is null
	exec('create procedure croatia.usp_GenerateDailyIContoReportTransaction as ')
go
alter procedure croatia.usp_GenerateDailyIContoReportTransaction
(
	@RelatedTransactionID bigint = null
)
as
begin
	set nocount on
	set xact_abort on
	declare @TransactionID bigint, @Date date, @ProductID int
	if @RelatedTransactionID is not null
	begin
		select top 1 @Date = DateOfReport, @ProductID = ProductID
		from croatia.TransactionDailyIContoHeader 
		where TransactionID = @RelatedTransactionID 
		order by DateOfReport desc

	end
	else
	begin
		select top 1 @Date = dateadd(day, 1, DateOfReport) from croatia.TransactionDailyIContoHeader order by DateOfReport desc
		if @Date is null
		begin
			select @Date = min(datetime)
			from admin_all.ACCOUNT_TRAN_ALL
			if @Date is null
			begin
				select @Date = min(datetime)
				from admin_all.ACCOUNT_TRAN_REALTIME
			end
		end
	end

	if @Date is null
		return;
	while @Date < cast(getdate() as date)
	begin
        print 'generating daily transactions of date: ' + cast(@Date as nvarchar(20))
		if not exists(select * from admin_all.DAILY_BALANCE_HISTORY where DATETIME = cast(@Date as datetime))
		begin
			raiserror('Could not find Daily Balance History records', 16, 1)
			return
		end
		begin transaction
		declare c cursor local static for
			select ID 
			from admin_all.PLATFORM p 
			where @RelatedTransactionID  is null 
				or		@RelatedTransactionID  is not null 
					and ID = isnull(@ProductID, ID) 
			order by 1
		open c
		fetch next from c into @ProductID
		while @@fetch_status = 0
		begin
			save transaction a 
			exec croatia.usp_CreateTransactionHeader @TransactionID output, 'IContoReport', @RelatedTransactionID
			insert into croatia.TransactionDailyIContoHeader(TransactionID, DateOfReport, ProductID)
				values(@TransactionID, @Date, @ProductID)
			insert into croatia.TransactionDailyIContoDetail(TransactionID, Casino, OpenBalanceReal, OpenBalanceReleasedBonus, OpenBalancePlayableBonus, PayInAmountReal, PayInAmountReleasedBonus, PayInAmountPlayableBonus, PayOutAmountReal, PayOutAmountReleasedBonus, PayOutAmountPlayableBonus, GamePayInAmountReal, GamePayInAmountReleasedBonus, GamePayInAmountPlayableBonus, GamePayOutAmountReal, GamePayOutAmountReleasedBonus, GamePayOutAmountPlayableBonus, Currency)
				select @TransactionID, Casino, OpenBalanceReal, OpenBalanceReleasedBonus, OpenBalancePlayableBonus, PayInAmountReal, PayInAmountReleasedBonus, PayInAmountPlayableBonus, PayOutAmountReal, PayOutAmountReleasedBonus, PayOutAmountPlayableBonus, GamePayInAmountReal, GamePayInAmountReleasedBonus, GamePayInAmountPlayableBonus, GamePayOutAmountReal, GamePayOutAmountReleasedBonus, GamePayOutAmountPlayableBonus, Currency
				from croatia.fn_DailyIContoReportPerCasino(@Date, @ProductID) x
			if @@rowcount = 0
				rollback transaction a;
			if @RelatedTransactionID is not null
				break;
			fetch next from c into @ProductID
		end
		close c
		deallocate c
		commit
		if @RelatedTransactionID is not null
			break;
	
		select @Date = dateadd(day, 1, @Date)
	end
end

