set ansi_nulls , quoted_identifier on
go
if object_id('croatia.usp_CreateTransactionHeader') is null
	exec('create procedure croatia.usp_CreateTransactionHeader as --')
	go
alter procedure croatia.usp_CreateTransactionHeader
(
	@TransactionID bigint output,
	@TransactionType varchar(30),
	@RelatedTransactionID bigint
)
as
begin
	set nocount, xact_abort on
	declare @Date datetime = getdate(), @TransactionLogID bigint, @Status varchar(30) = 'Pending'
	select @TransactionID = next value for croatia.SeqTranssactionID
	begin transaction
	insert into croatia.TransactionLog(TransactionID, Status, Date)
		values(@TransactionID, @Status, @Date)
		
	select @TransactionLogID =  scope_identity()
	insert into croatia.TransactionHeader(TransactionID, RelatedTransactionID, TransactionType, SendingDate, Status, StatusDate, TransactionLogID, Retries)
		values(@TransactionID, @RelatedTransactionID, @TransactionType, null , @Status, @Date, @TransactionLogID, 0 )
	commit
end
go