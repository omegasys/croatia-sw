set ansi_nulls , quoted_identifier on
go
if object_id('croatia.usp_GetTransactionDetail') is null
	exec('create procedure croatia.usp_GetTransactionDetail as --')
	go
alter procedure croatia.usp_GetTransactionDetail
(
	@TransactionID bigint,
	@TransactionType varchar(30),
	@SQLTransactionID bigint
)
as
begin
	set nocount, xact_abort on
	if current_transaction_id() <> @SQLTransactionID
	begin
		raiserror('croatia.usp_GetTransactionHeader and croatia.usp_GetTransactionDetails must be running within the same transaction', 16, 1)
		return
	end
	declare @PersonalID nvarchar(11) = admin_all.fn_GetRegistry('Croatia.Regulation.PersonalID'), 
			@PersonalName nvarchar(128) = admin_all.fn_GetRegistry('Croatia.Regulation.PersonalName')
	if @TransactionType = 'Product'
	begin
		select *, @PersonalID PersonalID, @PersonalName PersonalName
		from croatia.TransactionProduct
		where TransactionID = @TransactionID
	end
	else if @TransactionType = 'Game'
	begin
		select *, @PersonalID PersonalID, @PersonalName PersonalName
		from croatia.TransactionGame
		where TransactionID = @TransactionID
	end
	else if @TransactionType = 'Jackpot'
	begin
		select *, @PersonalID PersonalID, @PersonalName PersonalName
		from croatia.TransactionJackpot
		where TransactionID = @TransactionID
	end
	else if @TransactionType = 'TableGameReport'
	begin
		select h.DateOfReport, h.DateGenerated, d.*
		from croatia.TransactionDailyTableGameHeader h
			inner join croatia.TransactionDailyTableGameDetail d on h.TransactionID = d.TransactionID
		where h.TransactionID = @TransactionID
	end
	else if @TransactionType = 'SlotGameReport'
	begin
		select h.DateOfReport, h.DateGenerated, d.*
		from croatia.TransactionDailySlotGameHeader h
			inner join croatia.TransactionDailySlotGameDetail d on h.TransactionID = d.TransactionID
		where h.TransactionID = @TransactionID
	end
	else if @TransactionType = 'IContoReport'
	begin
		select h.DateOfReport, h.DateGenerated, d.*
		from croatia.TransactionDailyIContoHeader h
			inner join croatia.TransactionDailyIContoDetail d on h.TransactionID = d.TransactionID
		where h.TransactionID = @TransactionID
	end
	else if @TransactionType = 'MonthlyReport'
	begin
		select h.DateOfReport, h.DateGenerated, d.*
		from croatia.TransactionMonthlyHeader h
			inner join croatia.TransactionMonthlyDetail d on h.TransactionID = d.TransactionID
		where h.TransactionID = @TransactionID
	end
end 