set quoted_identifier, ansi_nulls on
if object_id('croatia.usp_GetTotalBalance') is null
	exec('create procedure croatia.usp_GetTotalBalance as ')
go
alter procedure croatia.usp_GetTotalBalance
as
begin
	set nocount on
	select 
		u.CURRENCY,
		sum(a.BALANCE_REAL) BalanceReal,
		sum(a.PLAYABLE_BONUS) PlayableBonus,
		sum(a.RELEASED_BONUS) ReleasedVonus,
		sum(a.RAW_LOYALTY_POINTS) RawLoyaltyPoints,
		sum(a.SECONDARY_BALANCE) SecondaryBalance,
		sum(a.UNPAID_CIT) UnpaidCIT
	from admin_all.ACCOUNT a
		inner join external_mpt.USER_CONF u on a.PARTYID = u.PARTYID
	group by u.CURRENCY
	order by 1
end