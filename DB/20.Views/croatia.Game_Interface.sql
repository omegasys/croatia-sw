set ansi_nulls , quoted_identifier on
go
if object_id('croatia.Game_Interface') is null
	exec('create view croatia.Game_Interface as select 1 as one')
go
alter view [croatia].[Game_Interface] as select cast([GameCode] as varchar(64)) as [GameCode],cast([Product] as varchar(64)) as [Product],cast([GameName] as varchar(128)) as [GameName],cast([GameType] as char(1)) as [GameType],cast([Jackpots] as nvarchar(max)) as [Jackpots],cast(0 as bit) as ___IsDeleted___ from [croatia].[Game]
go
