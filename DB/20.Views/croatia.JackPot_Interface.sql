set ansi_nulls , quoted_identifier on
go
if object_id('croatia.JackPot_Interface') is null
	exec('create view croatia.JackPot_Interface as select 1 as one')
go
alter view [croatia].[JackPot_Interface] as select cast([JackpotCode] as varchar(64)) as [JackpotCode],cast([Product] as varchar(64)) as [Product], cast([JackpotName] as varchar(128)) as [JackpotName],cast([Products] as nvarchar(max)) as [Products],cast(0 as bit) as ___IsDeleted___ from [croatia].[JackPot]
go
