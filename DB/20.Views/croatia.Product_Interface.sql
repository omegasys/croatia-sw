set ansi_nulls , quoted_identifier on
go
if object_id('croatia.Product_Interface') is null
	exec('create view croatia.Product_Interface as select 1 as one')
go
alter view [croatia].[Product_Interface] as select cast([CODE] as nvarchar(20)) as [CODE],cast([NAME] as varchar(50)) as [NAME],cast([IS_ENABLED] as tinyint) as [IS_ENABLED],cast(0 as bit) as ___IsDeleted___ from [croatia].[Product]
GO


