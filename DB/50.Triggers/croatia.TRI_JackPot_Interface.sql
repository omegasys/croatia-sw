set ansi_nulls , quoted_identifier on
go
if object_id('croatia.TRI_Jackpot_Interface') is null
	exec('create trigger [croatia].[TRI_Jackpot_Interface] on [croatia].[Jackpot_Interface]
instead of insert
as
begin
	return
end')
go
alter trigger [croatia].[TRI_Jackpot_Interface] on [croatia].[Jackpot_Interface]
instead of insert
as
begin
	if @@rowcount = 0
		return
	set nocount on;
	
	merge [croatia].[JackPot] t
	using inserted s on s.[JackpotCode] = t.[JackpotCode]
	when not matched and isnull(s.___IsDeleted___, 0) = 0 then
		insert ([JackpotCode],[JackpotName],[Products], Product)
			values (s.[JackpotCode],s.[JackpotName],s.[Products], s.Product)
	when matched and isnull(s.___IsDeleted___, 0) = 0 and ( s.[Product] is null and t.[Product] is not null or s.[Product] is not null and t.[Product] is null or s.[Product] <> t.[Product] or s.[JackpotName] is null and t.[JackpotName] is not null or s.[JackpotName] is not null and t.[JackpotName] is null or s.[JackpotName] <> t.[JackpotName] or s.[Products] is null and t.[Products] is not null or s.[Products] is not null and t.[Products] is null or s.[Products] <> t.[Products]) then
		update set t.[JackpotName] = s.[JackpotName],t.[Products] = s.[Products], t.Product = s.Product
	when matched and isnull(s.___IsDeleted___, 0) = 1 then
		delete
	;
	
end
go


