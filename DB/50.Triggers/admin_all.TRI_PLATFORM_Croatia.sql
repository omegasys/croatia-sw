set ansi_nulls , quoted_identifier on
go
if object_id('admin_all.TRI_PLATFORM_Croatia') is null
	exec('create trigger admin_all.TRI_PLATFORM_Croatia on admin_all.PLATFORM
for insert
as
begin
	return
end')
go
alter trigger admin_all.TRI_PLATFORM_Croatia on admin_all.PLATFORM
for insert, delete, update
as
begin
	if @@rowcount = 0
		return
	set nocount on
	if croatia.fn_IsEnabled() = 0
		return
	insert into croatia.Product_Interface(CODE, NAME, IS_ENABLED, ___IsDeleted___)
		select 
			case when i.CODE is not null then i.CODE else d.CODE end as CODE,
			case when i.CODE is not null then i.NAME else d.NAME end as NAME,
			case when i.CODE is not null then i.IS_ENABLED else d.IS_ENABLED end as IS_ENABLED,
			case when i.CODE is not null and i.IS_ENABLED = 1 then 0 else 1 end as ___IsDeleted___
		from inserted i
			full outer join deleted d on i.CODE = d.CODE

	;with x0 as
	(
		select 
			isnull(i.ID, d.ID) ID
		from inserted i
			full outer join deleted d on i.CODE = d.CODE
		where i.CODE is null or d.CODE is null
	),
	x1 as
	(
		select 
				cast(j.ID as varchar(64))JackpotCode,
				cast(j.NAME as varchar(128)) JackpotName,
				p.Code as Product,
				(
					select p.CODE as Product
					from admin_all.PLATFORM p
					where exists(
								select *
								from admin_all.GAME_JACKPOT_LINK l 
									inner join admin_all.GAME_INFO gi on gi.ID = l.GAME_ID 
									inner join admin_all.GAME_CATEGORY gc on gc.ID = gi.GAME_CATEGORY_ID and (gc.IS_SLOTS <> 0 or gc.IS_TABLEGAME <> 0)
								where gi.PLATFORM_ID = p.ID
									and l.JACKPOT_ID = j.ID)
						and p.IS_ENABLED = 1
					for xml path('Products')
				) Products,
				p.IS_ENABLED
		from admin_all.BRAND_JACKPOTS j
			inner join admin_all.PLATFORM p on p.id = j.PLATFORM_ID
		where exists(select * from x0 where x0.ID = p.ID)
	)
	insert into croatia.JackPot_Interface(JackpotCode, JackpotName, Product, Products, ___IsDeleted___)
		select JackpotCode, JackpotName, Product, Products, case when Products is null or IS_ENABLED = 0 then 1 else 0 end
		from x1

	;with x0 as
	(
		select 
			isnull(i.ID, d.ID) ID
		from inserted i
			full outer join deleted d on i.CODE = d.CODE
		where i.CODE is null or d.CODE is null
	),
	x1 as
	(
		select 
				cast(gi.ID as varchar(64)) as GameCode,
				cast(gi.NAME as varchar(128)) as GameName,
				p.Code as Product,
				case 
					when gc.IS_SLOTS = 1 then 'A' 
					when gc.IS_TABLEGAME = 1 then 'S' 
				end GameType,
				(
					select gl.JACKPOT_ID as Jackpot
					from admin_all.GAME_JACKPOT_LINK gl 
					where gl.GAME_ID = gi.ID
					for xml path('Jackpots')
				) Jackpots,
				gc.IS_SLOTS,
				gc.IS_TABLEGAME,
				p.IS_ENABLED
		from admin_all.GAME_CATEGORY gc
			inner join admin_all.GAME_INFO gi on gi.GAME_CATEGORY_ID = gc.ID
			inner join admin_all.PLATFORM p on p.id = gi.PLATFORM_ID
		where exists(select * from x0 where x0.ID = p.ID)
	)
	insert into croatia.Game_Interface(GameCode, GameName, GameType, Product, Jackpots, ___IsDeleted___)
		select GameCode, GameName, GameType, Product, Jackpots, case when IS_SLOTS = 0 and IS_TABLEGAME = 0 or IS_ENABLED = 0 then 1 else 0 end ___IsDeleted___
		from x1
end
/*
select 'case when i.CODE is not null then i.' + name + ' else d.' + name +' end as ' + name +',',
		
from sys.columns c
where object_id = object_id('admin_all.platform')
order by column_id
*/