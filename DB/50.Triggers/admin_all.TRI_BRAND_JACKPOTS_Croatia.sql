set ansi_nulls , quoted_identifier on
go
if object_id('admin_all.TRI_BRAND_JACKPOTS_Croatia') is null
	exec('create trigger admin_all.TRI_BRAND_JACKPOTS_Croatia on admin_all.BRAND_JACKPOTS
for insert
as
begin
	return
end')
go
alter trigger admin_all.TRI_BRAND_JACKPOTS_Croatia on admin_all.BRAND_JACKPOTS
for insert, delete, update
as
begin
	if @@rowcount = 0
		return
	set nocount on;
	if croatia.fn_IsEnabled() = 0
		return
	;with x0 as
	(
		select 
			isnull(i.ID, d.ID) ID,
			case when i.ID is null then d.PLATFORM_ID else i.PLATFORM_ID end PLATFORM_ID,
			case when i.ID is null then d.NAME else i.NAME end NAME,
			case when i.ID is null then 1 else 0 end ___IsDeleted___
		from inserted i
			full outer join deleted d on i.ID = d.ID
	),
	x1 as
	(
		select 
				cast(x0.ID as varchar(64))JackpotCode,
				cast(x0.NAME as varchar(128)) JackpotName,
				p.CODE as Product,
				case when x0.___IsDeleted___ = 0 then 
					(
						select p.CODE as Product
						from admin_all.PLATFORM p
						where exists(
									select *
									from admin_all.GAME_JACKPOT_LINK l 
										inner join admin_all.GAME_INFO gi on gi.ID = l.GAME_ID 
										inner join admin_all.GAME_CATEGORY gc on gc.ID = gi.GAME_CATEGORY_ID and (gc.IS_SLOTS <> 0 or gc.IS_TABLEGAME <> 0)
									where gi.PLATFORM_ID = p.ID
										and l.JACKPOT_ID = x0.ID)
							and p.IS_ENABLED = 1
						for xml path('Products')
					) 
				end as Products,
				p.IS_ENABLED,
				x0.___IsDeleted___
		from  x0
			inner join admin_all.PLATFORM p on p.ID = x0.PLATFORM_ID
	)
	insert into croatia.JackPot_Interface(JackpotCode, JackpotName, Product, Products, ___IsDeleted___)
		select JackpotCode, JackpotName, Product, Products, case when Products is null or IS_ENABLED = 0 then 1 else ___IsDeleted___ end
		from x1

	
end
go

--select * from admin_all.BRAND_JACKPOTS