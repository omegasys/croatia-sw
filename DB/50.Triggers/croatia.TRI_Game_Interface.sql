set ansi_nulls , quoted_identifier on
go
if object_id('croatia.TRI_Game_Interface') is null
	exec('create trigger [croatia].[TRI_Game_Interface] on [croatia].[Game_Interface]
instead of insert
as
begin
	return
end')
go
alter trigger [croatia].[TRI_Game_Interface] on [croatia].[Game_Interface]
instead of insert
as
begin
	if @@rowcount = 0
		return
	set nocount on;
	
	merge [croatia].[Game] t
	using inserted s on s.[GameCode] = t.[GameCode]
	when not matched and isnull(s.___IsDeleted___, 0) = 0 then
		insert ([GameCode],[GameName],[GameType],[Jackpots], Product)
			values (s.[GameCode],s.[GameName],s.[GameType],s.[Jackpots], s.Product)
	when matched and isnull(s.___IsDeleted___, 0) = 0 and (s.[Product] is null and t.[Product] is not null or s.[Product] is not null and t.[Product] is null or s.[Product] <> t.[Product] or s.[GameName] is null and t.[GameName] is not null or s.[GameName] is not null and t.[GameName] is null or s.[GameName] <> t.[GameName] or s.[GameType] is null and t.[GameType] is not null or s.[GameType] is not null and t.[GameType] is null or s.[GameType] <> t.[GameType] or s.[Jackpots] is null and t.[Jackpots] is not null or s.[Jackpots] is not null and t.[Jackpots] is null or s.[Jackpots] <> t.[Jackpots]) then
		update set t.[GameName] = s.[GameName],t.[GameType] = s.[GameType],t.[Jackpots] = s.[Jackpots], t.Product = s.Product
	when matched and isnull(s.___IsDeleted___, 0) = 1 then
		delete
	;
	
end
