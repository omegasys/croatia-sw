set ansi_nulls , quoted_identifier on
go
if object_id('croatia.TRI_Jackpot') is null
	exec('create trigger croatia.TRI_Jackpot on croatia.Jackpot
for insert
as
begin
	return
end')
go 
alter trigger croatia.TRI_Jackpot on croatia.Jackpot
for insert, delete, update
as
begin
	if @@rowcount = 0
		return
	set nocount on
	declare @TransactionID bigint, @Code nvarchar(64), @Date datetime, @Status char(1), @Name nvarchar(100), @Casino nvarchar(64), @Products xml
	declare c cursor local fast_forward for
		select 
			isnull(i.JackpotCode, d.JackpotCode) Code,
			Getdate() Date,
			case 
				when i.JackpotCode is null then 'N'
				when i.JackpotCode is not null and d.JackpotCode is not null then 'A'
				when d.JackpotCode is null then 'A'
				else 'P'
			end,
			case when i.JackpotCode is null then d.JackpotName else i.JackpotName end Name,
			case when i.JackpotCode is null then d.Product else i.Product end Product,
			cast(case when i.JackpotCode is null then d.Products else i.Products end as xml) Products
		from inserted i
			full outer join deleted d on i.JackpotCode = d.JackpotCode
	open c
	fetch next from c into @Code, @Date, @Status, @Name, @Casino, @Products
	while @@fetch_status = 0
	begin
		exec croatia.usp_CreateTransactionHeader @TransactionID output, 'Jackpot', null
		insert into croatia.TransactionJackpot(TransactionID, Code, Name, Date, Status, Casino, Product)
			select @TransactionID, @Code, @Name, @Date, @Status, @Casino, v.value('.', 'nvarchar(64)')
			from @Products.nodes('/Products/Product') j(v)
		fetch next from c into @Code, @Date, @Status, @Name, @Casino, @Products
	end
	close c
	deallocate c
end
go

--select * from croatia.Jackpot
