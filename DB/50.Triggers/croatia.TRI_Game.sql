set ansi_nulls , quoted_identifier on
go
if object_id('croatia.TRI_Game') is null
	exec('create trigger croatia.TRI_Game on croatia.Game
for insert
as
begin
	return
end')
go 
alter trigger croatia.TRI_Game on croatia.Game
for insert, delete, update
as
begin
	if @@rowcount = 0
		return
	set nocount on
	declare @TransactionID bigint, @Code nvarchar(64), @Type varchar(1), @Date datetime, @Status char(1), @Name nvarchar(100), @Casino nvarchar(64), @Jackpots xml
	declare c cursor local fast_forward for
		select 
			isnull(i.GameCode, d.GameCode) Code,
			case when i.GameCode is null then d.GameType else i.GameType end as Type,
			Getdate() Date,
			case 
				when i.GameCode is null then 'N'
				when i.GameCode is not null and d.GameCode is not null then 'A'
				when d.GameCode is null then 'A'
				else 'P'
			end,
			case when i.GameCode is null then d.GameName else i.GameName end Name,
			case when i.GameCode is null then d.Product else i.Product end Product,
			cast(case when i.GameCode is null then d.Jackpots else i.Jackpots end as xml) Jackpots
		from inserted i
			full outer join deleted d on i.GameCode = d.GameCode
	open c
	fetch next from c into @Code, @Type, @Date, @Status, @Name, @Casino, @Jackpots
	while @@fetch_status = 0
	begin
		exec croatia.usp_CreateTransactionHeader @TransactionID output, 'Game', null
		if @Jackpots is not null
		begin
			insert into croatia.TransactionGame(TransactionID, Code, Name, Type, Date, Status, Casino, Jackpot)
				select @TransactionID, @Code, @Name, @Type, @Date, @Status, @Casino, v.value('.', 'nvarchar(64)')
				from @Jackpots.nodes('/Jackpots/Jackpot') j(v)
		end
		else
		begin
			insert into croatia.TransactionGame(TransactionID, Code, Name, Type, Date, Status, Casino, Jackpot)
				select @TransactionID, @Code, @Name, @Type, @Date, @Status, @Casino, ''
		end
		fetch next from c into @Code, @Type, @Date, @Status, @Name, @Casino, @Jackpots
	end
	close c
	deallocate c
end
go

--select * from croatia.Game
