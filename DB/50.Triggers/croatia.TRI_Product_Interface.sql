set ansi_nulls , quoted_identifier on
go
if object_id('croatia.TRI_Product_Interface') is null
	exec('create trigger [croatia].[TRI_Product_Interface] on [croatia].[Product_Interface]
instead of insert
as
begin
	return
end')
go
alter trigger [croatia].[TRI_Product_Interface] on [croatia].[Product_Interface]
instead of insert
as
begin
	if @@rowcount = 0
		return
	set nocount on;
	merge [croatia].[Product] t
	using inserted s on s.[CODE] = t.[CODE]
	when not matched and isnull(s.___IsDeleted___, 0) = 0 then
		insert ([CODE],[NAME],[IS_ENABLED])
			values (s.[CODE],s.[NAME],s.[IS_ENABLED])
	when matched and isnull(s.___IsDeleted___, 0) = 0 and ( s.[NAME] is null and t.[NAME] is not null or s.[NAME] is not null and t.[NAME] is null or s.[NAME] <> t.[NAME] or s.[IS_ENABLED] is null and t.[IS_ENABLED] is not null or s.[IS_ENABLED] is not null and t.[IS_ENABLED] is null or s.[IS_ENABLED] <> t.[IS_ENABLED]) then
		update set t.[NAME] = s.[NAME],t.[IS_ENABLED] = s.[IS_ENABLED]
	when matched and isnull(s.___IsDeleted___, 0) = 1 then
		delete
	;
	
end
