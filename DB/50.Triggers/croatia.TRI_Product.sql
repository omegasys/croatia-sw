set ansi_nulls , quoted_identifier on
go
if object_id('croatia.TRI_Product') is null
	exec('create trigger croatia.TRI_Product on croatia.Product
for insert
as
begin
	return
end')
go 
alter trigger croatia.TRI_Product on croatia.Product
for insert, delete, update
as
begin
	if @@rowcount = 0
		return
	set nocount on
	declare @TransactionID bigint, @Code nvarchar(64), @Type varchar(1), @Date datetime, @Status char(1), @Name nvarchar(100)
	declare c cursor local fast_forward for
		select 
			isnull(i.CODE, d.CODE) Code,
			'I' as Type,
			Getdate() Date,
			case 
				when i.CODE is null then 'N'
				when i.CODE is not null and d.CODE is not null and i.IS_ENABLED = 1 and d.IS_ENABLED = 0 then 'N'
				when i.CODE is not null and d.CODE is not null and i.IS_ENABLED = 0 and d.IS_ENABLED = 1 then 'A'
				when d.CODE is null then 'A'
				else 'P'
			end,
			case when i.CODE is null then d.NAME else i.NAME end Name
		from inserted i
			full outer join deleted d on i.CODE = d.CODE
	open c
	fetch next from c into @Code, @Type, @Date, @Status, @Name
	while @@fetch_status = 0
	begin
		exec croatia.usp_CreateTransactionHeader @TransactionID output, 'Product', null
		insert into croatia.TransactionProduct(TransactionID, Code, Name, Type, Date, Status)
			values(@TransactionID, @Code, @Name, @Type, @Date, @Status)
		fetch next from c into @Code, @Type, @Date, @Status, @Name
	end
	close c
	deallocate c
end
