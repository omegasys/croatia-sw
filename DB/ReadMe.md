# Database Overview for Croatia Regulatory Requirement

The tables are designed to support Croatia surveillance message exchange between the online casino operator 
and the online casino surveillance system of the Ministry of Finance (CIS).

# Table Overview
- croatia.TransactionLog: Maintains the status of the request & response for communication between CIS. Communication error
would be stored in the message
- croatia.Product: Maintains the status of casino (platform)  
- croatia.Game: Maintains the game definitions
- croatia.JackPot: Maintains the relevant jackpot definition

# Configuration
Croatia.Regulation.Enabled is used in registry hash to control croatia related replications

## Administrative control

### Change of casino [Platform]
- admin_all.Platform table defines all the game providers
- trigger admin_all.TRI_PLATFORM_Croatia is defined on admin_all.PLATFORM to replicate the relevant 
casino information to croatia.Product table
- Trigger croatia.TRI_Product defined on croatia.Product to populate regulatory data exchange
 header croatia.TransactionHeader and payload transaction croatia.TransactionProduct

### Change of game [Game_Info] 
- Games are defined initially in the admin_all.GAME_INFO table.
- Trigger admin_all.TRI_GAME_INFO_Croatia is created to populate the change of the GAME_INFO, GAME_CATEGORY, 
 GAME_JACKPOT_LINK, to croatia.Game
- Trigger croatia.TRI_Game on croatia.Game is created to populate regulatory data exchange header croatia.TransactionHeader 
and payload transaction croatia.TransactionGame

### Change of jackpot [BRAND_JACKPOTS and GAME_JACKPOT_LINK]
- Jackpot information is defined in admin_all.BRAND_JACKPOTS table
- Game Jackpot relationship is defined in admin_all.GAME_JACKPOT_LINK
- triggers on both tables were created to replicate relevant settings to croatia.JackPot
- trigger on croatia.JackPot is created to populate regulatory data exchange header croatia.TransactionHeader 
and payload transaction croatia.TransactionJackpot



....
TO BE CONTINUED


