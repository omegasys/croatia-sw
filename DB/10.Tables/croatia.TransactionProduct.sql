set ansi_nulls, quoted_identifier on
go
if object_id('croatia.TransactionProduct')  is null
begin
	create table croatia.TransactionProduct
	(
		TransactionID  bigint not null,
		Code nvarchar(64) not null,
		Name nvarchar(100),
		Type char(1) not null,
		Date datetime,
		Status char(1) not null,
		constraint PK_croatia_TransactionProduct primary key clustered (TransactionID),
		constraint FK_croatia_TransactionProduct_TransactionHeader foreign key(TransactionID) references croatia.TransactionHeader(TransactionID)
	)
end
go
--drop table croatia.TransactionProduct