set quoted_identifier, ansi_nulls on
if object_id('croatia.TransactionMonthlyDetail') is null
begin
	create table croatia.TransactionMonthlyDetail
	(
		TransactionID bigint not null,
		Casino nvarchar(64) not null,

		SlotPayInAmountReal numeric(38,18) not null,
		SlotPayInAmountReleasedBonus numeric(38,18) not null,
		SlotPayInAmountPlayableBonus numeric(38,18) not null,
		
		SlotPayOutAmountReal numeric(38,18) not null,
		SlotPayOutAmountReleasedBonus numeric(38,18) not null,
		SlotPayOutAmountPlayableBonus numeric(38,18) not null,
		
		SlotJackpotAccumulatedAmountReal numeric(38,18) not null,
		SlotJackpotAccumulatedPayInAmountReleasedBonus numeric(38,18) not null,
		SlotJackpotAccumulatedPayInAmountPlayableBonus numeric(38,18) not null,
		
		SlotJackpotPayOutAmountReal numeric(38,18) not null,
		SlotJackpotPayOutAmountReleasedBonus numeric(38,18) not null,
		SlotJackpotPayOutAmountPlayableBonus numeric(38,18) not null,

		TablePayInAmountReal numeric(38,18) not null,
		TablePayInAmountReleasedBonus numeric(38,18) not null,
		TablePayInAmountPlayableBonus numeric(38,18) not null,
		
		TablePayOutAmountReal numeric(38,18) not null,
		TablePayOutAmountReleasedBonus numeric(38,18) not null,
		TablePayOutAmountPlayableBonus numeric(38,18) not null,
		
		TableJackpotAccumulatedAmountReal numeric(38,18) not null,
		TableJackpotAccumulatedPayInAmountReleasedBonus numeric(38,18) not null,
		TableJackpotAccumulatedPayInAmountPlayableBonus numeric(38,18) not null,
		
		TableJackpotPayOutAmountReal numeric(38,18) not null,
		TableJackpotPayOutAmountReleasedBonus numeric(38,18) not null,
		TableJackpotPayOutAmountPlayableBonus numeric(38,18) not null,
		Currency nchar(3) not null
		constraint PK_croatia_TransactionMonthlyDetail primary key (TransactionID, Casino, Currency)
	)
end
-- drop table croatia.TransactionMonthlyDetail