set ansi_nulls, quoted_identifier on
go
if object_id('croatia.Game')  is null
begin
	 create table croatia.Game
	 (
		GameCode varchar(64) not null,
		GameName varchar(128) not null,
		Product varchar(64) not null,
		GameType char(1) not null,
		Jackpots nvarchar(max),
		constraint PK_croatia_Game primary key(GameCode)
	) 
end
go
