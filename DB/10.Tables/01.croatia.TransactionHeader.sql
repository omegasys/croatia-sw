set ansi_nulls, quoted_identifier on
go
if object_id('croatia.TransactionHeader')  is null
begin
	create table croatia.TransactionHeader
	(
		TransactionID  bigint not null,
		GUID uniqueidentifier constraint DF_croatia_TransactionHeader default(newsequentialid()),
		RelatedTransactionID bigint,
		TransactionType varchar(30) not null,
		SendingDate datetime null,
		Status varchar(30) not null,--Pending, Sending, Sent, Exception, Error,....
		StatusDate datetime not null,
		TransactionLogID bigint not null,
		Retries int not null,
		constraint PK_croatia_TransactionHeader primary key clustered (TransactionID),
		constraint FK_croatia_TransactionHeader_RelatedTransactionID foreign key (RelatedTransactionID) references croatia.TransactionHeader(TransactionID)
	)
end

if not exists(select * from sys.indexes where name= 'IDX_croatia_TransactionHeader_RelatedTransactionID' and object_id = object_id('croatia.TransactionHeader'))
	create index IDX_croatia_TransactionHeader_RelatedTransactionID on croatia.TransactionHeader(RelatedTransactionID)

if not exists(select * from sys.indexes where name= 'IDX_croatia_TransactionHeader_GUID' and object_id = object_id('croatia.TransactionHeader'))
	create index IDX_croatia_TransactionHeader_GUID on croatia.TransactionHeader(GUID)

if not exists(select * from sys.indexes where name= 'IDX_croatia_TransactionHeader_Status_StatusDate' and object_id = object_id('croatia.TransactionHeader'))
	create index IDX_croatia_TransactionHeader_Status_StatusDate on croatia.TransactionHeader(Status, StatusDate) include(TransactionLogID)
go
if not exists(select * from sys.indexes where name= 'IDX_croatia_TransactionHeader_Status_StatusDate' and object_id = object_id('croatia.TransactionHeader'))
	create index IDX_croatia_TransactionHeader_Status_StatusDate on croatia.TransactionHeader(Status, StatusDate) include(TransactionLogID)
go
--drop table croatia.TransactionHeader