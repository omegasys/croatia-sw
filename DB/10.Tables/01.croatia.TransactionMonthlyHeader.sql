set quoted_identifier, ansi_nulls on
if object_id('croatia.TransactionMonthlyHeader') is null
begin
	create table croatia.TransactionMonthlyHeader
	(
		TransactionID bigint not null,
		DateOfReport date not null,
		DateGenerated datetime not null constraint DF_croatia_TransactionMonthlyHeader_DateGenerated default(getdate()),
		ProductID int not null,
		constraint PK_croatia_TransactionMonthlyHeader primary key (TransactionID)
	)
end
if not exists(select * from sys.indexes where object_id = object_id('croatia.TransactionMonthlyHeader') and name = 'IDX_croatia_TransactionMonthlyHeader_DateOfReport')
begin
	create index IDX_croatia_TransactionMonthlyHeader_DateOfReport on croatia.TransactionMonthlyHeader(DateOfReport)
end
--- drop table croatia.TransactionMonthlyHeader