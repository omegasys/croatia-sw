set quoted_identifier, ansi_nulls on
if object_id('croatia.TransactionDailyIContoHeader') is null
begin
	create table croatia.TransactionDailyIContoHeader
	(
		TransactionID bigint not null,
		DateOfReport date not null,
		DateGenerated datetime not null constraint DF_croatia_TransactionDailyIContoHeader_DateGenerated default(getdate()),
		ProductID int not null,
		constraint PK_croatia_TransactionDailyIContoHeader primary key (TransactionID),
		constraint FK_croatia_TransactionDailyIContoHeader_TransactionHeader foreign key (TransactionID) references croatia.TransactionHeader(TransactionID)
	)
end
if not exists(select * from sys.indexes where object_id = object_id('croatia.TransactionDailyIContoHeader') and name = 'IDX_croatia_TransactionDailyIContoHeader_DateOfReport')
begin
	create index IDX_croatia_TransactionDailyIContoHeader_DateOfReport on croatia.TransactionDailyIContoHeader(DateOfReport)
end
--- drop table croatia.TransactionDailyIContoHeader