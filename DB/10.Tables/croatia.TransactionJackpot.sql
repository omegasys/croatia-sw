set ansi_nulls, quoted_identifier on
go
if object_id('croatia.TransactionJackpot')  is null
begin
	create table croatia.TransactionJackpot
	(
		TransactionID  bigint not null,
		Code nvarchar(64) not null,
		Name nvarchar(100),
		Date datetime,
		Status char(1) not null,
		Casino nvarchar(64) not null,
		Product nvarchar(64) not null,
		constraint PK_croatia_TransactionJackpot primary key clustered (TransactionID, Code, Product),
		constraint FK_croatia_TransactionJackpot_TransactionHeader foreign key(TransactionID) references croatia.TransactionHeader(TransactionID)
	)
end
go
--drop table croatia.TransactionJackpot