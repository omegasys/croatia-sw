set ansi_nulls, quoted_identifier on
go
if object_id('croatia.Product')  is null
begin
	create table croatia.Product
	(
		CODE nvarchar(20) not null,
		NAME varchar(50),
		IS_ENABLED tinyint,
		--USERNAME varchar(20),
		--PASSWORD varchar(50),
		--WALLET_TYPE varchar(20) not null,
		--PLATFORM_TYPE varchar(20) not null,
		--CONVERTJACKPOTS tinyint not null,
		--SESSION_LIMIT_MOBILE varchar(10) not null,
		--REALITY_CHECK_MOBILE varchar(10) not null,
		--SESSION_LIMIT_WEB varchar(10) not null,
		--REALITY_CHECK_WEB varchar(10) not null,
		--EXCLUDED_COUNTRIES varchar(max) null,
		--PROVIDER_ID int null,
		--segment_id int NOT NULL,
		--is_ccyonthefly_enabled bit NULL,
		constraint PK_croatia_Product primary key clustered (Code)
	) 
end