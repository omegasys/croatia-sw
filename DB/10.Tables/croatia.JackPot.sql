set ansi_nulls, quoted_identifier on
go
if object_id('croatia.JackPot')  is null
begin
	 create table croatia.JackPot
	 (
		JackpotCode varchar(64) not null,
		JackpotName varchar(128) not null,
		Product varchar(64) not null,
		Products nvarchar(max) not null,
		constraint PK_croatia_GameJackPot primary key(JackpotCode)
	) 
end
go
