set ansi_nulls, quoted_identifier on
go
if object_id('croatia.TransactionLog')  is null
begin
	create table croatia.TransactionLog
	(
		TransactionID  bigint not null,
		TransactionLogID bigint identity(1,1) not null,
		Status varchar(30) not null,
		Date datetime not null,
		Message nvarchar(max),
		constraint PK_croatia_TransactionLog primary key clustered (TransactionID, TransactionLogID),
		--constraint FK_croatia_TransactionLog_TransactionHeader foreign key(TransactionID) references croatia.TransactionHeader(TransactionID)
	)
end
go
--drop table croatia.TransactionLog