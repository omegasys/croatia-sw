set quoted_identifier, ansi_nulls on
if object_id('croatia.TransactionDailyTableGameHeader') is null
begin
	create table croatia.TransactionDailyTableGameHeader
	(
		TransactionID bigint not null,
		DateOfReport date not null,
		DateGenerated datetime not null constraint DF_croatia_TransactionDailyTableGameHeader_DateGenerated default(getdate()),
		ProductID int not null,
		constraint PK_croatia_TransactionDailyTableGameHeader primary key (TransactionID),
		constraint FK_croatia_TransactionDailyTableGameHeader_TransactionHeader foreign key (TransactionID) references croatia.TransactionHeader(TransactionID)
	)
end
if not exists(select * from sys.indexes where object_id = object_id('croatia.TransactionDailyTableGameHeader') and name = 'IDX_croatia_TransactionDailyTableGameHeader_DateOfReport')
begin
	create index IDX_croatia_TransactionDailyTableGameHeader_DateOfReport on croatia.TransactionDailyTableGameHeader(DateOfReport)
end
--- drop table croatia.TransactionDailyTableGameHeader