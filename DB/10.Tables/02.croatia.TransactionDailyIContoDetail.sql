set quoted_identifier, ansi_nulls on
if object_id('croatia.TransactionDailyIContoDetail') is null
begin
	create table croatia.TransactionDailyIContoDetail
	(
		TransactionID bigint not null,
		Casino nvarchar(64) not null,

		OpenBalanceReal numeric(38,18) not null,
		OpenBalanceReleasedBonus numeric(38,18) not null,
		OpenBalancePlayableBonus numeric(38,18) not null,

		PayInAmountReal numeric(38,18) not null,
		PayInAmountReleasedBonus numeric(38,18) not null,
		PayInAmountPlayableBonus numeric(38,18) not null,
		
		PayOutAmountReal numeric(38,18) not null,
		PayOutAmountReleasedBonus numeric(38,18) not null,
		PayOutAmountPlayableBonus numeric(38,18) not null,

		GamePayInAmountReal numeric(38,18) not null,
		GamePayInAmountReleasedBonus numeric(38,18) not null,
		GamePayInAmountPlayableBonus numeric(38,18) not null,
		
		GamePayOutAmountReal numeric(38,18) not null,
		GamePayOutAmountReleasedBonus numeric(38,18) not null,
		GamePayOutAmountPlayableBonus numeric(38,18) not null,
		Currency nchar(3) not null
		constraint PK_croatia_TransactionDailyIContoDetail primary key (TransactionID, Casino, Currency),
		constraint FK_croatia_TransactionDailyIContoDetail_TransactionDailyIContoHeader foreign key (TransactionID) references croatia.TransactionDailyIContoHeader(TransactionID)
	)
end
-- drop table croatia.TransactionDailyIContoDetail