set quoted_identifier, ansi_nulls on
if object_id('croatia.TransactionDailySlotGameHeader') is null
begin
	create table croatia.TransactionDailySlotGameHeader
	(
		TransactionID bigint not null,
		DateOfReport date not null,
		DateGenerated datetime not null constraint DF_croatia_TransactionDailySlotGameHeader_DateGenerated default(getdate()),
		ProductID int not null,
		constraint PK_croatia_TransactionDailySlotGameHeader primary key (TransactionID),
		constraint FK_croatia_TransactionDailySlotGameHeader_TransactionHeader foreign key (TransactionID) references croatia.TransactionHeader(TransactionID)
	)
end
if not exists(select * from sys.indexes where object_id = object_id('croatia.TransactionDailySlotGameHeader') and name = 'IDX_croatia_TransactionDailySlotGameHeader_DateOfReport')
begin
	create index IDX_croatia_TransactionDailySlotGameHeader_DateOfReport on croatia.TransactionDailySlotGameHeader(DateOfReport)
end
--- drop table croatia.TransactionDailySlotGameHeader