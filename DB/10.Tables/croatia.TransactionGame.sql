set ansi_nulls, quoted_identifier on
go
if object_id('croatia.TransactionGame')  is null
begin
	create table croatia.TransactionGame
	(
		TransactionID  bigint not null,
		Code nvarchar(64) not null,
		Name nvarchar(100),
		Type char(1) not null,
		Date datetime,
		Status char(1) not null,
		Casino nvarchar(64) not null,
		Jackpot nvarchar(64) not null,
		constraint PK_croatia_TransactionGame primary key clustered (TransactionID, Code, Jackpot),
		constraint FK_croatia_TransactionGame_TransactionHeader foreign key(TransactionID) references croatia.TransactionHeader(TransactionID)
	)
end
go
--drop table croatia.TransactionGame