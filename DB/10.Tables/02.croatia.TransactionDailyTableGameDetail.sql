set quoted_identifier, ansi_nulls on
if object_id('croatia.TransactionDailyTableGameDetail') is null
begin
	create table croatia.TransactionDailyTableGameDetail
	(
		TransactionID bigint not null,
		Casino nvarchar(64) not null,
		GameCode nvarchar(64) not null,
		PayInAmountReal numeric(38,18) not null,
		PayInAmountReleasedBonus numeric(38,18) not null,
		PayInAmountPlayableBonus numeric(38,18) not null,
		
		PayOutAmountReal numeric(38,18) not null,
		PayOutAmountReleasedBonus numeric(38,18) not null,
		PayOutAmountPlayableBonus numeric(38,18) not null,

		JackpotAccumulatedAmountReal numeric(38,18) not null,
		JackpotAccumulatedAmountReleasedBonus numeric(38,18) not null,
		JackpotAccumulatedAmountPlayableBonus numeric(38,18) not null,
		
		JackpotPayoutAccumulatedAmountReal numeric(38,18) not null,
		JackpotPayoutAccumulatedAmountReleasedBonus numeric(38,18) not null,
		JackpotPayoutAccumulatedAmountPlayableBonus numeric(38,18) not null,
		Currency nchar(3) not null,
		constraint PK_croatia_TransactionDailyTableGameDetail primary key (TransactionID, Casino, GameCode, Currency),
		constraint FK_croatia_TransactionDailyTableGameDetail_TransactionDailyTableGameHeader foreign key (TransactionID) references croatia.TransactionDailyTableGameHeader(TransactionID)
	)
end
-- drop table croatia.TransactionDailyTableGameDetail