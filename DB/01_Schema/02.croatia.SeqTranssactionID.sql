set ansi_nulls, quoted_identifier on
go
if not exists(select * from sys.sequences where schema_id = schema_id('croatia') and name = 'SeqTranssactionID')
begin
	create sequence croatia.SeqTranssactionID as bigint start with 1 increment by 1 ; 
end